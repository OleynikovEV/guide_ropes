drop temporary table if exists tmp1;

create temporary table tmp1 as
	select STRAND_OPERATION.str_oper_id
	from STRAND_OPERATION 
	left join STRAND_CONST on STRAND_CONST.id = STRAND_OPERATION.strand_const_id
	where STRAND_OPERATION.operation_id = 4
	group by STRAND_CONST.gost_r_id;

delete from STRAND_OPERATION 
where STRAND_OPERATION.str_oper_id in (
	select str_oper_id from tmp1
);

drop temporary table if exists tmp1;
