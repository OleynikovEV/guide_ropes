insert into DIAMETER_S(id_gost_r, diam_r, id_strand_const, diam_s, step_s) 
select soi.id_gost_r, soi.diam_r, sc.id as id_strand_const, soi.diam_so as diam_s, soi.steep_so as step_s
from STRAND_OPERATION AS so 
left join STRAND_OPERATION_INFO AS soi ON soi.strand_oper_id = so.str_oper_id
left join STRAND_CONST AS sc on sc.gost_r_id = soi.id_gost_r and sc.strand_r_id = 7
where so.operation_id = 4 and soi.id is not null
group by soi.id_gost_r, soi.diam_r