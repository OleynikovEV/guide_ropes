drop temporary table if exists tmp;
create temporary table tmp as
	select STRAND_OPERATION_INFO.id
	from STRAND_OPERATION 
	left join STRAND_OPERATION_INFO ON STRAND_OPERATION_INFO.strand_oper_id = STRAND_OPERATION.str_oper_id
	where STRAND_OPERATION.operation_id = 4 and STRAND_OPERATION_INFO.id is not null
	group by STRAND_OPERATION_INFO.id_gost_r, STRAND_OPERATION_INFO.diam_r;

delete from STRAND_OPERATION_INFO 
where STRAND_OPERATION_INFO.id in (
	select id from tmp
);

drop temporary table tmp;
