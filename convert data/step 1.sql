insert into STRAND_CONST (gost_r_id, strand_r_id, amount, amount_w, const) 
select soi.id_gost_r, 7 as strand_r_id, 0 as amount, 0 as amount_w, so.construction as const
from STRAND_OPERATION AS so 
left join STRAND_OPERATION_INFO AS soi ON soi.strand_oper_id = so.str_oper_id
where so.operation_id = 4 and soi.id is not null 
group by soi.id_gost_r, const 