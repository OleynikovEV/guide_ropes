<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller{

    //Page info
    protected $data = Array();
    protected $pageName = FALSE;
    protected $template = "main";
    protected $hasNav = TRUE;
    //Page contents
    protected $javascript = array();
    protected $css = array();
    protected $fonts = array();
    //Page Meta
    protected $title = FALSE;
    protected $description = FALSE;
    protected $keywords = FALSE;
    protected $author = FALSE;

    function __construct(){
        parent::__construct();
        $this->data["uri_segment_1"]=$this->uri->segment(1);
        $this->data["uri_segment_2"]=$this->uri->segment(2);
        $this->title='Справочник канатов';
        $this->description = 'Справочник канатов';
        $this->keywords = '';
        $this->author = '';
        $this->pageName = strToLower(get_class($this));
    }

    protected function _render($view, $renderData = "FULLPAGE"){
        switch ($renderData) {
            case "AJAX"     :
                $this->load->view($view, $this->data);
                break;
            case "JSON"     :
                echo json_encode($this->data);
                break;
            case "FULLPAGE" :
            default         :

                $toTpl = $this->data;
                //static
                $toTpl["javascript"] = $this->javascript;
                $toTpl["css"] = $this->css;
                $toTpl["fonts"] = $this->fonts;

                //meta
                $toTpl["title"] = $this->title;
                $toTpl["description"] = $this->description;
                $toTpl["keywords"] = $this->keywords;
                $toTpl["author"] = $this->author;

                //data
                $toBody["content_body"] = $this->load->view($view, array_merge($this->data, $toTpl), true);
                $toHeader["basejs"] = $this->load->view("template/basejs", $this->data, true);
                $toBody["header"] = $this->load->view("template/header", $toHeader, true);
                $toBody["footer"] = $this->load->view("template/footer", '', true);
                $toTpl["body"] = $this->load->view("template/" . $this->template, $toBody, true);

                $this->load->view("template/skeleton", $toTpl);
                break;
        }
    }

    public function setData($data)
    {
        $this->data['data'] = $data;
        return;
    }
}
