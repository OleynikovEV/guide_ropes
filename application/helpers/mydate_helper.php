<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');


function mydate($date)
{
    $newdate = date_parse($date);
    if ($newdate['day'] < 10) {
        $day = '0' . $newdate['day'];
    } else {
        $day = $newdate['day'];
    }
    if ($newdate['month'] < 10) {
        $month = '0' . $newdate['month'];
    } else {
        $month = $newdate['month'];
    }
    $year = str_split($newdate['year'], 2);
    $result = $day . '.' . $month . '.' . $year[1];;
    return $result;
}

function mydatey($date)
{
    $newdate = date_parse($date);
    if ($newdate['month'] < 10) {
        $month = '0' . $newdate['month'];
    } else {
        $month = $newdate['month'];
    }
    $result = $month . '.' . $newdate['year'];
    return $result;
}