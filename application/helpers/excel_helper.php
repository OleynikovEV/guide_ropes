<?php  if (!defined('BASEPATH')) {
    exit ('No direct script access allowed');
}

$site_path = realpath(dirname(__FILE__) . "/") . "/";
define('SITE_PATH', $site_path);


function excelOut($filename, $data)
{

    $tabName[1] = explode(';', $data['tabName']);
    $data = $data['aaData'];
    // print_r($data);


    foreach ($data as $elToPush) {
        array_push($tabName, $elToPush);
    }


    // print_r($tabName);

    $data_arr = array();
    $row = 1;
    foreach ($tabName as $arr) {
        $cell = 0;
        foreach ($arr as $elem) {

            $data_arr[$row][$cell] = $elem;
            $cell++;
        }
        $row++;
    }

    //print_r($data_arr); //хранит сформированный двумерный массив
    require_once(SITE_PATH . 'lib/PHPExcel-1.7.7/Classes/PHPExcel.php');
    //require_once «PHPExcel.php»; // подключаем фреймворк
    $pExcel = new PHPExcel(); //создаем рабочий объект
    $pExcel->setActiveSheetIndex(0); // устанавливаем номер рабочего документа
    $aSheet = $pExcel->getActiveSheet(); // получаем объект рабочего документа
    $pExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
    $pExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
    $pExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
    $pExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
    $pExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
    $pExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
    $pExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
    $pExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
    $pExcel->getActiveSheet()->getColumnDimension('N')->setWidth(32);
    $pExcel->getActiveSheet()->getColumnDimension('O')->setWidth(32);
    $pExcel->getActiveSheet()->getColumnDimension('L')->setWidth(22);

    $writer_i = 1;
    foreach ($data_arr as $ar) { // читаем массив
        $j = 0;

        foreach ($ar as $val) {
            //--------

            //--------
            $aSheet->setCellValueByColumnAndRow($j, $writer_i, $val); // записываем данные массива в ячейку
            $j++;
        }
        $writer_i++;
    } //die();
    $objWriter = new PHPExcel_Writer_Excel5($pExcel); // создаем объект для записи excel в файл
    header('Content-Type: application/vnd.ms-excel'); // посылаем браузеру нужные заголовки для сохранения файла
    header('Content-Disposition: attachment;filename=»' . $filename . '»');
    header('Cache-Control: max-age=0');
    $objWriter->save('php://output'); // выводим данные в excel формате


}

