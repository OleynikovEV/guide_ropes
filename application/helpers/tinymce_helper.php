<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function tinymce()

{
    echo '<script type="text/javascript" src="/jscripts/editor/tiny_mce.js"></script>';
    echo '<script type="text/javascript">';
    echo 'tinyMCE.init({';
    echo 'language : "ru",';
    echo 'mode : "textareas",';
    echo 'theme : "advanced",';
    echo 'height : "400",';
    echo 'width : "600",';
    echo ' plugins : "autolink, lists, pagebreak, style, save, advhr, advimage, advlink, emotions, iespell, inlinepopups, insertdatetime, preview, media, searchreplace, print, contextmenu, paste, directionality, fullscreen, visualchars, nonbreaking, xhtmlxtras, template, wordcount, advlist, autosave" ,';
    echo 'theme_advanced_buttons1 : "bold, italic, underline, strikethrough, | , justifyleft, justifycenter, justifyright, justifyfull, styleselect, formatselect, fontselect, fontsizeselect",';
    echo 'theme_advanced_buttons2 : "cut, copy, paste, pastetext, pasteword, | , bullist, numlist, | , outdent, indent, blockquote, | , undo, redo , |, insertdate, inserttime, preview, |, forecolor, backcolor",';
    echo 'theme_advanced_buttons3 : "hr, removeformat, visualaid,|, sub, sup, |, print, |, ltr, rtl, |, fullscreen",
        theme_advanced_buttons4 : " moveforward, movebackward, absolute, |, styleprops, |, cite, abbr, acronym, del, ins, attribs, |, visualchars, nonbreaking, template, pagebreak, restoredraft",
        theme_advanced_toolbar_location : "top", theme_advanced_toolbar_align : "left", theme_advanced_statusbar_location : "bottom", theme_advanced_resizing : true
   ';
    echo '});';
    echo '</script>';
}

?>