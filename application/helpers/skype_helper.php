<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

function send($skype_user, $message)
{
    if (isset($skype_user) and isset($message)) {
        $skype = new COM('Skype4COM.Skype');

        if (!$skype->client()->isRunning()) {
            $skype->client()->start(true, true);
        }

        $skype->attach(5, false);

        $members = new COM('Skype4COM.UserCollection');
        $members->Add($skype->User($skype_user));

        $chat = $skype->CreateChatMultiple($members);
        $chat->OpenWindow();
        $chat->SendMessage($message);
//    $chat->CloseWindow();
    }
}

?>
