<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

function monthname($month)
{

    switch ($month) {
        case 1 :
            $monthname = 'Январь';
            break;
        case 2 :
            $monthname = 'Февраль';
            break;
        case 3 :
            $monthname = 'Март';
            break;
        case 4 :
            $monthname = 'Апрель';
            break;
        case 5 :
            $monthname = 'Май';
            break;
        case 6 :
            $monthname = 'Июнь';
            break;
        case 7 :
            $monthname = 'Июль';
            break;
        case 8 :
            $monthname = 'Август';
            break;
        case 9 :
            $monthname = 'Сентябрь';
            break;
        case 10 :
            $monthname = 'Октябрь';
            break;
        case 11 :
            $monthname = 'Ноябрь';
            break;
        case 12 :
            $monthname = 'Декабрь';
            break;

            return $monthname;

    }


}