<?php class DR_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Europe/Kiev');
        $this->load->database();
        $this->load->helper(array('form', 'url', 'access', 'do_mail'));
        $this->load->library('form_validation', 'uri');
    }

    /** для работы с новой версией */
    // список стандартов
    function getAllGost_r_sl2(){
        $qry = "Select concat(g.gost_r, ' (',g.id_gost_r, ')', ' (',g.sts, ')') as gost_r ,
          g.id_gost_r, g.sts, g.construct,
          (select id_diam_r from diameter_r where id_gost_r = g.id_gost_r limit 1) as id_diam_r
          from gost_r as g
          order by g.gost_r asc";
        $buf_table = $this->db->query($qry)->result_array();;
        $arr = array();
        foreach($buf_table as $key => $val) {
            $arr[$key]["id"] = $val["id_gost_r"];
            $arr[$key]["text"] = $val["gost_r"];
            $arr[$key]["sts"] = $val["sts"];
            $arr[$key]["construct"] = $val["construct"];
            $arr[$key]["id_diam_r"] = $val["id_diam_r"];
        }
        return $arr;
    }
    // список диаметров по госту
    function getDiametr_r_sl2($id_gost_r) {
        $gost_r = $this->db->select("id_diam_r,diam_r")
            ->where("id_gost_r", $id_gost_r)
            ->get("DIAMETER_R")->result_array();
        $res = array();
        foreach($gost_r as $key => $val) {
            $res[$key]["id"] = $val["id_diam_r"];
            $res[$key]["text"] = $val["diam_r"];
        }
        return $res;
    }

    function get_core_sl2($id_diam_r) {
        $qry = "Select core.core , core.id_core
          from core, final_weight
          where final_weight.id_diam_r=$id_diam_r and core.id_core=final_weight.id_core
          group by core.id_core";

        $buf_table = $this->db->query($qry)->result_array();
        $result = array();
        foreach ($buf_table as $key => $val) {
            $result[$key]["id"] = $val["id_core"];
            $result[$key]["text"] = $val["core"];
        }
        return $result;
    }

    function get_lubr_type_sl2($id_diam_r, $id_core) {
        $qry = "Select lubrication_type.id_type, lubrication_type.name
            from lubrication_type , final_weight
            where final_weight.id_diam_r='$id_diam_r' and
            final_weight.id_core='$id_core' and final_weight.id_type=lubrication_type.id_type
            group by lubrication_type.id_type
            order by lubrication_type.id_type";

        $buf_table = $this->db->query($qry)->result_array();
        foreach ($buf_table as $key => $val) {
            $result[$key]["id"] = $val["id_type"];
            $result[$key]["text"] = $val["name"];
        }
        return $result;
    }

    function get_steep_sl2($id_diam_r, $id_core) {
        $qry = "Select steep.id_steep, steep.name
          from  steep, final_weight
            where final_weight.id_diam_r=$id_diam_r and
            final_weight.id_core=$id_core
            and final_weight.id_steep=steep.id_steep
            group by steep.id_steep";

        $buf_table = $this->db->query($qry)->result_array();
        foreach ($buf_table as $key => $val) {
            $result[$key]["id"] = $val["id_steep"];
            $result[$key]["text"] = $val["name"];
        }
        return $result;
    }

    function get_weight_r($id_diam_r) {
        $qry = "Select weight_r From DIAMETER_R where id_diam_r=$id_diam_r";
        return $this->db->query($qry)->row()->weight_r;
    }

    /** диаметр и шаг металлического сердечника */
    function get_met_core_info($id_gost_r, $diam_r) {
        $qry = "
                select d.diam_s, d.step_s
                from STRAND_CONST as sc
                left join DIAMETER_S as d on d.id_strand_const = sc.id
                where sc.gost_r_id = $id_gost_r and d.diam_r = $diam_r and sc.strand_r_id = 7
            ";
        $result = $this->db->query($qry);
        if($result->num_rows() > 0){
            $result = $result->row();
            return array(
                'diam_s' => $result->diam_s,
                'step_s' => $result->step_s,
            );
        }
        else  return array(
            'diam_s' => null,
            'step_s' => null,
        );
    }
    /** диаметр сердечника */
    function get_diam_c_new1($id_diam_r, $id_core, $id_gost_r, $diam_r) {
        $qry = "
                select d.diam_s as diam_core
                from STRAND_CONST as sc
                left join DIAMETER_S as d on d.id_strand_const = sc.id
                where sc.gost_r_id = $id_gost_r and d.diam_r = $diam_r and sc.strand_r_id = 7
            ";
        $result = $this->db->query($qry);
        if($result->num_rows() > 0)
            return $result->row()->diam_core;
        else return $this->get_diam_c_new($id_diam_r, $id_core);
    }
    function get_diam_c_new($id_diam_r, $id_core) {
        $qry = "Select diameter_c.diam_core
            from  diameter_c
            where diameter_c.id_diam_r=$id_diam_r and diameter_c.id_core=$id_core";

        return $this->db->query($qry)->row()->diam_core;
    }

    function get_final_weight_new($id_diam_r, $id_core, $id_steep, $id_type) {
        $qry = "Select final_weight
            from final_weight
            where final_weight.id_diam_r=$id_diam_r and
            final_weight.id_core=$id_core and id_steep=$id_steep and id_type=$id_type";
        return $this->db->query($qry)->row()->final_weight;
    }

    function get_data_for_edit($id_gost_r) {
        $data["data_r"] = $this->get_data_for_edit_r($id_gost_r);
        $data["data_c"] = $this->get_data_for_edit_c($id_gost_r);
        $data["data_s"] = $this->get_data_for_edit_s($id_gost_r);
        return $data;
    }

    function get_data_for_edit_r($id_gost_r) {
        $sql = "
            select dr.id_diam_r, dr.diam_r, sl.step_r, sl.id_diam_r as sl_id_diam_r
            from gost_r as g
            left join diameter_r as dr on dr.id_gost_r = g.id_gost_r
            left join step_lay as sl on sl.id_diam_r = dr.id_diam_r
            where g.id_gost_r = $id_gost_r and dr.diam_r is not null
        ";
        return $this->db->query($sql)->result();
    }

    function get_data_for_edit_c($id_gost_r) {
        $sql = "
            select dr.id_diam_r, dr.diam_r, sl.id_diam_r as id_diam_r_sl,
            c.id_core, c.core, dc.id_diam_c, dc.diam_core, sl.step_c
            from gost_r as g
            left join diameter_r as dr on dr.id_gost_r = g.id_gost_r
            left join diameter_c as dc on dc.id_diam_r = dr.id_diam_r
            left join core as c on c.id_core = dc.id_core
            left join step_lay as sl on sl.id_diam_r = dr.id_diam_r
            where g.id_gost_r = $id_gost_r and dr.id_diam_r is not null
        ";
        return $this->db->query($sql)->result();
    }

    function get_data_for_edit_s($id_gost_r) {
        $sql = "
            select g.id_gost_r, dr.id_diam_r, dr.diam_r, sr.short as `strand_r_name`,
            sc.id as id_strand_const, sc.const, ds.id_diam_s, ds.diam_s, ds.step_s
            from gost_r as g
            left join diameter_r as dr on dr.id_gost_r = g.id_gost_r
            left join strand_const as sc on sc.gost_r_id = g.id_gost_r
            left join diameter_s as ds on ds.diam_r = dr.diam_r AND ds.id_strand_const = sc.id
            left join strand_r as sr on sr.id_strand_r = sc.strand_r_id
            where g.id_gost_r = $id_gost_r
            order by dr.id_diam_r, sr.id_strand_r
        ";
        return $this->db->query($sql)->result();
    }

    function getStrand($id_gost) {
        $sql = "
            select ps.id_strand_r, sr.name, sr.short as strand_short, sc.amount, sc.amount_w, sc.const, sc.rope, sc.id,
              sc.Z, sc.S
            from gost_r as g
            left join (select id_diam_r, id_gost_r from diameter_r where id_gost_r = $id_gost limit 1) as d on d.id_gost_r = g.id_gost_r
            left join spinup as s on s.id_diam_r = d.id_diam_r
            left join ply_strand as ps on ps.id_p_s = s.id_p_s
            left join strand_r as sr on sr.id_strand_r = ps.id_strand_r
            left join strand_const as sc on sc.gost_r_id  = g.id_gost_r and sc.strand_r_id = sr.id_strand_r
            where ps.id_strand_r is not null
            group by sr.id_strand_r

            union all

            select sc.strand_r_id as id_strand_r, sr.name, sr.short as strand_short, sc.amount, sc.amount_w, sc.const, sc.rope, sc.id,
              sc.Z, sc.S
            from strand_const as sc
            inner join strand_r as sr on sr.id_strand_r = sc.strand_r_id
            where sc.gost_r_id = $id_gost and sc.strand_r_id = 7

            order by id_strand_r desc, id desc
        ";
        return $this->db->query($sql)->result();
    }

    function getStrandEdit($id_gost) {
        $sql = "
            select t.*, Z.name as `right`, S.name as `left`
            from (
            select ps.id_strand_r, sr.name, sr.short as strand_short, sc.amount, sc.amount_w, sc.const, sc.rope, sc.id,
              sc.Z, sc.S, sc.oZ, sc.oS, null as id_strand_operation, sc.formula, sr.var
            from gost_r as g
              left join (select id_diam_r, id_gost_r from diameter_r where id_gost_r = $id_gost limit 1) as d on d.id_gost_r = g.id_gost_r
              left join spinup as s on s.id_diam_r = d.id_diam_r
              left join ply_strand as ps on ps.id_p_s = s.id_p_s
              left join strand_r as sr on sr.id_strand_r = ps.id_strand_r
              left join strand_const as sc on sc.gost_r_id  = g.id_gost_r and sc.strand_r_id = sr.id_strand_r
              where ps.id_strand_r is not null
              group by sr.id_strand_r

            union all

            select sc.strand_r_id as id_strand_r, sr.name, sr.short as strand_short, sc.amount, sc.amount_w, sc.const, sc.rope, sc.id,
              sc.Z, sc.S, sc.oZ, sc.oS, null as id_strand_operation, sc.formula, sr.var
            from strand_const as sc
              inner join strand_r as sr on sr.id_strand_r = sc.strand_r_id
              where sc.gost_r_id = $id_gost and sc.strand_r_id = 7

            union all

            select sc.strand_r_id as id_strand_r, o.name, null as strand_short, null as amount, null as amount_w, so.construction as const, sc.rope, null as id,
              so.Z, so.S, so.oZ, so.oS, so.str_oper_id as id_strand_operation, so.formula, o.var
              from STRAND_CONST as sc
              inner join STRAND_OPERATION as so on so.strand_const_id = sc.id
              inner join strand_r as sr on sr.id_strand_r = sc.strand_r_id
              inner join OPERATION as o on o.operation_id = so.operation_id
              where sc.gost_r_id = $id_gost
            ) as t
              left join npsv as Z on Z.n = t.Z
              left join npsv as S on S.n = t.S
            order by t.id_strand_r desc, t.id desc
        ";

        return $this->db->query($sql)->result();
    }

    function get_strand_r() {
        return $this->db->select('id_strand_r as id, short as text')->get('STRAND_R')->result();
    }

    function get_strand_cons($id_gostt) {
        return $this->db->select("id, const as text, amount, amount_w, strand_r_id, rope")->get_where("strand_const", array("gost_r_id" => $id_gostt))->result();
    }

    public function get_strand_info_old($id_gost_r, $diam_r) {
        return $this->db->select("diam_s, step_s, id_strand_const")
            ->order_by("id_strand_const")
            ->get_where("diameter_s", array("diam_r" => $diam_r))->result();
    }

    public function get_strand_info($id_gost_r, $diam_r) {
         return $this->db->select("d.diam_s, d.step_s, d.id_strand_const, d.id_gost_r")
            ->join("STRAND_CONST as sc", " sc.id = d.id_strand_const", "left")
            ->where("d.id_gost_r", $id_gost_r)
            ->where("d.diam_r", $diam_r)
            ->where("sc.strand_r_id <> 7")
//            ->order_by("d.id_strand_const")
            ->order_by("sc.strand_r_id desc")
            ->get("diameter_s as d")->result();
    }

    public function get_strand_operation($id_gost_r) {
        $sql = "
            select strand_operation.*
            from strand_operation, strand_const
            where strand_const.id = strand_operation.strand_const_id
            and strand_const.gost_r_id = $id_gost_r
        ";
        $data = $this->db->query($sql)->result();
        return $data;
    }

    function getStrandInfo($id_gost_r) {
        $sql = "
            select
              ds.id_diam_s as id,
              sc.gost_r_id as id_gost_r,
              ds.diam_s, ds.step_s,
              dr.diam_r,
              sl.step_r,
              sc.const,
              sr.short as name_strand,
              sr.id_strand_r,
              0 as operation_id,
              'sc' as param,
              sc.id as id_strand_const,
              null as strand_oper_id,
              sr.var,
              sc.formula,
              ds.Ki as Ki_
            from STRAND_CONST as sc
              left join DIAMETER_R as dr on dr.id_gost_r = sc.gost_r_id
              left join STEP_LAY as sl on sl.id_diam_r = dr.id_diam_r
              left join STRAND_R as sr on sr.id_strand_r = sc.strand_r_id
              left join DIAMETER_S as ds on ds.id_gost_r = sc.gost_r_id and ds.id_strand_const = sc.id and ds.diam_r = dr.diam_r
            where sc.gost_r_id = $id_gost_r and dr.id_diam_r is not null

            union

            select
              soi.id,
              sc.gost_r_id as id_gost_r,
              soi.diam_so as diam_s, soi.steep_so as step_s,
              dr.diam_r,
              sl.step_r,
              so.construction as const,
              o.name as name_strand,
              sc.strand_r_id as id_strand_r,
              o.operation_id,
              'soi' as param,
              null as id_strand_const,
              so.str_oper_id as strand_oper_id,
              o.var as var,
              so.formula,
              soi.Ki as Ki_
            from STRAND_OPERATION as so
              left join STRAND_CONST as sc on sc.id = so.strand_const_id
              left join DIAMETER_R as dr on dr.id_gost_r = sc.gost_r_id
              left join STEP_LAY as sl on sl.id_diam_r = dr.id_diam_r
              left join STRAND_OPERATION_INFO as soi on soi.id_gost_r = sc.gost_r_id and soi.diam_r = dr.diam_r and soi.strand_oper_id and so.str_oper_id = soi.strand_oper_id
              left join OPERATION as o on o.operation_id = so.operation_id
            where sc.gost_r_id = $id_gost_r and dr.id_diam_r is not null

            order by diam_r, id_strand_r, operation_id desc
        ";

        return $this->db->query($sql)->result();
    }

    public function get_operation_list() {
        return $this->db->select("operation_id as id, name as text")->get("operation")->result();
    }

    public function get_strand_operation_info_byGost($id_gost) {
        $result = $this->db->select("soi.id, soi.strand_oper_id, soi.steep_so, soi.diam_so, soi.diam_r,
                                     so.strand_const_id, so.operation_id, so.construction, sc.gost_r_id")
                        ->from("strand_const as sc")
                        ->join("strand_operation as so", "sc.id = so.strand_const_id", "left")
                        ->join("strand_operation_info as soi", "soi.strand_oper_id = so.str_oper_id", "left")
                        ->where("sc.gost_r_id", $id_gost)
                        ->where("soi.id is not null")
                        ->get()->result();
        $tmp = array();
        foreach($result as $key => $val) {
            $tmp[$val->diam_r][$val->strand_oper_id] = array(
                "diam_r" => $val->diam_r,
                "strand_oper_id" => $val->strand_oper_id,
                "steep_so" => $val->steep_so,
                "diam_so" => $val->diam_so,
                "id" => $val->id,
                "strand_const_id" => $val->strand_const_id,
                "operation_id" => $val->operation_id,
                "construction" => $val->construction,
                "id_gost_r" => $val->gost_r_id,
            );
        }

        return $tmp;
    }

    public function get_strand_operation_byGost_diam_r($id_gost, $diam) {
        $sql = "
            SELECT GOST_R.id_gost_r, STRAND_R.id_strand_r, STRAND_OPERATION.operation_id, STRAND_OPERATION.strand_const_id,
            STRAND_OPERATION.construction, STRAND_OPERATION_INFO.id AS soi_id, STRAND_OPERATION_INFO.steep_so,
            STRAND_OPERATION_INFO.diam_so, OPERATION.name AS oper_name, STRAND_OPERATION_INFO.diam_r
            FROM STRAND_R
            INNER JOIN PLY_STRAND ON PLY_STRAND.id_strand_r = STRAND_R.id_strand_r
            INNER JOIN SPINUP ON SPINUP.id_p_s = PLY_STRAND.id_p_s
            INNER JOIN DIAMETER_R ON DIAMETER_R.id_diam_r = SPINUP.id_diam_r
            INNER JOIN GOST_R ON GOST_R.id_gost_r = DIAMETER_R.id_gost_r
            INNER JOIN STRAND_CONST ON STRAND_CONST.gost_r_id = GOST_R.id_gost_r AND STRAND_CONST.strand_r_id = STRAND_R.id_strand_r
            INNER JOIN STRAND_OPERATION ON STRAND_OPERATION.strand_const_id = STRAND_CONST.id
            INNER JOIN OPERATION ON OPERATION.operation_id = STRAND_OPERATION.operation_id
            INNER JOIN STRAND_OPERATION_INFO ON STRAND_OPERATION_INFO.strand_oper_id = STRAND_OPERATION.str_oper_id
            WHERE GOST_R.id_gost_r = $id_gost AND STRAND_OPERATION_INFO.diam_r = $diam
            GROUP BY STRAND_R.id_strand_r, STRAND_OPERATION.operation_id
            ORDER BY STRAND_R.id_strand_r
        ";
        $result = $this->db->query($sql)->result();
        return $result;
    }

    /** обновление данных */
    /** КОГДА СПРАВОЧНИК БУДЕТ НАПОЛНЯТЬСЯ ИЗ НАШЕЙ СИСТЕМЫ А НЕ ИЗ 1С
     *  НУЖНО ПЕРЕДЕЛАТЬ
     * */
    public function update_step_r($data) {
        $update = array();
        $insert = array();
        foreach($data as $row) {
            array_push($update, array(
                "id_diam_r" => $row["id_diam_r"],
                "step_r" => $row["step_r"],
            ));
        }
        $result = 0;
        if(count($update) > 0) $this->db->update_batch("step_lay", $update, "id_diam_r");
        return $this->db->affected_rows();
    }

    public function update_step_c($data) {
        $update_diam_r = array();
        $update_step = array();
        foreach($data as $row){
            if(isset($row->core)) unset($row->core);
            if(isset($row->diam_r)) unset($row->diam_r);
            array_push($update_diam_r, array(
                "id_diam_c" => $row->id_diam_c,
                "id_diam_r" => $row->id_diam_r,
                "id_core" => $row->id_core,
                "diam_core" => $row->diam_core,
            ));
            array_push($update_step, array(
                "id_diam_r" => $row->id_diam_r,
                "step_c" => $row->step_c,
            ));
        }
        $result = 0;
        if(count($update_diam_r) > 0) $this->db->update_batch("DIAMETER_C", $update_diam_r, "id_diam_c");
        $result = $this->db->affected_rows();
        if(count($update_step) > 0)$this->db->update_batch("step_lay", $update_step, "id_diam_r");
        return $result = $result + $this->db->affected_rows();
    }

    public function update_step_s($data){
        $update = array();
        $insert = array();
        foreach($data as $row){
            if( !isset($row['id_diam_s']) || is_null($row['id_diam_s']) || $row['id_diam_s'] == 0 ) array_push($insert, (array)$row);
            else array_push($update, (array)$row);
        }
        if( count($insert) > 0 ) $this->db->insert_batch("DIAMETER_S", $insert);
        $result = $this->db->affected_rows();
        if( count($update) > 0 )  $this->db->update_batch("DIAMETER_S", $update, "id_diam_s");
        return $result + $this->db->affected_rows();
    }

    public function update_diam_w_pos($data)
    {
        $sql = '';
        $result = 0;
        foreach ($data as $row) {
            $sql = 'insert into  PLY_W_STRAND (id_ply_w_strand, id_gost_r, id_ply_strand, ply)
                      value ('.$row['id_gost_r'].$row['id_ply_strand'].','.$row['id_gost_r'].','.$row['id_ply_strand'].','.$row['ply'].')
                      on duplicate key update ply = '.$row['ply'].'; ';
            $this->db->query($sql);
            $result += $this->db->affected_rows();
        }
        return $result;
    }

    public function update_operation_s($data) {
        $update = array();
        $insert = array();
        foreach($data as $row){
            if( !isset($row['id']) || is_null($row['id']) || $row['id'] == 0 ) array_push($insert, (array)$row);
            else array_push($update, (array)$row);
        }

        if( count($insert) > 0 ) $this->db->insert_batch("STRAND_OPERATION_INFO", $insert);
        $result = $this->db->affected_rows();
        if( count($update) > 0 )  $this->db->update_batch("STRAND_OPERATION_INFO", $update, "id");
        return $result + $this->db->affected_rows();
    }

    function updateStrand($data) {
        foreach ($data as $row) {
            if ($row['id_strand_operation'] > 0) {
                $this->db->where('str_oper_id', $row['id_strand_operation']);
                $this->db->update('STRAND_OPERATION',
                    array(
                        'formula' => $row['formula'],
                        'Z' => $row['Z'],
                        'S' => $row['S'],
                        'oZ' => $row['oZ'],
                        'oS' => $row['oS']
                    )
                );
            }
            elseif ($row['id'] == 0) {
                unset($row['id']);
                unset($row['id_strand_operation']);
                $this->db->insert('STRAND_CONST', $row);
            } else {
                $this->db->where('id', $row['id']);
                unset($row['id']);
                unset($row['id_strand_operation']);
                $this->db->update('STRAND_CONST', $row);
            }
        }
    }

    function delete_strand_const($id) {
        $this->db->delete('STRAND_CONST', array('id' => $id));
        return $this->db->affected_rows();
    }

    // добавление операции к прядям
    public function add_operation_to_strand($data) {
        $this->db->insert("strand_operation", $data);
        return $this->db->affected_rows();
    }
    // дуаление операции из прядей
    public function del_operation_in_strand($id) {
        $this->db->delete("strand_operation_info", array("strand_oper_id" => $id));
        $this->db->delete("strand_operation", array("str_oper_id" => $id));
        if( $this->db->affected_rows() > 0 ) return true;
        return false;
    }

    /** для поддержки старой версии */
    // getAllGost_r
    function getAllGost_r()
    {
        $qry = "Select concat(gost_r, ' (',id_gost_r, ')', ' (',sts, ')') as gost_r , id_gost_r from gost_r order by gost_r asc";
        $buf_table = $this->db->query($qry)->result_array();;
        $test = "";
        foreach ($buf_table as $key) {
            if ($key["gost_r"] == $test) {
                $result[$key["gost_r"] . " "] = $key["id_gost_r"];
                $test = $key["gost_r"];
            } else {
                $result[$key["gost_r"]] = $key["id_gost_r"];
                $test = $key["gost_r"];
            }
        }
        return $result;
    }

    function getDiameterForGost($gost_r)
    {
        $res2 = array();
        if ($gost_r != null) {
            $lll = urldecode($gost_r);
            $gost_r = $this->db->where("id_gost_r = '$lll'")->select("id_diam_r,diam_r")->get("DIAMETER_R")->result_array();
            foreach ($gost_r as $key) {
                $res2[$key["id_diam_r"]] = $key["diam_r"];
            }
            return $res2;
        } else {
            return null;
        }
    }

    function getAllGostInfo()
    {
        $this->db->select( "gost_r.id_gost_r, gost_r.construct, gost_r.gost_r, gost_r.sts, DIAMETER_R.diam_r, DIAMETER_R.id_diam_r" );
        $this->db->from( "gost_r" );
        $this->db->join( "DIAMETER_R", "DIAMETER_R.id_gost_r = gost_r.id_gost_r", "left" );
        $this->db->order_by( "gost_r.gost_r" );
        $query = $this->db->get()->result_array();

        $data = null;
        $key_prev = null;
        foreach( $query as $key => $value )
        {
            $id_diam_r = $value["id_diam_r"];

            if ($key_prev != $value["id_gost_r"])
            {
                $data[$value["id_gost_r"]]["id_gost_r"] = $value["id_gost_r"];
                $data[$value["id_gost_r"]]["construct"] = $value["construct"];
                $data[$value["id_gost_r"]]["gost_r"] = $value["gost_r"];
                $data[$value["id_gost_r"]]["sts"] = $value["sts"];

                $data[$value["id_gost_r"]]["diam_r"][$value["diam_r"]] = $this->getSummWire($id_diam_r);
                $data[$value["id_gost_r"]]["diam_r"][$value["diam_r"]]["final_weight"] = $this->getFinalWeight($id_diam_r, $value["id_gost_r"]);
            }
            else
            {
                $data[$value["id_gost_r"]]["diam_r"][$value["diam_r"]] = $this->getSummWire($id_diam_r);
                $data[$value["id_gost_r"]]["diam_r"][$value["diam_r"]]["final_weight"] = $this->getFinalWeight($id_diam_r, $value["id_gost_r"]);
            }
            $key_prev = $id_diam_r;
        }
        return $data;
    }

    /* Сумма проволок по диаметру каната */
    function getSummWire($id_diam_r)
    {
        $this->db->select( "sum(spinup.percent_weight_w) as percent, diameter_w.diam_w, sum((diameter_r.weight_r*spinup.percent_weight_w/100)) as ves" );
        $this->db->join( "diameter_w", "diameter_w.id_diam_w = spinup.id_diam_w" , "left" );
        $this->db->join( "diameter_r", "diameter_r.id_diam_r = spinup.id_diam_r" , "left" );
        $this->db->where( "spinup.id_diam_r", $id_diam_r );
        $this->db->group_by( "diameter_w.diam_w" );
        $buf_table = $this->db->get( "spinup" );
        $datatable = null;
        $i = 1;
        foreach( $buf_table->result() as $row )
        {
            $datatable[$i]['diam_w'] = $row->diam_w;
            $datatable[$i]['ves'] = round($row->ves, 1);
            $datatable[$i]['percent'] = round($row->percent, 2);
            $i++;
        }
        return $datatable;
    }

    /* выборка веса каната с металическим сердечником или полипропиленовым */
    function getFinalWeight( $id_diam_r, $id_gost_r = null )
    {
        $this->db->select( 'id_core, final_weight' );
        $this->db->where( 'id_diam_r', $id_diam_r );
		//$this->db->where( 'id_type = 3 and (id_steep = 2 or id_steep = 4)' );
		$this->db->where( 'id_type = 3' );
        $tmp = $this->db->get( 'FINAL_WEIGHT' )->result_array();

        // если веса по данному диаметру нкту - возвращаем null
        if( $tmp == null )
            return null;

        $id_core = 3; // id сердечника поумолчанию - полипропилен
        // преобразуем рекзультаты в массив
        foreach( $tmp as $row )
        {
            $array[ $row['id_core'] ] = $row['final_weight'];
            $id_core =  $row['id_core'];
        }

        if( array_key_exists(3, $array) ) // проверяем входит ли сердечник на полипропилене
            $tmp = $array[3]; // входит - записываем в массив
        elseif( array_key_exists(4, $array) ) // проверяем входит ли сердечник на метале
            $tmp = $array[4]; // входит - записываем в массив
        else // если сердечников на мет. и полипроп. нету, записываем вес любого сердечника который есть
            $tmp = $array[$id_core];

        return $tmp;
    }

    function getConstrForGost($gost_r)
    {
        $lll = urldecode($gost_r);
        $val = array();
        $res2 = array();
        $sql = 'Select id_gost_r,construct From GOST_R where id_gost_r= ?';

        $val[] = $lll;
        $query = $this->db->query($sql, $val);
        $result = $query->result_array();

        foreach ($result as $key) {
            $res2[$key["id_gost_r"]] = $key["construct"];
        }
        return $res2;

    }

    function getIdDiam($id_diam_r)
    {
        $buf_diam_r = urldecode($id_diam_r);

        $qry = "Select id_diam_r,weight_r From DIAMETER_R where id_diam_r='$buf_diam_r'";

        $buf_data = $this->db->query($qry)->result_array();
        foreach ($buf_data as $key) {
            $res2[$key["id_diam_r"]] = $key["weight_r"];
        }
        return $res2;
    }

    function getSpinup($id_diam_r)
    {
//        $buf_diam_r = urldecode($id_diam_r);
//        $qry = "Select spinup.percent_weight_w as percent, diameter_w.diam_w, ROUND((diameter_r.weight_r*spinup.percent_weight_w/100),1) as ves
//            from spinup, diameter_w, diameter_r, ply_strand, PLY_W_STRAND
//            where spinup.id_diam_w=diameter_w.id_diam_w and spinup.id_diam_r='$buf_diam_r' and spinup.id_diam_r=diameter_r.id_diam_r
//              and ply_strand.id_p_s=spinup.id_p_s and PLY_W_STRAND.id_gost_r = diameter_r.id_gost_r and PLY_W_STRAND.id_ply_strand = ply_strand.id_p_s
//            ORDER BY ply_strand.id_strand_r desc, PLY_W_STRAND.ply, ply_strand.id_ply_w, ply_strand.id_p_s";

        $qry ="Select ply_strand.id_strand_r, ply_strand.id_p_s, spinup.percent_weight_w as percent, diameter_w.diam_w,
            ROUND((diameter_r.weight_r*spinup.percent_weight_w/100),1) as ves, ply_strand.id_ply_w, SPINUP.length_ratio, PLY_W_STRAND.*
        from spinup
        left join diameter_r on diameter_r.id_diam_r = spinup.id_diam_r
        left join diameter_w on diameter_w.id_diam_w = spinup.id_diam_w
        left join ply_strand on ply_strand.id_p_s = spinup.id_p_s
        left join PLY_W_STRAND on PLY_W_STRAND.id_gost_r = diameter_r.id_gost_r and PLY_W_STRAND.id_ply_strand = ply_strand.id_p_s
        where spinup.id_diam_r=$id_diam_r
        ORDER BY ply_strand.id_strand_r desc, PLY_W_STRAND.ply, ply_strand.id_ply_w,  ply_strand.id_p_s";

        $buf_table = $this->db->query($qry);
        foreach ($buf_table->result() as $row) {
            $datatable['diam_w'][] = $row->diam_w;
            $datatable['length_ratio'][] = $row->length_ratio;
            $datatable['ves'][] = $row->ves;
            $datatable['percent'][] = $row->percent;
        }
        return $datatable;
    }

    function getPLYSpinup($id_diam_r)
    {
//        $buf_diam_r = urldecode($id_diam_r);
//        $qry = "Select ply_strand.id_strand_r, ply_strand.id_p_s, spinup.percent_weight_w as percent, diameter_w.diam_w,
//                       ROUND((diameter_r.weight_r*spinup.percent_weight_w/100),1) as ves
//                from spinup, diameter_w, diameter_r, ply_strand, PLY_W_STRAND
//                where spinup.id_diam_w=diameter_w.id_diam_w and spinup.id_diam_r='$buf_diam_r'
//                      and spinup.id_diam_r=diameter_r.id_diam_r and ply_strand.id_p_s=spinup.id_p_s
//                      and PLY_W_STRAND.id_gost_r = diameter_r.id_gost_r and PLY_W_STRAND.id_ply_strand = ply_strand.id_p_s
//                ORDER BY ply_strand.id_strand_r desc, PLY_W_STRAND.ply, ply_strand.id_ply_w,  ply_strand.id_p_s";

        $qry ="Select ply_strand.id_strand_r, ply_strand.id_p_s, spinup.percent_weight_w as percent, diameter_w.diam_w,
            ROUND((diameter_r.weight_r*spinup.percent_weight_w/100),1) as ves, ply_strand.id_ply_w, PLY_W_STRAND.*
        from spinup
        left join diameter_r on diameter_r.id_diam_r = spinup.id_diam_r
        left join diameter_w on diameter_w.id_diam_w = spinup.id_diam_w
        left join ply_strand on ply_strand.id_p_s = spinup.id_p_s
        left join PLY_W_STRAND on PLY_W_STRAND.id_gost_r = diameter_r.id_gost_r and PLY_W_STRAND.id_ply_strand = ply_strand.id_p_s
        where spinup.id_diam_r=$id_diam_r
        ORDER BY ply_strand.id_strand_r desc, PLY_W_STRAND.ply, ply_strand.id_ply_w,  ply_strand.id_p_s";

        $buf_table = $this->db->query($qry);
        $i = 0;
        foreach ($buf_table->result() as $row) {
            $datatable[$i]['id_strand_r'] = $row->id_strand_r;
            $datatable[$i]['id_p_s'] = $row->id_p_s;
            $datatable[$i]['diam_w'] = $row->diam_w;
            $datatable[$i]['ves'] = $row->ves;
            $datatable[$i]['percent'] = $row->percent;
            $i++;
        }
        return $datatable;
    }

    function getSumVes($id_diam_r)
    {
        $buf_diam_r = urldecode($id_diam_r);
        $qry = "Select ROUND(sum(spinup.percent_weight_w), 2) as percent, diameter_w.diam_w,  ROUND( sum((diameter_r.weight_r*spinup.percent_weight_w/100)), 1) as ves
            from spinup, diameter_w, diameter_r
            where spinup.id_diam_w=diameter_w.id_diam_w and spinup.id_diam_r='$buf_diam_r' and spinup.id_diam_r=diameter_r.id_diam_r
            group BY diameter_w.diam_w";
        $buf_table = $this->db->query($qry);
        foreach ($buf_table->result() as $row) {
            $datatable['diam_w'][] = $row->diam_w;
            $datatable['ves'][] = $row->ves;
            $datatable['percent'][] = $row->percent;
        }
        return $datatable;
    }

    function get_step_lay($id_diam_r, $param)
    {
        $buf_diam_r = urldecode($id_diam_r);

        $qry = "Select id_diam_r , step_c, step_r
            from step_lay
            where id_diam_r='$buf_diam_r'";
        $buf_table = $this->db->query($qry)->result_array();
        if ($param == 0) {
            foreach ($buf_table as $key) {
                $result["step_s"] = $key["step_c"];
            }
            return $result;
        }
        if ($param == 1) {
            foreach ($buf_table as $key) {
                $result["step_r"] = $key["step_r"];
            }
            return $result;
        }
    }

    function get_core($id_diam_r)
    {
        $buf_diam_r = urldecode($id_diam_r);
        $qry = "Select core.core , core.id_core
            from core, final_weight
            where final_weight.id_diam_r='$buf_diam_r' and core.id_core=final_weight.id_core";
        $buf_table = $this->db->query($qry)->result_array();;
        foreach ($buf_table as $key) {
            $result[$key["id_core"]] = $key["core"];
        }
        return $result;
    }

    function get_core_const($id_gost_r)
    {
        $sql = "
            select IFNULL(sc.const, '') as construct
            from GOST_R as g
            left join STRAND_CONST as sc on sc.gost_r_id = g.id_gost_r and sc.strand_r_id = 7
            where g.id_gost_r = $id_gost_r
        ";
        return $this->db->query($sql)->row()->construct;
    }

    function get_core_step($id_gost_r, $diam_r, $id_diam_r, $param) {
        $qry = "
                select d.step_s as step_s
                from STRAND_CONST as sc
                left join DIAMETER_S as d on d.id_strand_const = sc.id
                where sc.gost_r_id = $id_gost_r and d.diam_r = $diam_r and sc.strand_r_id = 7
            ";
        $result = $this->db->query($qry);
        if( $result->num_rows() > 0 )
            return array("step_s" => $result->row()->step_s );
        else return $this->get_step_lay($id_diam_r, $param);
    }

    function get_diam_c($id_diam_r, $id_core)
    {
        $buf_diam_r = urldecode($id_diam_r);
        $buf_core = urldecode($id_core);
        $qry = "Select diameter_c.diam_core , diameter_c.id_diam_c
        from  diameter_c
        where diameter_c.id_diam_r='$buf_diam_r' and diameter_c.id_core='$buf_core'";
        $buf_table = $this->db->query($qry)->result_array();
        foreach ($buf_table as $key) {
            $result[$key["id_diam_c"]] = $key["diam_core"];
        }
        return $result;
    }

    function get_steep($id_diam_r, $id_core)
    {
        $qry = "Select steep.id_steep, steep.name
        from  steep, final_weight
        where final_weight.id_diam_r='$id_diam_r' and
        final_weight.id_core='$id_core'
        and final_weight.id_steep=steep.id_steep";
        $buf_table = $this->db->query($qry)->result_array();
        foreach ($buf_table as $key) {
            $result[$key["id_steep"]] = $key["name"];
        }
        return $result;
    }

    function get_l_type($id_diam_r, $id_core)
    {
        $qry = "Select lubrication_type.id_type, lubrication_type.name
from lubrication_type , final_weight
where final_weight.id_diam_r='$id_diam_r' and
 final_weight.id_core='$id_core' and final_weight.id_type=lubrication_type.id_type";


        $buf_table = $this->db->query($qry)->result_array();
        foreach ($buf_table as $key) {
            $result[$key["id_type"]] = $key["name"];
        }
        return $result;
    }

    function get_final_weight($id_diam_r, $id_core, $id_steep, $id_type)
    {
        $qry = "Select id_f_weight, final_weight
from final_weight
where final_weight.id_diam_r='$id_diam_r' and
 final_weight.id_core='$id_core' and id_steep='$id_steep' and id_type='$id_type' ";

        $buf_table = $this->db->query($qry)->result_array();
        foreach ($buf_table as $key) {
            $result[$key["id_f_weight"]] = $key["final_weight"];
        }
        return $result;
    }

    function getReel_new()
    {
        $qry = "Select *
from reel
order by diam_cheek,diam_pin, length_pin
";

        $query = $this->db->query($qry);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
            return $data;

        }
        return false;

    }

    function get_reel() {
        $qry = "Select * from reel";
        $buf_table = $this->db->query($qry);
        foreach ($buf_table->result() as $row) {
            $datatable['id'][] = $row->id;
            $datatable['name'][] = $row->name;
            $datatable['diam_cheek'][] = $row->diam_cheek;
            $datatable['diam_pin'][] = $row->diam_pin;
            $datatable['length_pin'][] = $row->length_pin;
            $datatable['weight'][] = $row->weight;
        }
        return $datatable;
    }

    function get_reel_all() {
        $qry = "Select *
            from reel
            order by diam_cheek,diam_pin, length_pin";
        $buf_table = $this->db->query($qry);
        foreach ($buf_table->result() as $row) {
            $datatable['id'][] = $row->id;
            $datatable['name'][] = $row->name;
            $datatable['diam_cheek'][] = $row->diam_cheek;
            $datatable['diam_pin'][] = $row->diam_pin;
            $datatable['length_pin'][] = $row->length_pin;
            $datatable['weight'][] = $row->weight;
            $datatable['weight_w'][] = $row->weight_w;
        }
        return $datatable;
    }

    function getNpsv() {
        $npsv = $this->db->select('n, name, short, en')
                         ->get('npsv')->result();
        $buf = array();
        foreach ($npsv as $key => $row) {
            $buf[$key]['id'] = $row->n;
            $buf[$key]['name'] = $row->name;
            $buf[$key]['other'] = $row;
        }
        return $buf;
    }
}



