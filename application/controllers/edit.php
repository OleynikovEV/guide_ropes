<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Edit extends MY_Controller {

    public function  __construct() {
        parent::__construct();
        $this->load->model("DR_model", "model");
    }

    public function index() {
        if( is_logged_in() !== true ) redirect("content");

        $data["gost_r_list"] = $this->model->getAllGost_r_sl2();
        $gost_r = current($data["gost_r_list"]);
        $id_gost_r = $gost_r["id"];
        $data_list = $this->model->get_data_for_edit_r($id_gost_r);
        $data["first_id_diam_r"] = $data_list[0]->id_diam_r;
        $data["construct"] = $gost_r["construct"];
        $data["operation_list"] = $this->model->get_operation_list();
        $data['strand_r_list'] = $this->model->get_strand_r();
        $data['npsv_list'] = $this->model->getNpsv();

        array_unshift($data['strand_r_list'], array());

        $this->setData($data);

        $this->_render('edit');
    }

    public function ajax_get_data_for_edit() {
        if( !$this->input->has("id_gost_r") ){
            echo null;
            return;
        }

        $id_gost_r = $this->input->post("id_gost_r");
        echo json_encode( $this->model->get_data_for_edit($id_gost_r) );
    }
    public function ajax_get_data_r() {
        if( !$this->input->has("id_gost_r") ){
            echo null;
            return;
        }

        $id_gost_r = $this->input->post("id_gost_r");
        $data["data_r"] = $this->model->get_data_for_edit_r($id_gost_r);
        echo json_encode($data);
    }
    public function ajax_get_data_c() {
        if( !$this->input->has("id_gost_r") ){
            echo null;
            return;
        }

        $id_gost_r = $this->input->post("id_gost_r");
        $data["data_c"] = $this->model->get_data_for_edit_c($id_gost_r);
        echo json_encode($data);
    }
    public function ajax_get_data_s() {
        if( !$this->input->has("id_gost_r") ){
            echo null;
            return;
        }

        $id_gost_r = $this->input->post("id_gost_r");
        $data["data_s"] = $this->model->get_data_for_edit_s($id_gost_r);
        echo json_encode($data);
    }
    public function ajax_get_strand_operation_info_byGost($id_gost_r) {
        echo json_encode( $this->model->get_strand_operation_info_byGost($id_gost_r) );
    }
    /** сохраняем инфу по канату */
    public function ajax_save_r() {
        if( !$this->input->has("data") ) {
            echo false;
            return;
        }
        $data = $this->input->post("data");
        if( $this->model->update_step_r($data) > 0)
            echo true;
        else echo false;
    }
    /** сохраняем инфу по сердечнику */
    public function ajax_save_c() {
        if( !$this->input->has("data") ) {
            echo false;
            return;
        }
        $data = json_decode($this->input->post("data"));
//        prn($data);
        if( $this->model->update_step_c($data) > 0)
            echo true;
        else echo false;
    }
    /** сохраняем инфу по прядям */
    public function ajax_save_s() {
        if( !$this->input->has("data_s") ) {
            echo false;
            return;
        }
        $result = 0;
        $sc = array();
        $soi = array();
        $data_s = json_decode($this->input->post("data_s"));
        foreach ($data_s as $key => $row) {
            if( isset($row->param) ) {
                switch ($row->param) {
                    case 'sc' :
                        $tmp = array(
                            'id_diam_s' => $row->id,
                            'id_gost_r' => $row->id_gost_r,
                            'diam_r' => $row->diam_r,
                            'id_strand_const' => $row->id_strand_const,
                            'diam_s' => $row->diam_s,
                            'step_s' => $row->step_s,
                            'Ki' => $row->Ki,
                        );
                        array_push($sc, $tmp);
                        break;
                    case 'soi' :
                        $tmp = array(
                            'id' => $row->id,
                            'id_gost_r' => $row->id_gost_r,
                            'diam_r' => $row->diam_r,
                            'strand_oper_id' => $row->strand_oper_id,
                            'diam_so' => $row->diam_s,
                            'steep_so' => $row->step_s,
                            'Ki' => $row->Ki,
                        );
                        array_push($soi, $tmp);
                        break;
                }
            }
        }
        if (count($sc) > 0)
            $result += $this->model->update_step_s($sc);
        if (count($soi) > 0)
            $result += $this->model->update_operation_s($soi);

        if( $result > 0) echo true;
        else echo false;
    }

    public function ajax_get_strand_operation($gost_id) {
        echo json_encode( $this->model->get_strand_operation($gost_id) );
    }

    public function ajax_get_strand_cons($gost_id) {
        echo json_encode( $this->model->get_strand_cons($gost_id) );
    }
    // доюавление операции к прядям
    public function ajax_add_operation_to_strand() {
        if( !$this->input->has("data") ) {
            echo false;
            return;
        }

        $data = $this->input->post("data");

        echo $this->model->add_operation_to_strand($data);
    }
    // удаление операции из пряди
    public function ajax_del_operation_in_strand($id) {
        echo $this->model->del_operation_in_strand($id);
    }

    // новое положение проволок в пряди
    public function ajax_save_diam_w_pos()
    {
        $data = $this->input->post('data');
        foreach ($data as $id => $row) {
            if (!isset($row['id_gost_r'])) unset($data[$id]);
        }

        echo json_encode( ($this->model->update_diam_w_pos($data) > 0) ? true : false );
    }

    function ajax_getStrand($id_gost_r) {
        echo json_encode( $this->model->getStrand($id_gost_r) );
    }

    function ajax_getStrandEdit($id_gost_r) {
        echo json_encode( $this->model->getStrandEdit($id_gost_r) );
    }

    function ajax_save_strand_const() {
        $strands = $this->input->post('strands');
        $id_gost_r = $this->input->post('id_gost_r');
        if (count($strands) == 0) {
            echo false;
        }
        else {
            $data = array();
            foreach ($strands as $i =>$str) {
                if(isset($str['id'])) {
                    $data[$i] = array(
                        'gost_r_id' => $id_gost_r,
                        'id' => $str['id'],
                        'strand_r_id' => $str['id_strand_r'],
                        'id_strand_operation' => $str['id_strand_operation'],
                        'amount' => $str['amount'],
                        'amount_w' => $str['amount_w'],
                        'const' => $str['const'],
                        'rope' => $str['rope'],
                        'formula' => $str['formula'],
                        'S' => $str['S'],
                        'Z' => $str['Z'],
                        'oS' => $str['oS'],
                        'oZ' => $str['oZ']
                    );
                }
            }
            $this->model->updateStrand($data);
            echo $this->ajax_getStrandEdit($id_gost_r);
        }
    }

    function ajax_delete_strand_const($id) {
        echo $this->model->delete_strand_const($id);
    }

    function ajax_getStrandInfo($id_gost_r) {
        $res = $this->model->getStrandInfo($id_gost_r);
        if (count($res) > 0) {
            $tmp = array();
            $index = -1;
            $i = 0;
            $cur_diam = 0;
            foreach ($res as $row) {
                if ($cur_diam != $row->diam_r) {
                    $index++;
                    $i = 0;
                    $tmp[$index]['info'] = array(
                        'diam_r' => $row->diam_r,
                        'step_r' => $row->step_r,
                    );
                    $tmp[$index]['list'] = array();
                }
                $var = $row->var;
                if (isset($tmp[$index]['list'][$var])) {
                    $var = $row->var . '_';
                }
                $tmp[$index]['list'][$var] = $row;
                $tmp[$index]['list'][$var]->index = $i;
                $i++;
                $cur_diam = $row->diam_r;
            }
            echo json_encode($tmp);
            return;
        }
        echo null;
    }
}