<?php

/**
 * Created by PhpStorm.
 * User: Eugene
 * Date: 04.10.2017
 * Time: 13:52
 */
class Login extends My_Controller {

    public function index() {
        // если это первая попытка входа
        if(!$this->input->has("login") || !$this->input->has("password")){
            $this->load->view('login');
            return;
        }

        // если пользователь логинится, получаем данные
        $login = $this->input->post("login");
        $passgord = $this->input->post("password");

        $res = $this->db->select("username")
                 ->get_where("users", array("username" => $login, "password" => $passgord));
        if($res->num_rows() > 0) {
            $this->session->set_userdata(array("logged_in" => true));
            redirect('edit');
        }
        else {
            $data["wrong_access"] = "Не верный логин или пароль";
            $this->load->view("login", $data);
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('content');
    }

}