<?php

class ListSTS extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Europe/Kiev');
        $this->load->model("DR_model");
        $this->load->database();
    }

    public function index()
    {
        $data = $this->DR_model->getAllGostInfo();
//        foreach($data as $key => $value)
//            $tmp[$key] = $value;
        //prn($tmp);

        $this->setData($data);
        $this->_render('list_of_sts', 'FULLPAGE');
    }
}