<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Start extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Europe/Kiev');
        $this->load->model("ticket_model");
        $this->load->database();
    }

    public function ajax($param)
    {
        if ($param != "0") {
            $res = $this->DR_model->get_rows($param);
            foreach($res as $key){
                $arr[] = $key["name"];
            }
            die(json_encode($arr));
        }
        die("error");
    }
}
