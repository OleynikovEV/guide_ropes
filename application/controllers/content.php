<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Content extends My_Controller
{
    public function  __construct()
    {
        parent::__construct();
        $this->load->model("DR_model");

    }

    public function index() {
        $this->form_validation->set_rules('gost_r', 'GOST', 'required|callback_checkZero');
        $this->form_validation->set_rules('diameter_r', 'Diameter', 'required|callback_checkZero');
        $res = null;
        $ver = $this->input->get("ver");
        if($ver == "old")
            $this->_render('guide_view_old');
        else {
            $data["gost_r_list"] = $this->DR_model->getAllGost_r_sl2();
            $this->setData($data);
            $this->_render('guide_view_new');
        }
        $param["id_gost_r"] = $this->input->post("gost_r");
        $param["id_diameter_r"] = $this->input->post("diameter_r");
    }

    /** --- для поддержки новой функции */
    public function ajax_get_diam_r($id_gost_r) {
        echo json_encode( $this->DR_model->getDiametr_r_sl2($id_gost_r) );
    }
    /** вид сердечника */
    public function ajax_get_core($id_diam_r) {
        echo json_encode( $this->DR_model->get_core_sl2($id_diam_r) );
    }
    /** конструкция сердечника */
    public function ajax_get_core_const($id_gost_r){
        echo $this->DR_model->get_core_const($id_gost_r);
    }
    /** способ смазки */
    public function ajax_get_lubr_type($id_diam_r, $id_core) {
        echo json_encode( $this->DR_model->get_lubr_type_sl2($id_diam_r, $id_core) );
    }
    /** операции - пряди в канате */
    public function getStrand($id_gost) {
        echo json_encode( $this->DR_model->getStrand($id_gost) );
    }
    /** диаметр сердечника */
    public function ajax_get_diam_core($id_diam_r, $id_core, $id_gost_r = null, $diam_r = null) {
        $res = null;
//        if($id_core == 4)
//            $res = $this->DR_model->get_diam_c_new1($id_diam_r, $id_core, $id_gost_r, $diam_r);
//        else $res = $this->DR_model->get_diam_c_new($id_diam_r, $id_core);
        $res = $this->DR_model->get_diam_c_new($id_diam_r, $id_core);
        echo $res;
    }
    /** пропиткасердечника */
    public function ajax_get_steep($id_diam_r, $id_core) {
        echo json_encode( $this->DR_model->get_steep_sl2($id_diam_r, $id_core) );
    }
    /** диаметр каната */
    public function ajax_get_weight_r($id_diam_r) {
        echo $this->DR_model->get_weight_r($id_diam_r);
    }
    /** вес каната на 1000 м */
    public function ajax_get_final_weight_r($id_diam_r, $id_core, $id_steep, $id_type) {
        echo $this->DR_model->get_final_weight_new($id_diam_r, $id_core, $id_steep, $id_type);
    }
    /** информация о диаметре и шаге свивки пряди */
    public function ajax_get_strand_info($id_gost_r, $id_diam_r) {
        echo json_encode( $this->DR_model->get_strand_info($id_gost_r, $id_diam_r) );
    }
    /** список операций */
    public function ajax_get_operations($id_goat, $id_diam_r){
        echo json_encode( $this->DR_model->get_strand_operation_byGost_diam_r($id_goat, $id_diam_r) );
    }
    /** --- для поддержки старой версии */

    public function getAllGost_r()
    {
        $res = $this->DR_model->getAllGost_r();

        foreach ($res as $key) {
            $arr[] = $key;
        }
        echo(json_encode($res)); /*AJAX jsonData*/
    }
    public function gost_r($gost_r)
    {
        $res = $this->DR_model->getDiameterForGost($gost_r);
       echo(json_encode($res)); /*AJAX jsonData*/
    }
    public function constr($gost_r)
    {
        $res = null;
        $res = $this->DR_model->getConstrForGost($gost_r);
        echo(json_encode($res)); /*AJAX jsonData*/
    }
    public function weight_r($id_diam_r)
    {
       $res = null;
        $res = $this->DR_model->getIdDiam($id_diam_r);
        echo(json_encode($res)); /*AJAX jsonData*/
    }
    public function spinup($id_diam_r)
    {
        $res = null;
        $res = $this->DR_model->getSpinup($id_diam_r);
        echo(json_encode($res)); /*AJAX jsonData*/
    }

    public function getPLYSpinup($id_diam_r)
    {
        $res = null;
        $res = $this->DR_model->getPLYSpinup($id_diam_r);
        echo(json_encode($res)); /*AJAX jsonData*/
    }

    /** информация о мет. сердечнике */
    public function ajax_get_met_core_info($id_gost_r, $diam_r) {
        echo json_encode( $this->DR_model->get_met_core_info($id_gost_r, $diam_r) );
    }
    /** шаг свивки сердечника */
    public function get_step_lay($id_diam_r, $param, $id_core = null, $id_gost_r = null, $diam_r = null)
    {
        $res = null;
//        if($id_core == 4)
//            $res =  $this->DR_model->get_core_step($id_gost_r, $diam_r, $id_diam_r, $param);
//        else $res = $this->DR_model->get_step_lay($id_diam_r, $param);
        $res = $this->DR_model->get_step_lay($id_diam_r, $param);
        echo(json_encode($res)); /*AJAX jsonData*/
    }
    public function get_core($id_diam_r)
    {
       $res = null;
        $res = $this->DR_model->get_core($id_diam_r);
        foreach ($res as $key) {
            $arr[] = $key;
        }
        echo(json_encode($res)); /*AJAX jsonData*/
    }
    public function get_diam_c($id_diam_r, $id_core)
    {
        $res = null;
        $res = $this->DR_model->get_diam_c($id_diam_r, $id_core);
        foreach ($res as $key) {
            $arr[] = $key;
        }
        echo(json_encode($res)); /*AJAX jsonData*/
    }
    public function get_final_weight($id_diam_r, $id_core, $id_steep, $id_type)
    {
        $res = null;
        $res = $this->DR_model->get_final_weight($id_diam_r, $id_core, $id_steep, $id_type);
        foreach ($res as $key) {
//            $arr[] = $key["final_weight"];
            $arr[] = $key;
        }
        echo(json_encode($res)); /*AJAX jsonData*/
    }
    public function get_steep($id_diam_r, $id_core)
    {
       $res = null;
        $res = $this->DR_model->get_steep($id_diam_r, $id_core);
        foreach ($res as $key) {
//            $arr[] = $key["name"];
            $arr[] = $key;
        }
        echo(json_encode($res)); /*AJAX jsonData*/
    }
    public function get_l_type($id_diam_r, $id_core)
    {
        $res = null;
        $res = $this->DR_model->get_l_type($id_diam_r, $id_core);
        foreach ($res as $key) {
            $arr[] = $key;
        }
        echo(json_encode($res)); /*AJAX jsonData*/
    }
    public function getSumVes($id_diam_r)
    {
        $res = null;
        $res = $this->DR_model->getSumVes($id_diam_r);
        echo(json_encode($res)); /*AJAX jsonData*/
    }
    public function get_reel()
    {
        $res = null;
        $res = $this->DR_model->get_reel();
        echo(json_encode($res)); /*AJAX jsonData*/
    }
    public function get_reel_all()
    {
        $res = null;
        $res = $this->DR_model->get_reel_all();
        echo(json_encode($res)); /*AJAX jsonData*/
    }
    public function get_opt_reel($diam_r, $length)
    {
        $res = null;
        $res = $this->DR_model->getReel_new();
        $result["name"] = "null";
        $result["weight"] = 0;
        $k_1 = 1;

        if (($diam_r > 0) && ($diam_r < 5)) {
            $k_1 = 5;
        }

        if (($diam_r >= 5) && ($diam_r < 10)) {
            $k_1 = 4;
        }

        if (($diam_r >= 10) && ($diam_r < 30)) {
            $k_1 = 3;
        }

        if (($diam_r >= 30) && ($diam_r < 40)) {
            $k_1 = 2;
        }

        if ($diam_r >= 40) {
            $k_1 = 1;
        }

        foreach ($res as $row) {
            $D_res = $row["diam_cheek"] - (2 * $k_1);
            $n_1 = $row["length_pin"] / ($diam_r * 1.01);
            $n_2 = round(($D_res - $row["diam_pin"]) / (2 * $diam_r));
            $d_mid = $row["diam_pin"] + ($diam_r * $n_2);
            $l = round(3.14 * $d_mid * $n_1 * $n_2 * 0.9 / 1000);
            if (($l > $length) && (($row["diam_pin"] / $diam_r) > 15)) {
                $result["weight"] = $row["weight"];
                $result["name"] = $row["name"];
                break;
            }
        }
        foreach ($result as $key) {
            $arr[] = $key;
        }
        die(json_encode($result));
    }
    public function checkZero($id)
    {
        if ($id == 0 or $id == "0") {
            $this->form_validation->set_message('checkZero', 'Поле %s должно принимать какое либо значение"');
            return false;
        } else {
            return true;
        }
    }
    public function ajax()
    {
        $res = $this->DR_model->getAllGost_r();
        foreach ($res as $key) {
            $arr[] = $key["gost_r"];
        }
        die(json_encode($arr));
    }
    public function diam()
    {
        $res = $this->DR_model->getDiameterForGost();
        foreach ($res as $key) {
            $arr[] = $key["diam_r"];
        }
        die(json_encode($arr));
    }
}
