<style>[v-cloak] { display: none; }</style>

<div class="container" id="cont" v-cloak>

    <div style="text-align: center;">
        <div v-show="msg_error.length != 0" class="alert alert-dismissable alert-danger" style="display: none">
            <span>{{msg_error}}</span>
            <i class="close fa fa-times" aria-hidden="true" v-on:click="close"></i>
        </div>
        <div v-show="msg_success.length != 0" class="alert alert-dismissable alert-success" style="display: none">
            <span>{{msg_success}}</span>
            <i class="close fa fa-times" aria-hidden="true" v-on:click="close"></i>
        </div>
    </div>

    <h3 align="center"> Редактирование справочника канатов </h3>

    <div class="control-group">
        <label class="control-label" id="help" for="gost_r">Стандарт (код)</label>
        <div class="controls">
            <select2 name="gost_r" id="gost_r" style="width: 410px"
                     :options="gost_r_list"
                     v-on:change="gost_r_change"></select2>
            <span>{{construct}}</span>
            <span class="pull-right"> <b>Поиск Ø </b>
                <input type="number" v-model.number="diam_search" class="input-small">
            </span>
        </div>
    </div>

    <ul class="nav nav-tabs" role="tablist" id="myTab" style="margin-top: 20px;">
        <li role="presentation" class="active"><a href="#tab1" role="tab" data-toggle="tab">Канаты</a></li>
        <li role="presentation"><a href="#tab2" role="tab" data-toggle="tab">Сердечник</a></li>
        <li role="presentation"><a href="#tab3" role="tab" data-toggle="tab" v-on:click="strand_tab">Пряди</a></li>
        <li role="presentation"><a href="#tab4" role="tab" data-toggle="tab">Операции</a></li>
    </ul>

    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="tab1">
            <input type="button" class="btn btn-success" value="сохранить" style="margin-bottom: 10px"
                   v-on:click="save_r" :disabled="disabled_r" />
            <table id="data_r" class="display compact" cellspacing="0" cellpadding="5" style="width: 100%">
                <thead>
                    <tr>
                        <th>Ø каната</th>
                        <th>шаг свивки каната</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(item, index) in data_r" v-if="item.diam_r.indexOf(diam_search) != -1 || diam_search.length == 0">
                        <td>{{item.diam_r}}</td>
                        <td>
                            <input type="number" :id="'data_r_1_'+index" :value="item.step_r" v-model="item.step_r"/>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div role="tabpanel" class="tab-pane" id="tab2">
            <input type="button" class="btn btn-success" value="сохранить" style="margin-bottom: 10px"
                   v-on:click="save_c" :disabled="disabled_c" />
            <table id="data_c" class="display compact" cellspacing="0" cellpadding="5" style="width: 100%">
                <thead>
                    <tr>
                        <th>Ø каната</th>
                        <th>тип сердечника</th>
                        <th>Ø сердечника</th>
                        <th>шаг свивки сердечника</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(item, index) in data_c" v-if="item.diam_r.indexOf(diam_search) != -1 || diam_search.length == 0">
                        <td>{{item.diam_r}}</td>
                        <td>{{item.core}}</td>
                        <td>
                            <input type="number" :id="'data_c_2_'+index" :value="item.diam_core" v-model="item.diam_core"/>
                        </td>
                        <td>
                            <input type="number" :id="'data_c_3_'+index" :value="item.step_c" v-model="item.step_c" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div role="tabpanel" class="tab-pane" id="tab3">
            <input type="button" class="btn btn-success" value="сохранить" style="margin-bottom: 10px"
                   :disabled="disabled_s" v-on:click="save_s" />
            <table id="strand" class="display compact" cellspacing="0" cellpadding="5" style="width: 100%">
                <thead>
                    <tr>
                        <th>Ø каната</th>
                        <th>конст-ия пряди</th>
                        <th>название</th>
                        <th>Ø пряди</th>
                        <th>шаг свивки пряди</th>
                    </tr>
                </thead>
<!--                <template v-for="(item, index_s, key) in data_s" v-if="item.diam_r.indexOf(diam_search) != -1 || diam_search.length == 0">-->
                <template v-for="(item, index_s, key) in data_s">
                    <my-tr :strand="item"></my-tr>
                </template>
            </table>
        </div>
        <div role="tabpanel" class="tab-pane" id="tab4">
            <table class="display compact" style="margin-bottom: 5px;"  cellspacing="0" cellpadding="5" style="width: 100%">
                <tr>
                    <th></th>
                    <th>Прядь</th>
                    <th>Конструкция</th>
                    <th>Операция</th>
                </tr>
                <tr>
                    <td><input type="button" v-on:click="operation_add" class="btn btn-success" value="Добавить" style="margin: 0" /></td>
                    <td><select2 name="strand_cons" id="strand_cons" :options="strand_cons_list"
                                    v-on:change="operation_cons_change" ></select2></td>
                    <td><input type="text" v-model="operation_cons" style="margin: 0; border-radius: 6px;"></td>
                    <td><select2 name="operation" id="operation" :options="operation_list"
                                    v-on:change="operation_list_change"></select2></td>
                </tr>
                <tr><th></th><th></th><th><span style="color: red">{{error_operation_cons}}</span></th><th></th></tr>
            </table>
            <table id="operation" class="display compact" cellspacing="0" cellpadding="5" style="width: 100%"
                v-if="str_operation_list.length > 0">
                <tbody>
                    <tr v-for="(item, index) in str_operation_list" :key="item.str_oper_id">
                        <td style="width: 120px;">
                            <a rel="#" v-on:click="oparetion_del(index)">del</a> / edit
                        <td style="width: 220px;">{{strand_cons_list2[item.strand_const_id]}}
<!--                            <select2 :id="'scl_'+index" :options="strand_cons_list" :value="item.strand_const_id"></select2>-->
                        <td><input readonly type="text" :value="item.construction"></td>
                        <td><select2 disabled :id="'ol_'+index" :options="operation_list" :value="item.operation_id"></select2>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<link href="/css/select2.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/js/select2.js"></script>
<script type="text/javascript" src="/js/vue.js"></script>
<script type="text/javascript" src="/js/vue-select2.js"></script>

<link href="/css/jquery.dataTables.css" rel="stylesheet">
<link href="/css/keyTable.bootstrap.css" rel="stylesheet">
<script type="text/javascript" src="/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="/js/dataTables.keyTable.js"></script>
<script type="text/javascript" src="/js/langDataTable.js"></script>
<script>
    const DATA_ADD_SUCCES = 'Данные успешно сохранены';
    const DATA_DEL_SUCCES = 'Не удалось сохранить данные';
    function toArray(obj){ // преобразуем объект в массив
        var res = [];
        res.push(obj);
        return res;
    }

    var num = 0;
    var diam_r = null;
    Vue.component('my-tr', {
        props: {
            strand:{
                default: null
            },
        },
        render: function(el) {
//            log(num++);
//            log(this.strand);
            var item = [];
            item[0]= this.strand;
            return el('tbody', item.map(
                function(row) {
                    log(row.diam_r + ' --- ' + diam_r);
                    if(diam_r != row.diam_r) diam_r = row.diam_r;
                    //else diam_r = null;
                    return el('tr',
                        [
                            el('td', diam_r),
                            el('td', row.const),
                            el('td', row.strand_r_name),
                            el('td', row.diam_s),
                            el('td', row.step_s),
                        ]);
                })
            )
        },
    });

    var vm = new Vue({
        el: '#cont',
        data: {
            disabled_r: false,
            disabled_c: false,
            disabled_s: false,
            construct: '<?= $data["construct"]; ?>', // конструкция каната
            gost_r_list: <?= json_encode($data["gost_r_list"]); ?>, // стандарт
//            data_r: <?//= json_encode($data["data_r"]); ?>//, // канат
            data_r: null,
//            data_c: <?//= json_encode($data["data_c"]); ?>//, // сердечник
            data_c: null, // сердечник
//            data_s: <?//= json_encode($data["data_s"]); ?>//, // пряди
            data_s: null,
            operation_list: <?= json_encode($data["operation_list"]); ?>, // список доступных операций
            operation_list_id: [], // список по ID + название
            selected_operation: <?= $data["selected_operation"]; ?>, // выбранная операция
            strand_operation_info: <?= $data["strand_operation_info"]; ?>, // список операций с прядями
            strand_operation_info_lng: 0,
            operation_cons: null, // конструкция операции
            strand_cons_list: null, // список прядей (в виде конструкции) в канате для select2
            strand_cons_list2: [], // список прядей (в виде конструкции) в канате
            selected_strand_cons: null, // выбранная прядь
            str_operation_list: [], // список уже существующих операций в прядях
            selected_id_gost: null,
            msg_success: '',
            msg_error: '',
            error_operation_cons: '',
            diam_search: '',
            operation: false,
        },
        mounted(){
            $('#gost_r').trigger('change');
            this.operation_list.forEach((operation) => {
                this.operation_list_id[operation.id] = operation.text;
            });
        },
        methods: {
            row_focus(event){
            },
            strand_tab(){
                if(this.operation == true) {
                    this.get_strand_operation_info(this.selected_id_gost);
                }
                this.operation = false;
            },
            close() {
                this.msg_success = '';
                this.msg_error = '';
            },
            gost_r_change(id_gost, index, arr) { // выбор нового стандарта
                num = 0;
                diam_r = null;
                this.selected_id_gost = id_gost;
                this.error_operation_cons = '';
                this.construct = '';
                this.get_diam(id_gost);
                this.construct = arr.construct;
                this.get_strand_operation(id_gost);
                this.get_strand_cons(id_gost);
                this.get_strand_operation_info(id_gost);
            },
            operation_list_change(id) {
                this.selected_operation = id;
            },
            operation_cons_change(id) {
                this.selected_strand_cons = id;
            },
            get_diam(id_gost) { // получить список диаметров
                var that = this;
                $.ajax({
                    type: 'POST',
                    url: base_url +'/edit/ajax_get_data_for_edit',
                    dataType: 'JSON',
                    data: { 'id_gost_r': id_gost },
                    async: false,
                    success: function(data) {
                        that.data_r = data.data_r;
                        that.data_c = data.data_c;
                        that.data_s = data.data_s;
                    },
                    error: function(e) {
                        console.error('ошибка загрузки /edit/ajax_get_data_for_edit/ Функция get_diam');
                    }
                });
            },
            get_diam_r(id_gost) { // получить список диаметров каната
                var that = this;
                $.ajax({
                    type: 'POST',
                    url: base_url +'/edit/ajax_get_data_r',
                    dataType: 'JSON',
                    data: { 'id_gost_r': id_gost },
                    async: false,
                    success: function(data) {
                        that.data_r = data.data_r;
                    },
                    error: function(e) {
                        console.error('ошибка загрузки /edit/ajax_get_data_r/ Функция get_diam_r');
                    }
                });
            },
            get_diam_c(id_gost) { // получить список диаметров каната
                var that = this;
                $.ajax({
                    type: 'POST',
                    url: base_url +'/edit/ajax_get_data_c',
                    dataType: 'JSON',
                    data: { 'id_gost_r': id_gost },
                    async: false,
                    success: function(data) {
                        that.data_c = data.data_c;
                    },
                    error: function(e) {
                        console.error('ошибка загрузки /edit/ajax_get_data_c/ Функция get_diam_c');
                    }
                });
            },
            get_diam_s(id_gost) { // получить список диаметров каната
                var that = this;
                $.ajax({
                    type: 'POST',
                    url: base_url +'/edit/ajax_get_data_s',
                    dataType: 'JSON',
                    data: { 'id_gost_r': id_gost },
                    async: false,
                    success: function(data) {
                        that.data_s = data.data_s;
                    },
                    error: function(e) {
                        console.error('ошибка загрузки /edit/ajax_get_data_s/ Функция get_diam_s');
                    }
                });
            },
            get_strand_operation(id_gost) {
                $.post(base_url +'/edit/ajax_get_strand_operation/'+id_gost, function(data) {
                    vm.str_operation_list = null;
                    vm.str_operation_list = JSON.parse(data);
                    vm.strand_operation_info_lng = Object.keys(vm.str_operation_list).length;
                }).fail(function() {
                    console.error('ошибка загрузки /edit/ajax_get_strand_operation/ Функция get_strand_operation');
                });
            },
            get_strand_cons(id_gost) {
                $.post(base_url +'/edit/ajax_get_strand_cons/'+id_gost, function(data) {
                    vm.strand_cons_list = JSON.parse(data);
                    vm.strand_cons_list2 = [];
                    for(var item in vm.strand_cons_list){
                        vm.strand_cons_list2[vm.strand_cons_list[item].id] = vm.strand_cons_list[item].text;
                    }
                    vm.selected_strand_cons = vm.strand_cons_list[0].id;
                }).fail(function() {
                    console.error('ошибка загрузки /edit/ajax_get_strand_cons/ Функция get_strand_cons');
                });
            },
            get_strand_operation_info(id_gost) {
                $.post(base_url +'/edit/ajax_get_strand_operation_info_byGost/'+id_gost, function(data) {
                    vm.strand_operation_info = null;
                    vm.strand_operation_info = JSON.parse(data);
                    var key = Object.keys(vm.strand_operation_info);
                    var tmp = {};
                    if(vm.strand_operation_info_lng > 0){
                        tmp = vm.strand_operation_info;
                        for(var index in vm.data_r){ // создаем новый массив
                            if( key.indexOf(vm.data_r[index].id_diam_r) == -1 ) { // если еще нет сохраненных данных
                                tmp[vm.data_r[index].id_diam_r] = {};
                                for (var oper in vm.str_operation_list) {
                                    tmp[vm.data_r[index].id_diam_r][oper] = {
                                        diam_r_id: vm.data_r[index].id_diam_r,
                                        diam_so: '',
                                        steep_so: '',
                                        strand_oper_id: vm.str_operation_list[oper].str_oper_id,
                                        operation_id: vm.str_operation_list[oper].operation_id,
                                        strand_const_id: vm.str_operation_list[oper].strand_const_id,
                                        construction: vm.str_operation_list[oper].construction
                                    }
                                }
                            } else {
                                var id = vm.data_r[index].id_diam_r;
                                var length = Object.keys(tmp[id]).length;
                                var count = 1;
                                if(length != vm.strand_operation_info_lng) {
                                    for (var oper in vm.str_operation_list) {
                                        if(count > length) {
                                            tmp[id][oper] = {
                                                diam_r_id: vm.data_r[index].id_diam_r,
                                                diam_so: '',
                                                steep_so: '',
                                                strand_oper_id: vm.str_operation_list[oper].str_oper_id,
                                                operation_id: vm.str_operation_list[oper].operation_id,
                                                strand_const_id: vm.str_operation_list[oper].strand_const_id,
                                                construction: vm.str_operation_list[oper].construction
                                            }
                                        }
                                        count++;
                                    }
                                }
                            }
                        }
                    }
                }).fail(function() {
                    console.error('ошибка загрузки /edit/ajax_get_strand_operation_info_byGost/ Функция get_strand_operation_info');
                });
            },
            save_r() { // сохранить данные по канату
                this.disabled_r = true;
                this.close();
                $.post(base_url +'/edit/ajax_save_r/', {data: vm.data_r})
                .done(function(data) {
                    if(data == true) vm.msg_success = DATA_ADD_SUCCES;
                    else vm.msg_error = DATA_DEL_SUCCES;
                    vm.get_diam_r(vm.selected_id_gost);
                    vm.disabled_r = false;
                })
                .fail(function() {
                    vm.msg_error = DATA_DEL_SUCCES;
                    vm.disabled_r = false;
                    console.error('ошибка загрузки /edit/ajax_save_r/ Функция save_r');
                });
            },
            save_c() { // сохранить данные по сердечнику
                this.disabled_c = true;
                this.close();
                $.post(base_url +'/edit/ajax_save_c/', {data: vm.data_c})
                .done(function(data) {
                    if(data == true) vm.msg_success = DATA_ADD_SUCCES;
                    else vm.msg_error = DATA_DEL_SUCCES;
                    vm.get_diam_c(vm.selected_id_gost);
                    vm.disabled_c = false;
                })
                .fail(function() {
                    vm.msg_error = DATA_DEL_SUCCES;
                    vm.disabled_c = false;
                    console.error('ошибка загрузки /edit/ajax_save_c/ Функция save_c');
                });
            },
            save_s() { // сохранить данные по прядям
                this.disabled_s = true;
                this.close();
                $.post(base_url +'/edit/ajax_save_s/', {data_s: JSON.stringify(vm.data_s), data_oper: JSON.stringify(vm.strand_operation_info)})
                .done(function(data) {
                    if(data == true) vm.msg_success = DATA_ADD_SUCCES;
                    else vm.msg_error = DATA_DEL_SUCCES;
                    vm.get_diam_s(vm.selected_id_gost);
                    vm.get_strand_operation_info(vm.selected_id_gost);
                    vm.disabled_s = false;
                })
                .fail(function() {
                    vm.msg_error = DATA_DEL_SUCCES;
                    vm.get_diam_s(vm.selected_id_gost);
                    vm.get_strand_operation_info(vm.selected_id_gost);
                    vm.disabled_s = false;
                    console.error('ошибка загрузки /edit/ajax_save_s/ Функция save_s');
                });
            },
            operation_add() { // добавление операции к прядям
                this.close();
                if(this.operation_cons === null || this.operation_cons.trim() === '') this.error_operation_cons = 'Укажите конструкцию';
                else {
                    this.error_operation_cons = '';
                    var obj = {
                        'strand_const_id': vm.selected_strand_cons,
                        'operation_id': vm.selected_operation,
                        'construction': vm.operation_cons
                    };
                    $.post(base_url +'/edit/ajax_add_operation_to_strand/', {data: obj})
                        .done(function(data) {
                            if(data !== false) {
                                vm.operation = true;
                                vm.get_strand_operation(vm.selected_id_gost);
                                vm.get_strand_operation_info(vm.selected_id_gost);
                            }
                            else vm.msg_error = '';
                        })
                        .fail(function() {
                            vm.msg_error = DATA_DEL_SUCCES;
                            console.error('ошибка загрузки /edit/ajax_add_operation_to_strand/ Функция operation_add');
                        });
                }
            },
            oparetion_del(index) {
                if(confirm('Также будут удалены связанные данный из раздела ПРЯДИ! Удалить?')) {
                    var arr = this.str_operation_list[index];
                    $.post(base_url + '/edit/ajax_del_operation_in_strand/' + arr.str_oper_id, function (data) {
                        if (data == false) vm.msg_error = 'Не удалось удалить запись';
                        else {
                            vm.operation = true;
                            vm.get_strand_operation(vm.selected_id_gost);
                            vm.get_strand_operation_info(vm.selected_id_gost);
                        }
                    })
                        .fail(function () {
                            vm.msg_error = 'Не удалось удалить запись';
                            console.error('ошибка загрузки /edit/ajax_del_operation_in_strand/ Функция oparetion_del');
                        });
                }
            },
        },
    });

    function log(data) { console.log(data); }
</script>

<script>
    $(document).ready(function () {
        var hash = window.location.hash;
        hash && $('ul.nav-tabs a[href="' + hash + '"]').tab('show');
    });
</script>

<style>
    .select_text > .select2-container--default .select2-selection--single
    .select2-selection__rendered  {
        font-style: italic;
    }
    .select_text > .select2-container--default .select2-selection--single {
        border: 0;
    }
    .select_text > .select2-container--default .select2-selection--single
    .select2-selection__arrow {
        display: none;
    }
    .select_text > .select2-container--default.select2-container--disabled
    .select2-selection--single {
        background-color: #fff;
        cursor: default;
    }
    a { cursor: pointer }
    .dataTables_wrapper .dataTables_filter input {
        position: absolute;
        right: 0px;
        top: -40px;
    }
    table.dataTable th.focus, table.dataTable td.focus {
        background-color: rgba(52, 124, 186, 0.27) !important;
        box-shadow: inset 0 1px 1px rgba(52, 124, 186, 0.57);
        outline: 1px solid rgba(52, 124, 186, 0.27) !important;
        outline-offset: -1px !important;
    }
    table.dataTable.compact tbody th,
    table.dataTable.compact tbody td { padding: 1px; }
    input[type="number"] { margin-bottom: 0; }

</style>