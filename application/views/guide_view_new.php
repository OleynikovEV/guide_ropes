<div class="container" id="cont" v-cloak>
    <h3 align="center">Полуфабрикат для изготовления канатов. Вес канатов.
        <a style="font-size:24px" style="color: black" href="#win2" id="reel_table"
            v-on:click="show_reel_all"> Барабаны</a>
    </h3>
    <!--  блок 1  -->
    <table width="100%" cellspacing="0" cellpadding="5">
        <tr>
            <td valign="top" style="width: 420px;">
                <div class="control-group">
                    <label class="control-label" id="help" for="gost_r">Стандарт (код)</label>
                    <div class="controls">
                        <select2 name="gost_r" id="gost_r" style="width: 100%"
                                 :options="gost_r_list"
                                 v-on:change="gost_r_change"></select2><br>
                        {{gost}}
                    </div>
                </div>
            </td>

            <td valign="top" style="width: 170px;">
                <div class="control-group second">
                    <label class="control-label" for="step_r">Шаг свивки сердечника</label>
                    <div class="controls">
                        <input type="text" disabled name="step_s" id="step_s"
                               :value="(step_s > 0) ? step_s : null">
                    </div>
                </div>
            </td>

            <td valign="top">
                <div class="control-group">
                    <label class="control-label" for="step_r">Шаг свивки каната</label>
                    <div class="controls">
                        <input type="text" disabled name="step_r" id="step_r"
                               :value="(step_r > 0) ? step_r : null">
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <!--  блок 2  -->
    <table width="100%" cellspacing="0" cellpadding="5">
        <tr>
            <td valign="top" style="width: 420px;">
                <div class="control-group">
                    <label class="control-label" for="constr">Конструкция</label>
                    <div class="controls">
                        <input style="width:410px" type="text" disabled :value="construct">
                    </div>
                </div>
            </td>

            <td valign="top" style="width: 170px;">
                <div class="control-group second">
                    <label class="control-label" for="core">Выбор сердечника</label>
                    <div class="controls">
                        <select2 name="core" id="core"
                                 :options="core_list"
                                 v-on:change="core_change"></select2>
                    </div>
                </div>
            </td>

            <td valign="top">
                <div class="control-group">
                    <label class="control-label" for="lubr_type">Способ смазки</label>
                    <div class="controls">
                            <select2 name="lubr_type" id="lubr_type" style="width: 220px"
                                     :options="lubr_type_list"
                                     v-on:change="lubr_type_change"></select2>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <!--  блок 3  -->
    <table width="100%" cellspacing="0" cellpadding="5">
        <tr>
            <td valign="top" style="width: 420px;">
                <div class="control-group">
                    <label class="control-label" for="diam_r">Диаметр каната</label>
                    <div class="controls">
                            <select2 name="diam_r" id="diam_r" style="width: 100%"
                                     :options="diam_r_list"
                                     v-on:change="diam_r_change"></select2>
                    </div>
                </div>
            </td>

            <td valign="top" style="width: 170px;">
                <div class="control-group second">
                    <label class="control-label" for="core">Диаметр сердечника</label>
                    <div class="controls">
                        <input type="text" disabled name="diam_core" id="diam_core"
                               :value="(diam_core > 0) ? diam_core : null">
                    </div>
                </div>
            </td>

            <td valign="top">
<!--                <div class="control-group" v-show="core_construct.length > 0">-->
<!--                    <label class="control-label" for="core">Конструкция сердечника</label>-->
<!--                    <div class="controls">-->
<!--                        <input type="text" disabled name="core_const" id="core_const"-->
<!--                               :value="core_construct" style="width: 100%;">-->
<!--                    </div>-->
<!--                </div>-->
            </td>
        </tr>
    </table>
    <!--  блок 4  -->
    <table width="100%" cellspacing="0" cellpadding="5">
        <tr>
            <td valign="top" style="width: 420px;">
                <div class="control-group">
                    <label class="control-label" for="constr">Вес полуфабриката (1000m)</label>
                    <div class="controls">
                        <input style="width:410px" type="text" disabled name="weight_r" id="weight_r"
                               :value="weight_r">
                    </div>
                </div>
            </td>

            <td valign="top" style="width: 170px;">
                <div class="control-group second">
                    <label class="control-label" for="steep">Пропитка</label>
                    <div class="controls">
                            <select2 name="steep" id="steep"
                                     :options="steep_list"
                                     v-on:change="steep_change"></select2>
                    </div>
                </div>
            </td>

            <td valign="top">
                <div class="control-group">
                    <label class="control-label" for="final_weight">Вес каната (1000m)</label>
                    <div class="controls">
                        <input type="text" disabled name="final_weight" id="final_weight"
                               :value="final_weight">
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <!--  блок 5  -->
    <table width="100%" cellspacing="0" cellpadding="5">
        <tr>
            <td valign="top" style="width:33%">
                <a href="#win_img" v-on:click="show_big_img" style="cursor: pointer">
                    <img :src="img" style="width: 200px; max-height: 200px; margin-left: 30px">
                </a>
            </td>

            <td valign="top">
                <div class="control-group">
                    <label class="control-label" for="Real_ves">Длина заданная</label>
                    <div class="controls">
                        <input style="width:350px" type="number" name="Real_ves" id="Real_ves"
                               v-model.number="input_length">
                    </div>
                </div>

                <div class="control-group">
                    <div class="control-group">
                        <input style="width:350px" type="button" id="b_ves"
                               v-on:click="calculate"
                               :disabled="(input_length > 0) ? false : true"
                               value="Рассчитать">
                    </div>
                </div>
            </td>

            <td valign="top" style="width:33%"></td>
        </tr>
    </table>
    <!--  блок 6  -->
    <table width="100%" cellspacing="0" cellpadding="5">
        <tr>
            <td valign="top" style="width:50%">
                <div class="control-group">
                    <label class="control-label" for="constr">Вес полуфабриката (заданный)</label>
                    <div class="controls">
                        <input style="width:350px" type="text" disabled name="weight_calc" id="weight_calc"
                               :value="weight_sf_calc">
                    </div>
                </div>
            </td>

            <td>
                <div class="control-group">
                    <label class="control-label" for="constr">№ барабана (вес)</label>
                    <div class="controls">
                        <input style="width:350px" type="text" disabled name="reel" id="reel"
                               :value="reel_info">
                    </div>

                </div>
            </td>

            <td valign="top" style="width:50%">
                <div class="control-group">
                    <label class="control-label" for="constr">Вес каната (заданный)</label>
                    <div class="controls">
                        <input style="width:350px" type="text" disabled name="weight_calc_final" id="weight_calc_final"
                               :value="weight_rope_calc">
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <!--  блок 7  -->
    <div class="table">
        <table white-space="nowrap">
            <tr v-if="core_construct.length > 0">
                <th nowrap="nowrap">Мет. сердечник</th>
                <td style="padding-right: 25px; border-left: 1px solid #EEEEEE" colspan="10">
                    {{core_construct}} (Ø {{met_core.diam_s}}  шаг-{{met_core.step_s}})
                </td>
            </tr>
            <tr>
                <th nowrap="nowrap">Элементы каната</th>
                <th v-for="(item, index) in strand" nowrap="nowrap"
                    :colspan="separator[index]"
                    style="padding-right: 25px; border-left: 1px solid #EEEEEE"> {{item.name}} </th>
            </tr>
<!--            <tr v-if="amount_operation() > 0">-->
            <tr v-if="amount_operations > 0">
                <th>Операции</th>
                <td v-for="(item, index) in strand_info" nowrap="nowrap" :colspan="separator[index]" style="border-left: 1px solid #EEEEEE">
                    <template v-for="oper, id in operation_list" v-if="item.id_strand_const == oper.strand_const_id">
                        <div>Ø{{oper.diam_so}}&nbsp;&nbsp;шаг-{{oper.steep_so}}
                            &nbsp;&nbsp;<b style="color: #CC0000">(&nbsp;</b>
                            {{oper.construction}}<b style="color: #CC0000">&nbsp;)</b>
                            &nbsp;&nbsp;{{oper.oper_name}}&nbsp;&nbsp;
                        </div>
                    </template>
                </td>
            </tr>
            <tr>
                <th>Свивка пряди</th>
                <td v-for="(item, index) in strand_info" nowrap="nowrap" :colspan="separator[index]" style="padding-right: 25px; border-left: 1px solid #EEEEEE">
                    Ø{{item.diam_s}}&nbsp;&nbsp;шаг-{{item.step_s}}
                </td>
            </tr>
            <tr>
                <th>Конструкция</th>
                <td v-for="item in constr" nowrap="nowrap" v-if="item != ''" v-html="item"></td>
            </tr>
            <tr>
                <th>Диаметр проволоки</th>
                <template v-for="item in diam_w" nowrap="nowrap" >
                    <td></td>
                    <td class="border_right">{{item}}</td>
                </template>
            </tr>
            <tr>
                <th>% увивки проволоки<br>(в канате)</th>
                <template v-for="item in length_ratio" nowrap="nowrap" >
                    <td></td>
                    <td class="border_right">{{item}}</td>
                </template>
            </tr>
<!--            <tr>-->
<!--                <th>% увивки проволоки<br>(в пряди)</th>-->
<!--                <template v-for="(item, pos) in length_ratio" nowrap="nowrap" >-->
<!--                    <td></td>-->
<!--                    <td class="border_right">-->
<!--                        <span v-if=" pos == 0">1</span>-->
<!--                        <span v-else>{{(item / length_ratio[0]).toFixed(3)}}</span>-->
<!--                    </td>-->
<!--                </template>-->
<!--            </tr>-->
            <tr>
                <th>Вес(1000m)</th>
                <template v-for="item in weight" nowrap="nowrap" >
                    <td></td>
                    <td class="border_right">{{item}}</td>
                </template>
            </tr>
            <tr>
                <th>Процент проволоки</th>
                <template v-for="item in procent_w" nowrap="nowrap" >
                    <td></td>
                    <td class="border_right">{{item}}</td>
                </template>
            </tr>
            <!-- Заданный вес -->
            <tr v-if="lng_input != null">
                <th>Заданный вес</th>
                <template v-for="item in lng_input" nowrap="nowrap" >
                    <td></td>
                    <th class="border_right">{{item}}</th>
                </template>
            </tr>
        </table>
    </div>
    <!--  блок 8  -->
    <div class="table" >
        <table>
            <tr>
                <th>Сумма по диаметрам проволоки</th>
                <td v-for="item in sum_diam_w" class="border_right">{{item}}</td>
            </tr>
            <tr>
                <th>Вес(1000m)</th>
                <td v-for="item in sum_weight" class="border_right">{{item}}</td>
            </tr>
            <tr>
                <th>Процент проволоки</th>
                <td v-for="item in sum_procent_w" class="border_right">{{item}}</td>
            </tr>
            <tr v-if="sum_lng_input != null">
                <th>Заданный вес</th>
                <th v-for="item in sum_lng_input" class="border_right">{{item}}</th>
            </tr>
        </table>
    </div>

    <!-- Список барабанов -->
    <a href="#" class="overlay" id="win2"></a>
    <div class="popup">
        <a class="close" title="Закрыть" href="#close"></a>
        <h3 align="center">Таблица барабанов</h3>
        <center>
            <table v-if="reel != null" border="1" style="text-align: center">
                <tr>
                    <th>№</th> <th>Диам. щеки</th> <th>Длина шейки</th> <th>Вес без оп.*</th> <th>Вес с оп.*</th>
                </tr>
                <tr v-for="(item, n) in reel.name">
                    <th>{{item}}</th>
                    <td>{{reel.diam_cheek[n]}}</td>
                    <td>{{reel.length_pin[n]}}</td>
                    <td>{{reel.weight[n]}}</td>
                    <td>{{reel.weight_w[n]}}</td>
                </tr>
            </table>
        </center>
        <h4 align="center">*"оп." - опалубка</h4>
    </div>

    <a href="#" class="overlay" id="win_img"></a>
    <div class="popup">
        <a class="close" title="Закрыть" href="#close"></a>
        <center>
            <img :src="img" style="width: 100%">
        </center>
    </div>

</div>
<style>
    .second input, .second select {
        width: 150px;
    }
    [v-cloak] { display: none; }
    .table thead > tr > th, .table tbody > tr > th,
    .table tfoot > tr > th, .table thead > tr > td,
    .table tbody > tr > td, .table tfoot > tr > td {
        padding: 8px 4px;
    }
    .border_right { border-right: 1px solid #EEEEEE }
    .border_right:last-child { border-right: 0 }
</style>
    <link href="/css/select2.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="/js/select2.js"></script>
    <script type="text/javascript" src="/js/vue.js"></script>
    <script type="text/javascript" src="/js/vue-select2.js"></script>
    <script>
        function log(data) { console.log(data); }
        var vm = new Vue({
            el: '#cont',
            data: {
                img: '',
                construct: '',
                gost: '',
                step_s: '',
                step_r: '',
                id_diam_r: 0,
                id_gost_r: 0,
                diam_r: 0,
                id_core: 0,
                id_steep: 0,
                id_lubr_type: 0,
                diam_core: 0,
                weight_r: 0,
                final_weight: 0,
                input_length: '',
                reel_info: '',
                weight_sf_calc: '', // вес полуфабриката - расчетный
                weight_rope_calc: '', // вес каната - расчетный
                core_construct: '', // конструкция сердечника

                strand: null, // виды операций
                strand_info: null, // виды операций
                constr: null, // конструкция каната
                diam_w: null, // диам. проволоки в канате
                length_ratio: null, // процент цвивки проволоки в канате
                weight: null, // вес проволоки
                procent_w: null, // проволока в процентах
                lng_input: null, // расчетная длина каната
                separator: [], // разделитель для операций
                reel: null, // список барабанов
                operation_list: null,
                met_core: { diam_s: null, step_s: null },

                // сумма по диаметрам проволок
                sum_diam_w: null,
                sum_weight: null,
                sum_procent_w: null,
                sum_lng_input: null,

                gost_r_list: <?= json_encode($data['gost_r_list']); ?>,
                diam_r_list: null,
                core_list: null,
                lubr_type_list: null,
                steep_list: null,

                final_weight: false,
            },
            watch: {
                constr: function(newVal){
                    var regexp = /\d <b>/ig;
                    var count = 0;
                    this.lng_input = null;
                    this.separator = [];
                    newVal.forEach(function(item, ind){
                        if( regexp.exec(item) != null && ind != 0) {
                            vm.separator.push(count);
                            count =0;
                        }
                        count++;
                    });
                    this.separator.push(count);
                }
            },
            mounted() {
                //console.clear();
                $('select').select2();
                $('#gost_r').trigger('change');
            },
            computed: {
                amount_operations: function() {
                    if(this.operation_list == null || this.operation_list == undefined)
                        return 0;
                    else return Object.keys(this.operation_list).length;
                }
            },
            methods: {
                // СОБЫТИЯ
                show_big_img() { // увеличить картинку

                },
                show_reel_all() {
                    this.get_all_reel();
                },
                getImage: function(url){
                    return new Promise(function(resolve, reject){
                        var img = new Image();
                        img.onload = function(){
                            resolve(url);
                        };
                        img.onerror = function(){
                            reject(url);
                        };
                        img.src = url;
                    });
                },
                gost_r_change(id_gost, index, arr) { // выбор нового стандарта
                    this.gost = arr.text;
                    var date = new Date();
                    var ver = date.getHours() + date.getMinutes() + date.getSeconds() + date.getMilliseconds();
                    this.img_load_error = false;
                    this.id_gost_r = id_gost;
                    this.id_steep = 0;
                    if(arr.sts.length > 0) {
                        var src = 'images/sts/'+arr.id+'.png?'+ver;
                        this.getImage(src).then(function(){
                            vm.img = src;
                        }).catch(function(){
                            vm.img = null;
                            src = 'images/sts/' + arr.sts.replace(/\s/g, '') + '.png?'+ver;
                            vm.getImage(src).then(function(){
                                vm.img = src;
                            }).catch(function() {
                                vm.img = null;
                            })
                        });
                    }
                    else this.img = 'images/sts/'+arr.id+'.png?'+ver;
                    this.construct = arr.construct;
                    this.get_diam(id_gost);
                    this.constr_to_array(this.construct);
                    this.get_strand(id_gost);
                    this.core_const(id_gost);
                },
                diam_r_change(id_diam, index, arr) { // выбор диаметра
                    this.id_steep = 0;
                    this.id_diam_r = id_diam;
                    this.diam_r = arr.text;
                    this.get_operations(this.id_gost_r, arr.text);
                    if( arr !== undefined ) this.diam_r = arr.text;
                    this.get_step_s(id_diam, this.id_core, this.id_gost_r, this.diam_r);
                    this.get_step_r(id_diam);
                    this.get_met_core_info(this.id_gost_r, this.diam_r);
                    this.get_core(id_diam);
                    this.get_weight_r(id_diam);
                    this.get_tab1(id_diam);
                    this.get_tab2(id_diam);
                    this.get_strand_info(this.id_gost_r, arr.text);
//                    this.get_strand_info(this.id_gost_r, id_diam);

                    // обнуляем поля
                    this.reel_info = '';
                    this.weight_sf_calc = '';
                    this.weight_rope_calc = '';
                    this.input_length = '';
                },
                core_change(id_core) {
                    this.id_core = id_core;
                    this.get_diam_core(this.id_diam_r, id_core, this.id_gost_r, this.diam_r);
                    this.get_lubr_type(this.id_diam_r, id_core);
                    this.get_steep(this.id_diam_r, id_core);
                },
                lubr_type_change(id_lubr_type) {
                    this.id_lubr_type = id_lubr_type;
                    if(this.id_steep > 0) {
                        this.get_final_weight_r(this.id_diam_r, this.id_core, this.id_steep, id_lubr_type);
                    }
                },
                steep_change(id_steep) {
                    this.id_steep = id_steep;
                    this.get_final_weight_r(this.id_diam_r, this.id_core, id_steep, this.id_lubr_type);
                },

                // расчет веса проволок по заданной длине
                calculate() {
                    if(this.input_length != 0) {
                        this.get_reel_weight(this.diam_r, this.input_length);
                        var koef = 0;
                        koef = this.input_length / 1000;
                        this.weight_sf_calc = Math.round((this.weight_r * koef * 10000)) / 10000;
                        this.weight_rope_calc = Math.round((this.final_weight * koef * 10000)) / 10000;
                        // расчет данных по заданной длине
                        vm.lng_input = [];
                        $.each(vm.weight, function (key, val) {
                            res = (val * koef) * 1000 / 1000;
                            vm.lng_input.push(res.round(1));
                        });
                        // расчет данных по заданной длине (сумарный)
                        vm.sum_lng_input = [];
                        $.each(vm.sum_weight, function (key, val) {
                            res = (val * koef) * 1000 / 1000;
                            vm.sum_lng_input.push(res.round(1));
                        });
                    }
                },

                // дополнительные функции
                constr_to_array(construct_r) { //разбиваем конструкцию каната на массив
                    construct_str = "";
                    construct_str = construct_r.replace(/\+/g, "!+!");
                    construct_str = construct_str.replace(/\|/g, "!|!");
                    construct_str = construct_str.replace(/\;/g, "!;!");
                    construct_str = construct_str.replace(/\//g, "!/!");
                    construct_str = construct_str.replace(/\(/g, " <b><font color=\"#CC0000\" >(!");
                    construct_str = construct_str.replace(/\)/g, "!<b><font color=\"\#CC0000\">)!");
                    construct_str = construct_str.replace(/\)!!\+!/g, ")</font></b>+");
                    construct_str = construct_str.replace(/\(!0!/g, "(</font></b>0");
                    construct_str = construct_str.replace(/\(!о.с.!/g, "(</font></b>о.с.");
                    construct_str = construct_str.replace(/\)!!\/!/g, "!<b><font color=\"\#CC0000\">)</font></b>/!");
                    construct_str = construct_str.replace(/\(!эл\.пр\.!/g, "(</font></b>эл.пр.");
                    construct_str = construct_str.replace(/\(!1заготовка!/g, "(</font></b>1заготовка");
                    construct_str = construct_str.replace(/0!\+!/g, "0+!");
                    construct_str = construct_str.replace(/\)\<\/font\>\<\/b\>\/!/g, ")</font></b>/");
                    construct_str = construct_str.replace(/!\<b\>\<font color\=\"\#CC0000\"\>!/g, "!");
                    construct_str = construct_str.replace(/\+1о/g, "+!1о");
                    construct_str = construct_str.replace(/\+3о/g, "+!3о");
                    construct_str = construct_str.replace(/\/3о/g, "/!3о");
                    this.constr = construct_str.split('!');
                },
                // получаем конструкцию сердечника
                core_const(id_gost_r) {
                    $.post(base_url +'/content/ajax_get_core_const/'+id_gost_r, function(data) {
                        vm.core_construct = data;
                    }).fail(function() {
                        console.error('ошибка загрузки /content/ajax_get_core_const/ Функция core_const');
                    });
                },
                // раскрутка каната на проволоки с учетом веса и процентного соотношения
                get_tab1(id_diam) {
                    $.post(base_url +'/content/spinup/'+id_diam, function(data) {
                        var arr = JSON.parse(data);
                        vm.diam_w = arr.diam_w;
                        vm.length_ratio = arr.length_ratio;
                        vm.weight = arr.ves;
                        vm.procent_w = arr.percent;
                    }).fail(function() {
                        console.error('ошибка загрузки /content/spinup/ Функция get_tab1');
                    });
                },
                // выводим сумарный вес по проволокам
                get_tab2(id_diam) {
                    $.post(base_url +'/content/getSumVes/'+id_diam, function(data) {
                        var arr = JSON.parse(data);
                        vm.sum_diam_w = arr['diam_w'];
                        vm.sum_weight = arr['ves'];
                        vm.sum_procent_w = arr['percent'];
                    }).fail(function() {
                        console.error('ошибка загрузки /content/getSumVes/ Функция get_tab2');
                    });
                },
                get_operations(id_gost, diam_r) { // получаем список операций
                    $.post(base_url +'/content/ajax_get_operations/'+id_gost+'/'+diam_r, function(data) {
                        vm.operation_list = JSON.parse(data);
                    }).fail(function() {
                        console.error('ошибка загрузки /content/ajax_get_operations/ Функция get_operations');
                    });
                },
                get_strand(id_gost){ // операции (пряди)
                    $.post(base_url +'/content/getStrand/'+id_gost, function(res) {
                        let data = JSON.parse(res);
                        if ( data.length > 0 )
                            data.forEach((item, ind) => {
                                if (item.id_strand_r == 7) data.splice(ind, 1);
                            });
                        vm.strand = data;
                    }).fail(function() {
                        console.error('ошибка загрузки /content/getStrand// Функция get_strand');
                    });
                },
                get_strand_info(id_gost_r, id_diam_r) { // диаметр и шаг свивки пряди
                    $.post(base_url +'/content/ajax_get_strand_info/'+id_gost_r+'/'+id_diam_r, function(data) {
                        vm.strand_info = JSON.parse(data);
                    }).fail(function() {
                        console.error('ошибка загрузки /content/ajax_get_strand_info// Функция get_strand_info');
                    });
                },
                get_diam(id_gost) { // получить список диаметров
                    $.post(base_url +'/content/ajax_get_diam_r/'+id_gost, function(data) {
                        vm.diam_r_list = JSON.parse(data);
                        vm.diam_r_change(vm.diam_r_list[0].id, 0, vm.diam_r_list[0]); // event diam_change
                        vm.diam_r = vm.diam_r_list[0].text;
                    }).fail(function() {
                        console.error('ошибка загрузки /content/ajax_get_diam_r/ Функция get_diam');
                    });
                },
                get_step_s(id_diam, id_core, id_gost_r, diam_r) { // шаг свивки сердечника
                    $.post(base_url +'/content/get_step_lay/'+id_diam+'/0/'+id_core+'/'+id_gost_r+'/'+diam_r, function(data) {
                        vm.step_s = JSON.parse(data).step_s;
                    }).fail(function() {
                        console.error('ошибка загрузки /content/get_step_lay/0 Функция get_step_s');
                    });
                },
                get_step_r(id_diam) { // шаг свивки каната
                    $.post(base_url +'/content/get_step_lay/'+id_diam+'/1/0/0/0', function(data) {
                        vm.step_r = JSON.parse(data).step_r;
                    }).fail(function() {
                        console.error('ошибка загрузки /content/get_step_lay/1 Функция get_step_r');
                    });
                },
                get_met_core_info(id_gost_r, diam_r) {
                    $.post(base_url +'/content/ajax_get_met_core_info/'+id_gost_r+'/'+diam_r, function(data) {
                        var res = JSON.parse(data);
                        vm.met_core.diam_s = res.diam_s;
                        vm.met_core.step_s = res.step_s;
                      }).fail(function() {
                        console.error('ошибка загрузки /content/ajax_get_met_core_info Функция get_met_core_info');
                    });
                },
                get_core(id_diam) { // сердечник каната
                    $.post(base_url +'/content/ajax_get_core/'+id_diam, function(data) {
                        vm.core_list = JSON.parse(data);
                        vm.core_change(vm.core_list[0].id); // event core_change
                    }).fail(function() {
                        console.error('ошибка загрузки /content/ajax_get_core/ Функция get_core');
                    });
                },
                get_lubr_type(id_diam, id_core) { // способ смазки
                    $.post(base_url +'/content/ajax_get_lubr_type/'+id_diam+'/'+id_core, function(data) {
                        vm.lubr_type_list = JSON.parse(data);
                        vm.lubr_type_change(vm.lubr_type_list[0].id); // event lubr_type_change
                    }).fail(function() {
                        console.error('ошибка загрузки /content/ajax_get_luber_type/ Функция get_lubr_type');
                    });
                },
                get_diam_core(id_diam, id_core, id_gost_r, diam_r) { // диаметр сердечника
                    $.post(base_url +'/content/ajax_get_diam_core/'+id_diam+'/'+id_core+'/'+id_gost_r+'/'+diam_r, function(data) {
                        vm.diam_core = data;
                    }).fail(function() {
                        console.error('ошибка загрузки /content/ajax_get_diam_core/ Функция get_diam_core');
                    });
                },
                get_steep(id_diam, id_core) { // пропитка сердечника
                    $.post(base_url +'/content/ajax_get_steep/'+id_diam+'/'+id_core, function(data) {
                        vm.steep_list = JSON.parse(data);
                        vm.steep_change(vm.steep_list[0].id); // event steep_change
                    }).fail(function() {
                        console.error('ошибка загрузки /content/ajax_get_steep/ Функция get_steep');
                    });
                },
                get_weight_r(id_diam) { // вес каната
                    $.post(base_url +'/content/ajax_get_weight_r/'+id_diam, function(data) {
                        vm.weight_r = data;
                    }).fail(function() {
                        console.error('ошибка загрузки /content/ajax_get_weight_r/ Функция get_weight_r');
                    });
                },
                get_final_weight_r(id_diam_r, id_core, id_steep, id_type) { // вес каната на 1000 м
                    $.post(base_url +'/content/ajax_get_final_weight_r/'+id_diam_r+'/'+id_core+'/'+id_steep+'/'+id_type, function(data) {
                        vm.final_weight = data;
                    }).fail(function() {
                        console.error('ошибка загрузки /content/ajax_get_final_weight_r/ Функция get_final_weight_r');
                    });
                },
                get_reel_weight(id_diam_r, length){ // расчет барабана для заданного веса
                    $.post(base_url +'/content/get_opt_reel/'+id_diam_r+'/'+length, function(data) {
                        var res = JSON.parse(data);
                        vm.reel_info = '№ ' + res.name + ' (' + res.weight + ' кг)';
                    }).fail(function() {
                        console.error('ошибка загрузки /content/get_opt_reel/ Функция get_reel_weight');
                    });
                },
                get_all_reel() { // список всех барабанов
                    $.post(base_url +'/content/get_reel_all/', function(data) {
                        vm.reel = JSON.parse(data);
                    }).fail(function() {
                        console.error('ошибка загрузки /content/get_reel_all/ Функция get_all_reel');
                    });
                },

                amount_operation: function() { // устаревшая - перенес в вычисляемые свойства
                    console.log(this.operation_list);
                    if(this.operation_list == null || this.operation_list == undefined)
                        return 0;
                    else return Object.keys(this.operation_list).length;
                },
            },
        });

        Number.prototype.round = function(places) { // математическое округление
            return +(Math.round(this + "e+" + places)  + "e-" + places);
        }
    </script>
