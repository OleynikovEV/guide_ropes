<!--
 * Created by Oleynicov_EV
 * Date: 22.12.2016
 * Description: форма входа
-->

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">

    <title>Система контролья качества</title>

    <!-- Bootstrap -->
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/css/font-awesome.css" rel="stylesheet">
    <script type="text/javascript" src="<?= base_url(JS . "libs/jquery-ui/jquery-1.9.1.js"); ?>"></script>
    <!-- Select2 -->
    <link href="/css/select2.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="/js/select2.js"></script>
    <!--// Select2 -->

    <!-- Custom Theme Style -->
    <link href="/css/custom.css" rel="stylesheet">
    <link href="/css/login.css" rel="stylesheet">
</head>
<body class="login">
<div id="app">
    <?php
    if( isset($wrong_access) ) {
        echo "<p class='alert alert-dismissable alert-danger' style='text-align: center; font-size: 16px;'>$wrong_access</p>";
    }
    ?>

    <div class="login_wrapper">
        <div class="login_form">
            <section class="login_content">
                <?= form_open(base_url()."login"); ?>
                <h1>Форма входа</h1>
                <div>
                    <input type="text" name="login" class="form-control" placeholder="Введите логин" required=""/>
                </div>
                <div>
                    <input type="password" name="password" class="form-control" placeholder="Введите пароль" required=""/>
                </div>
                <div>
                    <input type="submit" style="width: 100% !important;"
                           class="btn btn-success btn-large submit" value="Вход">
                    <a class="reset_pass" href="#">Забыли пароль?</a><br>
                    <p id="pass" class='alert alert-dismissable alert-info'
                       style='display: none; margin-top: 40px; padding: 5px; text-align: center; font-size: 16px;'>
                        звоните 6-65
                    </p>
                </div>

                <div class="clearfix"></div>

                <div class="separator">
                    <div class="clearfix"></div>
                    <br />

                    <div>
                        <h1><img src="/images/logo.jpg" width="70"> Стальканат-Силур</h1>
                        <p>©2016 Система контроля и учета нарушений технологии при изготовлении продукции.</p>
                    </div>
                </div>
                </form>
            </section>
        </div>

    </div>
</div>
</body>
</html>
<script>
    $(function(){
        $('.reset_pass').click(function(){
            $('#pass').css('display', 'block');
        });
    });
</script>
