<style>[v-cloak] { display: none; }</style>

<div class="container" id="cont" v-cloak>

    <div style="text-align: center;">
        <div v-show="msg_error.length != 0" class="alert alert-dismissable alert-danger" style="display: none">
            <span>{{msg_error}}</span>
            <i class="close fa fa-times" aria-hidden="true" v-on:click="close"></i>
        </div>
        <div v-show="msg_success.length != 0" class="alert alert-dismissable alert-success" style="display: none">
            <span>{{msg_success}}</span>
            <i class="close fa fa-times" aria-hidden="true" v-on:click="close"></i>
        </div>
    </div>

    <h3 align="center"> Редактирование справочника канатов </h3>

    <div class="control-group">
        <label class="control-label" id="help" for="gost_r">Стандарт (код)</label>
        <div class="controls">
            <select2 name="gost_r" id="gost_r" style="width: 410px"
                     :options="gost_r_list"
                     v-on:change="gost_r_change"></select2>
            <span>{{construct}}</span>
            <div class="pull-right">
                <span> <b>Поиск Ø </b>
                    <input type="number" v-model.number="diam_search" class="input-small">
                </span>
                <span style="margin-right: 5px"> <b>Поиск эл. кан.</b>
                    <select2 id="strand_r_list" class="input-medium"
                             :options="strand_r_list"
                             :allowClear="true"
                             v-on:change="strand_r_change"></select2>
                </span>
            </div>
        </div>
    </div>

    <ul class="nav nav-tabs" role="tablist" id="myTab" style="margin-top: 20px;">
        <li role="presentation" class="active"><a href="#tab5" role="tab" data-toggle="tab">Проволоки в пряди</a></li>
        <li role="presentation"><a href="#tab1" role="tab" data-toggle="tab">Канаты</a></li>
        <li role="presentation"><a href="#tab2" role="tab" data-toggle="tab">Сердечник</a></li>
        <li role="presentation"><a href="#tab3" role="tab" data-toggle="tab" v-on:click="strand_tab">Пряди</a></li>
        <li role="presentation"><a href="#tab4" role="tab" data-toggle="tab">Операции</a></li>
    </ul>

    <div class="tab-content">
        <!-- Проволоки в пряди -->
        <div role="tabpanel" class="tab-pane active" id="tab5">
            <wires-in-strand></wires-in-strand>
            <strand-const :id_gost_r="Number(id_gost_r)"></strand-const>
        </div>
        <!-- Канаты -->
        <div role="tabpanel" class="tab-pane" id="tab1">
            <input type="button" class="btn btn-success" value="сохранить" style="margin-bottom: 10px"
                   v-on:click="save_r" :disabled="disabled_r" />
            <table id="data_r" class="display compact" cellspacing="0" cellpadding="5" style="width: 50%">
                <thead>
                    <tr>
                        <th>Ø каната</th>
                        <th>шаг свивки каната</th>
                        <th>Кратность</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(item, index) in data_r" v-if="item.diam_r.indexOf(diam_search) != -1 || diam_search.length == 0">
                        <td>{{item.diam_r}}
                        <td><input type="number" :id="'data_r_1_'+index" :value="item.step_r" v-model="item.step_r"/>
                        <td>{{(item.step_r/item.diam_r).round(1)}}
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- Сердечник -->
        <div role="tabpanel" class="tab-pane" id="tab2">
            <input type="button" class="btn btn-success" value="сохранить" style="margin-bottom: 10px"
                   v-on:click="save_c" :disabled="disabled_c" />
            <table id="data_c" class="display compact" cellspacing="0" cellpadding="5" style="width: 100%">
                <thead>
                    <tr>
                        <th>Ø каната</th>
                        <th>тип сердечника</th>
                        <th>Ø сердечника</th>
                        <th>шаг свивки сердечника</th>
                        <th>Кратность</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(item, index) in data_c" v-if="item.diam_r.indexOf(diam_search) != -1 || diam_search.length == 0">
                        <td>{{item.diam_r}}
                        <td>{{item.core}}
                        <td><input type="number" :id="'data_c_2_'+index" :value="item.diam_core" v-model="item.diam_core"/>
                        <td><input type="number" :id="'data_c_3_'+index" :value="item.step_c" v-model="item.step_c" />
                        <td v-if="item.diam_core != 0">{{(item.step_c/item.diam_core).round(1)}}
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- Пряди -->
        <div role="tabpanel" class="tab-pane" id="tab3">
            <input type="button" class="btn btn-success" value="сохранить" style="margin-bottom: 10px"
                   :disabled="disabled_s" v-on:click="save_s" />
            <table id="strand" class="display compact" cellspacing="0" cellpadding="5" style="width: 100%">
                <thead>
                    <tr>
                        <th>Ø каната</th>
                        <th>конст-ия пряди</th>
                        <th>название</th>
                        <th>Ø пряди</th>
                        <th>шаг свивки пряди</th>
                        <th>Кратность</th>
                    </tr>
                </thead>
                <tbody>
                    <template v-for="(item, index_s) in data_s"
                              v-if="( item.diam_r.indexOf(diam_search) != -1 || diam_search.length == 0 )
                              && ( strand_r_selected == '' || strand_r_selected == item.strand_r_name )">
                        <!--  ЕСЛИ В ПРЯЛЯХ ЕСТЬ ДОПОЛНИТЕЛЬНЫЕ ОПЕРАЦИИ   -->
                        <template v-if="strand_operation_info_lng != 0">
                            <tr> <!--  ОСНОВНЫЕ ДАННЫЕ ПО ПРЯДЯМ   -->
                                <td>{{item.diam_r}}
                                <td>{{item.const}}
                                <td >{{item.strand_r_name}}
                                <td><input type="number" :id="'strand_3_'+index_s" :value="item.diam_s" v-model="item.diam_s" />
                                <td><input type="number" :id="'strand_4_'+index_s" :value="item.step_s" v-model="item.step_s" />
                                <td v-if="item.step_s != null && item.diam_s != null && item.diam_s != 0">{{(item.step_s/item.diam_s).round(1)}}
                            </tr>
                            <!--  ЕСЛИ УЖЕ ЕСТЬ ДАННЫЕ ПО ДОП. ОПЕРАЦИЯМ - ВЫВОДИМ ИХ   -->
                            <template v-if="strand_operation_info.hasOwnProperty(item.diam_r)">
                                <tr v-for="(info, index_oi, key) in strand_operation_info[item.diam_r]" v-if="item.id_strand_const == info.strand_const_id">
                                    <td>
                                    <td>{{info.construction}}
                                    <td><i>{{operation_list_id[info.operation_id]}}</i>
                                    <td><input type="number" :value="info.diam_so" v-model="info.diam_so" >
                                    <td><input type="number" :value="info.steep_so" v-model="info.steep_so" >
                                    <td v-if="info.diam_so != null && info.steep_so != null && info.diam_so != 0">{{(info.steep_so/info.diam_so).round(1)}}
                                </tr>
                            </template>
                            <!--  ЕСЛИ ДАННЫХ НЕТ - ПРОСИМ ВВЕСТИ   -->
<!--                            <template v-else v-for="oper in str_operation_list">-->
<!--                                <tr>-->
<!--                                    <td>-->
<!--                                    <td><i>{{oper.construction}}</i>-->
<!--                                    <td><i>{{operation_list_id[oper.operation_id]}}</i>-->
<!--                                    <td><input type="number" v-model="strand_operation_info[item.id_diam_r]">-->
<!--                                    <td><input type="number">-->
<!--                                </tr>-->
<!--                            </template>-->
                        </template>
                        <!--  ЕСЛИ ДОП. ОПЕРАЦИЙ В ПРЯДЯХ НЕТУ   -->
                        <template v-else>
                            <tr>
                                <td>{{item.diam_r}}
                                <td>{{item.const}}
                                <td>{{item.strand_r_name}}
                                <td><input type="number" :id="'strand_3_'+index_s" :value="item.diam_s" v-model="item.diam_s" />
                                <td><input type="number" :id="'strand_4_'+index_s" :value="item.step_s" v-model="item.step_s" />
                                <td v-if="item.step_s != null && item.diam_s != null && item.diam_s != 0">{{(item.step_s/item.diam_s).round(1)}}
                            </tr>
                        </template>
                    </template>
                </tbody>
            </table>
        </div>
        <!-- Операции -->
        <div role="tabpanel" class="tab-pane" id="tab4">
            <table class="display compact" style="margin-bottom: 5px;"  cellspacing="0" cellpadding="5" style="width: 100%">
                <tr>
                    <th></th>
                    <th>Прядь</th>
                    <th>Конструкция</th>
                    <th>Операция</th>
                </tr>
                <tr>
                    <td><input type="button" v-on:click="operation_add" class="btn btn-success" value="Добавить" style="margin: 0" /></td>
                    <td><select2 name="strand_cons" id="strand_cons" :options="strand_cons_list"
                                    v-on:change="operation_cons_change" ></select2></td>
                    <td><input type="text" v-model="operation_cons" style="margin: 0; border-radius: 6px;"></td>
                    <td><select2 name="operation" id="operation" :options="operation_list"
                                    v-on:change="operation_list_change"></select2></td>
                </tr>
                <tr><th></th><th></th><th><span style="color: red">{{error_operation_cons}}</span></th><th></th></tr>
            </table>
            <table id="operation" class="display compact" cellspacing="0" cellpadding="5" style="width: 100%"
                v-if="str_operation_list.length > 0">
                <tbody>
                    <tr v-for="(item, index) in str_operation_list" :key="item.str_oper_id">
                        <td style="width: 120px;">
                            <a rel="#" v-on:click="oparetion_del(index)">del</a> / edit
                        <td style="width: 220px;">{{strand_cons_list2[item.strand_const_id]}}
<!--                            <select2 :id="'scl_'+index" :options="strand_cons_list" :value="item.strand_const_id"></select2>-->
                        <td><input readonly type="text" :value="item.construction"></td>
                        <td><select2 disabled :id="'ol_'+index" :options="operation_list" :value="item.operation_id"></select2>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php
//    require_once '/components/strand_const.vue';
?>

<link href="/css/select2.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/js/select2.js"></script>
<script type="text/javascript" src="/js/vue.js"></script>
<script type="text/javascript" src="/js/vue-select2.js?ver=1.4"></script>
<script type="text/javascript" src="/js/vue-select2_new.js?ver=1.4"></script>

<link href="/css/jquery.dataTables.css" rel="stylesheet">
<link href="/css/keyTable.bootstrap.css" rel="stylesheet">
<script type="text/javascript" src="/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="/js/dataTables.keyTable.js"></script>
<script type="text/javascript" src="/js/langDataTable.js"></script>
<script>
    'use strict';
    const DATA_ADD_SUCCES = 'Данные успешно сохранены';
    const DATA_DEL_SUCCES = 'Не удалось сохранить данные';

    // добавить в массив если элемента нет
    Array.prototype.pushIfNotExist = function(element) {
        if (this.length == 0) {
            this.push(element);
        }
        else {
            if (this.indexOf(element) == -1) {
                this.push(element);
            }
        }
    };

    Number.prototype.round = function(places) { // математическое округление
        return +(Math.round(this + "e+" + places)  + "e-" + places);
    }

    var vm = new Vue({
        el: '#cont',
        data: {
            test: 103,
            disabled_r: false,
            disabled_c: false,
            disabled_s: false,
            construct: '<?= $data["construct"]; ?>', // конструкция каната

            // положение проволок в пряди
            show_pos: true,
            const_decompose: null,
            first_id_diam_r: null, // первый диаметр каната для положения проволоки в пряди
            diam_w: null,
            diam_w_list: [], // список проволок
            strand_sequence_list: [], // порядок элементов каната в канате
            id_gost_r: null,
            diam_w_posit_new: [],

            gost_r_list: <?= json_encode($data["gost_r_list"]); ?>, // стандарт
            data_r: <?= json_encode($data["data_r"]); ?>, // канат
            data_c: <?= json_encode($data["data_c"]); ?>, // сердечник
            data_s: <?= json_encode($data["data_s"]); ?>, // пряди
            operation_list: <?= json_encode($data["operation_list"]); ?>, // список доступных операций
            strand_r_list: <?= json_encode($data["strand_r_list"]); ?>, // список доступных элементов канатов
            strand_r_selected: '',
            operation_list_id: [], // список по ID + название
            selected_operation: <?= $data["selected_operation"]; ?>, // выбранная операция
            strand_operation_info: <?= $data["strand_operation_info"]; ?>, // список операций с прядями
            strand_operation_info_lng: 0,
            operation_cons: null, // конструкция операции
            strand_cons_list: null, // список прядей (в виде конструкции) в канате для select2
            strand_cons_list2: [], // список прядей (в виде конструкции) в канате
            selected_strand_cons: null, // выбранная прядь
            str_operation_list: [], // список уже существующих операций в прядях
            selected_id_gost: null,
            msg_success: '',
            msg_error: '',
            error_operation_cons: '',
            diam_search: '',
            operation: false,
        },
        components: {
            strandConst: httpVueLoader('/components/strand_const.vue'),
            wiresInStrand: httpVueLoader('/components/wires_in_strand.vue')
        },
        mounted(){
            $('#gost_r').trigger('change');
            this.operation_list.forEach((operation) => {
                this.operation_list_id[operation.id] = operation.text;
            });
        },
        methods: {
            row_focus(event){},
            strand_r_change(val, index, data) {
                this.strand_r_selected = (val > 0) ? data.text : '';
            },
            strand_tab(){
                if(this.operation == true) {
                    this.get_strand_operation_info(this.selected_id_gost);
                }
                this.operation = false;
            },
            close() {
                this.msg_success = '';
                this.msg_error = '';
            },
            gost_r_change(id_gost, index, arr) { // выбор нового стандарта
                this.show_pos = false;
                this.diam_w_posit_new = [];
                this.id_gost_r = id_gost;
                this.num = 0;
                this.selected_id_gost = id_gost;
                this.error_operation_cons = '';
                this.construct = '';
                this.get_diam(id_gost);
                this.get_tab1(this.first_id_diam_r);
                this.construct = arr.construct;
                this.constr_to_array(this.construct);
                this.get_strand_operation(id_gost);
                this.get_strand_cons(id_gost);
                this.get_strand_operation_info(id_gost);
            },
            operation_list_change(id) {
                this.selected_operation = id;
            },
            operation_cons_change(id) {
                this.selected_strand_cons = id;
            },
            get_diam(id_gost) { // получить список диаметров
                var that = this;
                $.ajax({
                    type: 'POST',
                    url: base_url +'/edit/ajax_get_data_for_edit',
                    dataType: 'JSON',
                    data: { 'id_gost_r': id_gost },
                    async: false,
                    success: function(data) {
                        that.data_r = data.data_r;
                        that.first_id_diam_r = that.data_r[0].id_diam_r;
                        that.data_c = data.data_c;
                        that.data_s = data.data_s;
                    },
                    error: function(e) {
                        console.error('ошибка загрузки /edit/ajax_get_data_for_edit/ Функция get_diam');
                    }
                });
            },
            get_diam_r(id_gost) { // получить список диаметров каната
                var that = this;
                $.ajax({
                    type: 'POST',
                    url: base_url +'/edit/ajax_get_data_r',
                    dataType: 'JSON',
                    data: { 'id_gost_r': id_gost },
                    async: false,
                    success: function(data) {
                        that.data_r = data.data_r;
                    },
                    error: function(e) {
                        console.error('ошибка загрузки /edit/ajax_get_data_r/ Функция get_diam_r');
                    }
                });
            },
            get_diam_c(id_gost) { // получить список диаметров каната
                var that = this;
                $.ajax({
                    type: 'POST',
                    url: base_url +'/edit/ajax_get_data_c',
                    dataType: 'JSON',
                    data: { 'id_gost_r': id_gost },
                    async: false,
                    success: function(data) {
                        that.data_c = data.data_c;
                    },
                    error: function(e) {
                        console.error('ошибка загрузки /edit/ajax_get_data_c/ Функция get_diam_c');
                    }
                });
            },
            get_diam_s(id_gost) { // получить список диаметров каната
                var that = this;
                $.ajax({
                    type: 'POST',
                    url: base_url +'/edit/ajax_get_data_s',
                    dataType: 'JSON',
                    data: { 'id_gost_r': id_gost },
                    async: false,
                    success: function(data) {
                        that.data_s = data.data_s;
                    },
                    error: function(e) {
                        console.error('ошибка загрузки /edit/ajax_get_data_s/ Функция get_diam_s');
                    }
                });
            },
            get_strand_operation(id_gost) {
                $.post(base_url +'/edit/ajax_get_strand_operation/'+id_gost, function(data) {
                    vm.str_operation_list = null;
                    vm.str_operation_list = JSON.parse(data);
                    vm.strand_operation_info_lng = Object.keys(vm.str_operation_list).length;
                }).fail(function() {
                    console.error('ошибка загрузки /edit/ajax_get_strand_operation/ Функция get_strand_operation');
                });
            },
            get_strand_cons(id_gost) {
                $.post(base_url +'/edit/ajax_get_strand_cons/'+id_gost, function(data) {
                    vm.strand_cons_list = JSON.parse(data);
                    vm.strand_cons_list2 = [];
                    for(var item in vm.strand_cons_list){
                        vm.strand_cons_list2[vm.strand_cons_list[item].id] = vm.strand_cons_list[item].text;
                    }
                    vm.selected_strand_cons = vm.strand_cons_list[0].id;
                }).fail(function() {
                    console.error('ошибка загрузки /edit/ajax_get_strand_cons/ Функция get_strand_cons');
                });
            },
            get_strand_operation_info(id_gost) {
                $.post(base_url +'/edit/ajax_get_strand_operation_info_byGost/'+id_gost, function(data) {
                    vm.strand_operation_info = null;
                    vm.strand_operation_info = JSON.parse(data);
                    var key = Object.keys(vm.strand_operation_info);
                    var tmp = {};
                    if(vm.strand_operation_info_lng > 0){
                        tmp = vm.strand_operation_info;
                        for(var index in vm.data_r){ // создаем новый массив
                            if( key.indexOf(vm.data_r[index].diam_r) == -1 ) { // если еще нет сохраненных данных
                                tmp[vm.data_r[index].diam_r] = {};
                                for (var oper in vm.str_operation_list) {
                                    tmp[vm.data_r[index].diam_r][oper] = {
                                        id_gost_r: id_gost,
                                        diam_r: vm.data_r[index].diam_r,
                                        diam_so: '',
                                        steep_so: '',
                                        strand_oper_id: vm.str_operation_list[oper].str_oper_id,
                                        operation_id: vm.str_operation_list[oper].operation_id,
                                        strand_const_id: vm.str_operation_list[oper].strand_const_id,
                                        construction: vm.str_operation_list[oper].construction
                                    }
                                }
                            } else {
                                var id = vm.data_r[index].diam_r;
                                var length = Object.keys(tmp[id]).length;
                                var count = 1;
                                if(length != vm.strand_operation_info_lng) {
                                    for (var oper in vm.str_operation_list) {
                                        if(count > length) {
                                            tmp[id][oper] = {
//                                                id_gost_r: vm.data_r[index].id_gost,
                                                id_gost_r: id_gost,
                                                diam_r: vm.data_r[index].diam_r,
                                                diam_so: '',
                                                steep_so: '',
                                                strand_oper_id: vm.str_operation_list[oper].str_oper_id,
                                                operation_id: vm.str_operation_list[oper].operation_id,
                                                strand_const_id: vm.str_operation_list[oper].strand_const_id,
                                                construction: vm.str_operation_list[oper].construction
                                            }
                                        }
                                        count++;
                                    }
                                }
                            }
                        }
                    }
                }).fail(function() {
                    console.error('ошибка загрузки /edit/ajax_get_strand_operation_info_byGost/ Функция get_strand_operation_info');
                });
            },
            save_r() { // сохранить данные по канату
                this.disabled_r = true;
                this.close();
                $.post(base_url +'/edit/ajax_save_r/', {data: vm.data_r})
                .done(function(data) {
                    if(data == true) vm.msg_success = DATA_ADD_SUCCES;
                    else vm.msg_error = DATA_DEL_SUCCES;
                    vm.get_diam_r(vm.selected_id_gost);
                    vm.disabled_r = false;
                })
                .fail(function() {
                    vm.msg_error = DATA_DEL_SUCCES;
                    vm.disabled_r = false;
                    console.error('ошибка загрузки /edit/ajax_save_r/ Функция save_r');
                });
            },
            save_c() { // сохранить данные по сердечнику
                this.disabled_c = true;
                this.close();
                $.post(base_url +'/edit/ajax_save_c/', {data: JSON.stringify(vm.data_c)})
                .done(function(data) {
                        console.log(data);
                    if(data == true) vm.msg_success = DATA_ADD_SUCCES;
                    else vm.msg_error = DATA_DEL_SUCCES;
                    vm.get_diam_c(vm.selected_id_gost);
                    vm.disabled_c = false;
                })
                .fail(function() {
                    vm.msg_error = DATA_DEL_SUCCES;
                    vm.disabled_c = false;
                    console.error('ошибка загрузки /edit/ajax_save_c/ Функция save_c');
                });
            },
            save_s() { // сохранить данные по прядям
                this.disabled_s = true;
                this.close();
                var soi = [];
                for(var item in vm.strand_operation_info){
                    soi.push(vm.strand_operation_info[item]);
                }
                $.post(base_url +'/edit/ajax_save_s/', {data_s: JSON.stringify(vm.data_s), data_oper: JSON.stringify(soi)})
                .done(function(data) {
                    if(data == true) vm.msg_success = DATA_ADD_SUCCES;
                    else vm.msg_error = DATA_DEL_SUCCES;
                    vm.get_diam_s(vm.selected_id_gost);
                    vm.get_strand_operation_info(vm.selected_id_gost);
                    vm.disabled_s = false;
                })
                .fail(function() {
                    vm.msg_error = DATA_DEL_SUCCES;
                    vm.get_diam_s(vm.selected_id_gost);
                    vm.get_strand_operation_info(vm.selected_id_gost);
                    vm.disabled_s = false;
                    console.error('ошибка загрузки /edit/ajax_save_s/ Функция save_s');
                });
            },
            operation_add() { // добавление операции к прядям
                this.close();
                if(this.operation_cons === null || this.operation_cons.trim() === '') this.error_operation_cons = 'Укажите конструкцию';
                else {
                    this.error_operation_cons = '';
                    var obj = {
                        'strand_const_id': vm.selected_strand_cons,
                        'operation_id': vm.selected_operation,
                        'construction': vm.operation_cons
                    };
                    $.post(base_url +'/edit/ajax_add_operation_to_strand/', {data: obj})
                        .done(function(data) {
                            if(data !== false) {
                                vm.operation = true;
                                vm.get_strand_operation(vm.selected_id_gost);
                                vm.get_strand_operation_info(vm.selected_id_gost);
                            }
                            else vm.msg_error = '';
                        })
                        .fail(function() {
                            vm.msg_error = DATA_DEL_SUCCES;
                            console.error('ошибка загрузки /edit/ajax_add_operation_to_strand/ Функция operation_add');
                        });
                }
            },
            oparetion_del(index) {
                if(confirm('Также будут удалены связанные данный из раздела ПРЯДИ! Удалить?')) {
                    var arr = this.str_operation_list[index];
                    $.post(base_url + '/edit/ajax_del_operation_in_strand/' + arr.str_oper_id, function (data) {
                        if (data == false) vm.msg_error = 'Не удалось удалить запись';
                        else {
                            vm.operation = true;
                            vm.get_strand_operation(vm.selected_id_gost);
                            vm.get_strand_operation_info(vm.selected_id_gost);
                        }
                    })
                        .fail(function () {
                            vm.msg_error = 'Не удалось удалить запись';
                            console.error('ошибка загрузки /edit/ajax_del_operation_in_strand/ Функция oparetion_del');
                        });
                }
            },
            constr_to_array(construct_r) { //разбиваем конструкцию каната на массив
                var construct_str = "";
                construct_str = construct_r.replace(/\+/g, "!+!");
                construct_str = construct_str.replace(/\|/g, "!|!");
                construct_str = construct_str.replace(/\;/g, "!;!");
                construct_str = construct_str.replace(/\//g, "!/!");
                construct_str = construct_str.replace(/\(/g, " <b><font color=\"#CC0000\" >(!");
                construct_str = construct_str.replace(/\)/g, "!<b><font color=\"\#CC0000\">)!");
                construct_str = construct_str.replace(/\)!!\+!/g, ")</font></b>+");
                construct_str = construct_str.replace(/\(!0!/g, "(</font></b>0");
                construct_str = construct_str.replace(/\(!о.с.!/g, "(</font></b>о.с.");
                construct_str = construct_str.replace(/\)!!\/!/g, "!<b><font color=\"\#CC0000\">)</font></b>/!");
                construct_str = construct_str.replace(/\(!эл\.пр\.!/g, "(</font></b>эл.пр.");
                construct_str = construct_str.replace(/\(!1заготовка!/g, "(</font></b>1заготовка");
                construct_str = construct_str.replace(/0!\+!/g, "0+!");
                construct_str = construct_str.replace(/\)\<\/font\>\<\/b\>\/!/g, ")</font></b>/");
                construct_str = construct_str.replace(/!\<b\>\<font color\=\"\#CC0000\"\>!/g, "!");
                construct_str = construct_str.replace(/\+1о/g, "+!1о");
                construct_str = construct_str.replace(/\+3о/g, "+!3о");
                construct_str = construct_str.replace(/\/3о/g, "/!3о");
                this.const_decompose = construct_str.split('!');
            },
            // сохраняем новое положение проволок
            save_diam_w_pos(){
                this.disabled_r = true;
                $.post(base_url +'/edit/ajax_save_diam_w_pos/', {data: vm.diam_w_posit_new})
                    .done(function(data) {
                        if(data == 'true') vm.msg_success = DATA_ADD_SUCCES;
                        else vm.msg_error = DATA_DEL_SUCCES;
                        vm.diam_w_posit_new = [];
                        vm.disabled_r = false;
                    })
                    .fail(function() {
                        vm.msg_error = DATA_DEL_SUCCES;
                        vm.disabled_r = false;
                        console.error('ошибка загрузки /edit/ajax_save_r/ Функция save_r');
                    });
            },
            select_diam_w_pos(val, ind, arr, key){
                this.diam_w_posit_new[arr.id] =
                    {
                        'id_gost_r': vm.id_gost_r,
                        'ply': Number(key) + 1,
                        'id_ply_strand': arr.id,
                    };
            },
            // раскрутка каната на проволоки с учетом веса и процентного соотношения
            get_tab1(id_diam) {
                var that = this;
                $.post(base_url +'/content/getPLYSpinup/'+id_diam, function(data) {
                    var arr = JSON.parse(data);
                    that.diam_w = arr;
                    that.diam_w_list = [];
                    that.strand_sequence_list = [];
                    arr.forEach(function(item, i){
                        if (!that.diam_w_list.hasOwnProperty(item.id_strand_r)) {
                            that.diam_w_list[item.id_strand_r] = [];
                        }
                        that.strand_sequence_list.pushIfNotExist(item.id_strand_r);
                        that.diam_w_list[item.id_strand_r].push( {
                            id: item.id_p_s,
                            text: item.diam_w
                        } );
                    });
                    that.show_pos = true;
                }).fail(function() {
                    console.error('ошибка загрузки /content/spinup/ Функция get_tab1');
                });
            },
        },
    });

    function log(data) { console.log(data); }
</script>

<script>
    $(document).ready(function () {
        var hash = window.location.hash;
        hash && $('ul.nav-tabs a[href="' + hash + '"]').tab('show');
    });
</script>

<style>
    .select_text > .select2-container--default .select2-selection--single
    .select2-selection__rendered  {
        font-style: italic;
    }
    .select_text > .select2-container--default .select2-selection--single {
        border: 0;
    }
    .select_text > .select2-container--default .select2-selection--single
    .select2-selection__arrow {
        display: none;
    }
    .select_text > .select2-container--default.select2-container--disabled
    .select2-selection--single {
        background-color: #fff;
        cursor: default;
    }
    a { cursor: pointer }
    .dataTables_wrapper .dataTables_filter input {
        position: absolute;
        right: 0px;
        top: -40px;
    }
    table.dataTable th.focus, table.dataTable td.focus {
        background-color: rgba(52, 124, 186, 0.27) !important;
        box-shadow: inset 0 1px 1px rgba(52, 124, 186, 0.57);
        outline: 1px solid rgba(52, 124, 186, 0.27) !important;
        outline-offset: -1px !important;
    }
    table.dataTable.compact tbody th,
    table.dataTable.compact tbody td { padding: 1px; }
    input[type="number"] { margin-bottom: 0; }

    .container {
        width: 1200px;
    }

    @media (min-width: 1200px)
        .container {
            max-width: 1200px;
            width: 1200px;
        }

        #tab5 {
            width: 100%;
        }
        .tab5 th { padding-right:  10px; }
        .border_right { border-right: 1px solid #EEEEEE }
        .border_right:last-child { border-right: 0 }
</style>