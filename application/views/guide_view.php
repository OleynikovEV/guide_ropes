<div class="container" id="cont">
    <h3 align="center">Полуфабрикат для изготовления канатов. Вес канатов.
        <a style="font-size:24px" style="color: black" onclick="get_reel_all()" onclick="window.location.reload()"
            href="#win2" id="reel_table" value="Барабаны"> Барабаны</a>
    </h3>

    <table width="100%" cellspacing="0" cellpadding="5">
        <tr>
            <td valign="top">
                <div class="control-group">
                    <label class="control-label" id="help" for="gost_r">Стандарт (код)</label>
                    <div class="controls">
                        <select name="gost_r" id="gost_r" value="<? set_value("gost_r") ?>">
                    </div>
                </div>
            </td>

            <td valign="top">
                <div class="control-group">
                    <label class="control-label" for="step_r">Шаг свивки сердечника</label>
                    <div class="controls">
                        <input type="text" readonly name="step_s" id="step_s" value="<? set_value("step_s") ?>">
                    </div>
                </div>
            </td>

            <td valign="top">
                <div class="control-group">
                    <label class="control-label" for="step_r">Шаг свивки каната</label>
                    <div class="controls">
                        <input type="text" readonly name="step_r" id="step_r" value="<? set_value("step_r") ?>">
                    </div>
                </div>
            </td>
        </tr>
    </table>

    <table width="100%" cellspacing="0" cellpadding="5">
        <tr>
            <td valign="top">
                <div class="control-group">
                    <label class="control-label" for="constr">Конструкция</label>
                    <div class="controls">
                        <input style="width:410px" type="text" readonly name="constr" id="constr" value="<? set_value("constr") ?>">
                    </div>
                </div>
            </td>

            <td valign="top">
                <div class="control-group">
                    <label class="control-label" for="core">Выбор сердечника</label>
                    <div class="controls">
                        <select name="core" id="core" value="<? set_value("core") ?>">
                    </div>
                </div>
            </td>

            <td valign="top">
                <div class="control-group">
                    <label class="control-label" for="lubr_type">Способ смазки</label>
                    <div class="controls">
                        <select name="lubr_type" id="lubr_type" value="<? set_value("lubr_type") ?>">
                    </div>
                </div>
            </td>
        </tr>
    </table>

    <table width="100%" cellspacing="0" cellpadding="5">
        <tr>
            <td valign="top" style="width:448px">
                <div class="control-group">
                    <label class="control-label" for="diam_r">Диаметр каната</label>
                    <div class="controls">
                        <select name="diam_r" id="diam_r" value="<? set_value("diam_r") ?>">
                    </div>
                </div>
            </td>

            <td valign="top">
                <div class="control-group">
                    <label class="control-label" for="core">Диаметр сердечника</label>
                    <div class="controls">
                        <input type="text" readonly name="diam_core" id="diam_core" value="<? set_value("diam_core") ?>">
                    </div>
                </div>
            </td>

            <td valign="top">
            </td>
        </tr>
    </table>

    <table width="100%" cellspacing="0" cellpadding="5">
        <tr>
            <td valign="top">
                <div class="control-group">
                    <label class="control-label" for="constr">Вес полуфабриката (1000m)</label>
                    <div class="controls">
                        <input style="width:410px" type="text" readonly name="weight_r" id="weight_r" value="<? set_value("weight_r") ?>">
                    </div>
                </div>
            </td>

            <td valign="top">
                <div class="control-group">
                    <label class="control-label" for="steep">Пропитка</label>
                    <div class="controls">
                        <select name="steep" id="steep" value="<? set_value("steep") ?>">
                    </div>
                </div>
            </td>

            <td valign="top">
                <div class="control-group">
                    <label class="control-label" for="final_weight">Вес каната (1000m)</label>
                    <div class="controls">
                        <input type="text" readonly name="final_weight" id="final_weight" value="<? set_value("final_weight") ?>">
                    </div>
                </div>
            </td>
        </tr>
    </table>

    <table width="100%" cellspacing="0" cellpadding="5">
        <tr>
            <td valign="top" style="width:33%">
                <a rel="" v-on:click="show_big_img" style="cursor: pointer">
                    <img :src="img" style="max-width: 70%; margin-left: 30px" id="imgID">
                </a>
            </td>

            <td valign="top">
                <div class="control-group">
                    <label class="control-label" for="Real_ves">Длина заданная</label>
                    <div class="controls">
                        <input style="width:350px" type="text" name="Real_ves" id="Real_ves" value="<? set_value("Real_ves") ?>" readonly="true">
                    </div>
                </div>

                <div class="control-group">
                    <div class="control-group">
                        <input style="width:350px" type="button" onclick="MyFunction()" onclick="window.location.reload()" id="b_ves" disabled="true" value="Рассчитать">
                    </div>
                </div>
            </td>

            <td valign="top" style="width:33%"></td>
        </tr>
    </table>

    <table width="100%" cellspacing="0" cellpadding="5">
        <tr>
            <td valign="top" style="width:50%">
                <div class="control-group">
                    <label class="control-label" for="constr">Вес полуфабриката (заданный)</label>
                    <div class="controls">
                        <input style="width:350px" type="text" readonly="true" name="weight_calc" id="weight_calc" value="<? set_value("weight_calc") ?>">
                    </div>
                </div>
            </td>

            <td>
                <div class="control-group">
                    <label class="control-label" for="constr">№ барабана (вес)</label>
                    <div class="controls">
                        <input style="width:350px" type="text" readonly="true" name="reel" id="reel" value="<? set_value("weight_calc") ?>">
                    </div>

                </div>
            </td>

            <td valign="top" style="width:50%">
                <div class="control-group">
                    <label class="control-label" for="constr">Вес каната (заданный)</label>
                    <div class="controls">
                        <input style="width:350px" type="text" readonly="true" name="weight_calc_final" id="weight_calc_final" value="<? set_value("weight_calc_final") ?>">
                    </div>
                </div>
            </td>
        </tr>
    </table>

    <div class="table">
        <table white-space="nowrap">
            <tr>
                <th nowrap="nowrap">Операции</th>
                <th v-for="(item, index) in strand" nowrap="nowrap"
                    :colspan="separator[index]"
                    style="padding-right: 25px; border-left: 1px solid #EEEEEE"> {{item.name}} </th>
            </tr>
            <tr>
                <th>Конструкция</th>
                <td v-for="item in constr" nowrap="nowrap" v-if="item != ''" v-html="item"></td>
            </tr>
            <tr>
                <th>Диаметр проволоки</th>
                <template v-for="item in diam_w" nowrap="nowrap" >
                    <td></td>
                    <td>{{item}}</td>
                </template>
            </tr>
            <tr>
                <th>Вес(1000m)</th>
                <template v-for="item in weight" nowrap="nowrap" >
                    <td></td>
                    <td>{{item}}</td>
                </template>
            </tr>
            <tr>
                <th>Процент проволоки</th>
                <template v-for="item in procent_w" nowrap="nowrap" >
                    <td></td>
                    <td>{{item}}</td>
                </template>
            </tr>
            <!-- Заданный вес -->
            <tr v-if="lng_input != null">
                <th>Заданный вес</th>
                <template v-for="item in lng_input" nowrap="nowrap" >
                    <td></td>
                    <th>{{item}}</th>
                </template>
            </tr>
        </table>
    </div>
    <div class="table" >
        <table>
            <tr>
                <th>Сумма по диаметрам проволоки</th>
                <td v-for="item in sum_diam_w">{{item}}</td>
            </tr>
            <tr>
                <th>Вес(1000m)</th>
                <td v-for="item in sum_weight">{{item}}</td>
            </tr>
            <tr>
                <th>Процент проволоки</th>
                <td v-for="item in sum_procent_w">{{item}}</td>
            </tr>
            <tr v-if="sum_lng_input != null">
                <th>Заданный вес</th>
                <th v-for="item in sum_lng_input">{{item}}</th>
            </tr>
        </table>
    </div>

    <!-- Список барабанов -->
    <a href="#x" class="overlay" id="win2"></a>
    <div class="popup">
        <h3 align="center">Таблица барабанов</h3>
        <center>
            <table v-if="reel != null" border="1" style="text-align: center">
                <tr>
                    <th>№</th> <th>Диам. щеки</th> <th>Длина шейки</th> <th>Вес без оп.*</th> <th>Вес с оп.*</th>
                </tr>
                <tr v-for="(item, n) in reel.name">
                    <th>{{item}}</th>
                    <td>{{reel.diam_cheek[n]}}</td>
                    <td>{{reel.length_pin[n]}}</td>
                    <td>{{reel.weight[n]}}</td>
                    <td>{{reel.weight_w[n]}}</td>
                </tr>
            </table>
        </center>
        <h4 align="center">*"оп." - опалубка</h4>
    </div>
    <a class="close" title="Закрыть" href="#close"></a></div>
</div>

    <script type="text/javascript" src="/js/vue.js"></script>
    <script type="text/javascript" src="/js/vue-select2.js"></script>
    <script>
        var vm = new Vue({
            el: '#cont',
            data: {
                img: '',
                strand: null,
                constr: null,
                diam_w: null,
                weight: null,
                procent_w: null,
                lng_input: null,
                separator: [],
                reel: null,

                sum_diam_w: null,
                sum_weight: null,
                sum_procent_w: null,
                sum_lng_input: null,
            },
            watch: {
                constr: function(newVal){
                    var regexp = /\d <b>/ig;
                    var count = 0;
                    this.lng_input = null;
                    this.separator = [];
                    newVal.forEach(function(item, ind){
                        if( regexp.exec(item) != null && ind != 0) {
                            vm.separator.push(count);
                            count =0;
                        }
                        count++;
                    });
                    this.separator.push(count);
                }
            },
            mounted() {
                console.clear();
            },
            methods: {
                show_big_img: function() {
                    console.clear();
                },
            },
        });
    </script>

    <script type="text/javascript">
    var html = "";
    var table;
    var part1;
    var table_d;
    var part_d;

    // читаем конструкцию каната и разбиваем на массив
    function string_to_array(str) {
        construct_str = "";
        construct_str = str.replace(/\+/g, "!+!");
        construct_str = construct_str.replace(/\|/g, "!|!");
        construct_str = construct_str.replace(/\;/g, "!;!");
        construct_str = construct_str.replace(/\//g, "!/!");
        construct_str = construct_str.replace(/\(/g, " <b><font color=\"#CC0000\" >(!");
        construct_str = construct_str.replace(/\)/g, "!<b><font color=\"\#CC0000\">)!");
        construct_str = construct_str.replace(/\)!!\+!/g, ")</font></b>+");
        construct_str = construct_str.replace(/\(!0!/g, "(</font></b>0");
        construct_str = construct_str.replace(/\(!о.с.!/g, "(</font></b>о.с.");
        construct_str = construct_str.replace(/\)!!\/!/g, "!<b><font color=\"\#CC0000\">)</font></b>/!");
        construct_str = construct_str.replace(/\(!эл\.пр\.!/g, "(</font></b>эл.пр.");
        construct_str = construct_str.replace(/\(!1заготовка!/g, "(</font></b>1заготовка");
        construct_str = construct_str.replace(/0!\+!/g, "0+!");
        construct_str = construct_str.replace(/\)\<\/font\>\<\/b\>\/!/g, ")</font></b>/");
        construct_str = construct_str.replace(/!\<b\>\<font color\=\"\#CC0000\"\>!/g, "!");
        construct_str = construct_str.replace(/\+1о/g, "+!1о");
        construct_str = construct_str.replace(/\+3о/g, "+!3о");
        construct_str = construct_str.replace(/\/3о/g, "/!3о");
        arr = construct_str.split('!');
        vm.constr = arr;
        return arr;
    }
    // получаем информ. о барабане для каната
    function get_get_reel() {
        $.ajax({
            type: "POST",
            url: base_url + "/content/get_opt_reel/" + $(diam_r).select2('data').text + "/" + ($("#Real_ves").val()),
            cache: false,
            dataType: "JSON",
            success: function (data) {
                $("#reel").val("№ " + data["name"] + " ( " + data["weight"] + " кг )");
            }
        })
    }
    // список всех барабанов
    function get_reel_all() {
        $.ajax({
            type: "POST",
            url: base_url + "/content/get_reel_all/",
            cache: false,
            dataType: "JSON",
            success: function (data) {
                vm.reel = data;
                location.href = "#win2";
            }
        })
    }
    // вес каната
    function get_final_weight() {
        $.ajax({
            type: "POST",
            url: base_url + "/content/get_final_weight/" + $(diam_r).select2('data').id + "/" + $(core).select2('data').id + "/" + $(steep).select2('data').id + "/" + $(lubr_type).select2('data').id,
            cache: false,
            dataType: "JSON",
            success: function (data) {
                $('#final_weight').empty();
                $("#final_weight").select2("val", "");
                $.each(data, function (key, value) {
                    $('#final_weight').val(value).attr('value', key);
                })
                $("#weight_calc_final").val("");
                $("#reel").val("");
            }
        })
    }
    // способ смазки каната
    function get_l_type() {
        $.ajax({
            type: "POST",
            url: base_url + "/content/get_l_type/" + $(diam_r).select2('data').id + "/" + $(core).select2('data').id,
            cache: false,
            dataType: "JSON",
            success: function (data) {
                $('#lubr_type').empty();
                $("#lubr_type").select2("val", "");
                $.each(data, function (i, value) {
                    $('#lubr_type').append($('<option>').text(value).attr('value', i));
                });
                $(document).ready(function () {
                    $("#lubr_type").select2({width: '220'});
                });
                get_final_weight();
            }
        })
    }
    // шаг
    function get_steep() {
        $.ajax({
            type: "POST",
            url: base_url + "/content/get_steep/" + $(diam_r).select2('data').id + "/" + $(core).select2('data').id,
            cache: false,
            dataType: "JSON",
            success: function (data) {
                $('#steep').empty();
                $("#steep").select2("val", "");
                $.each(data, function (i, value) {
                    $('#steep').append($('<option>').text(value).attr('value', i));
                });
                $(document).ready(function () {
                    $("#steep").select2({width: '220'});
                });
            }
        })
    }
    // диаметр сердечника
    function get_diam_core() {
        $.ajax({
            type: "POST",
            url: base_url + "/content/get_diam_c/" + $(diam_r).select2('data').id + "/" + $(core).select2('data').id,
            cache: false,
            dataType: "JSON",
            success: function (data) {
                $('#diam_core').empty();
                $("#diam_core").select2("val", "");
                $.each(data, function (key, value) {
                    $('#diam_core').val(value).attr('value', key);
                })
            }
        })
        get_steep();
        get_l_type();
    }
    // событие при выборе нового стандарта
    function gostChange(id_gost) {
        vm.img = 'images/sts/'+id_gost+'.png';
        $.post(base_url +'/content/getStrand/'+id_gost, function(data) {
            vm.strand = JSON.parse(data);
        }).fail(function() {
            console.clear();
            console.error('ошибка загрузки /content/getStrand// Функция gostChange');
        });
    }

    // раскрутка каната на проволоки с учетом веса и процентного соотношения
    // таб 1
    function get_tab1() {
        $.ajax({
            type: "POST",
            url: base_url + "/content/spinup/" + $(diam_r).select2('data').id,
            cache: false,
            dataType: "JSON",
            success: function (result) {
                string_to_array(construct);
                vm.diam_w = result['diam_w'];
                vm.weight = result['ves'];
                vm.procent_w = result['percent'];
            }
        });
    }

    // выводим сумарный вес по проволокам
    // таб 2
    function get_tab2() {
        $.ajax({
            type: "POST",
            url: base_url + "/content/getSumVes/" + $(diam_r).select2('data').id,
            cache: false,
            dataType: "JSON",
            success: function (result) {
                vm.sum_diam_w = result['diam_w'];
                vm.sum_weight = result['ves'];
                vm.sum_procent_w = result['percent'];
            }
        });
    }
    // сердечник каната
    function get_core() {
        $.ajax({
            type: "POST",
            url: base_url + "/content/get_core/" + $(diam_r).select2('data').id,
            cache: false,
            dataType: "JSON",
            success: function (data) {
                $('#core').empty();
                $("#core").select2("val", "");
                $.each(data, function (i, value) {
                    $('#core').append($('<option>').text(value).attr('value', i));
                });
                $(document).ready(function () {
                    $("#core").select2({width: '220'});
                });
                get_diam_core();
            }
        })
    }

    WeightEnter = function () {
        get_core();
        $.ajax({
            type: "POST",
            url: base_url + "/content/weight_r/" + $(diam_r).select2('data').id,
            cache: false,
            dataType: "JSON",
            success: function (data) {
                $.each(data, function (key, value) {
                    $('#weight_r').val(value);
                })
            }
        })
        $.ajax({
            type: "POST",
            url: base_url + "/content/get_step_lay/" + $(diam_r).select2('data').id + "/" + 0,
            cache: false,
            dataType: "JSON",
            success: function (data) {
                $.each(data, function (key, value) {
                    $('#step_s').val(value);
                })
            }
        })
        $.ajax({
            type: "POST",
            url: base_url + "/content/get_step_lay/" + $(diam_r).select2('data').id + "/" + 1,
            cache: false,
            dataType: "JSON",
            success: function (data) {
                $.each(data, function (key, value) {
                    $('#step_r').val(value);
                })
            }
        })

        get_tab1();
        get_tab2();

        $('#b_ves').prop('disabled', false);
        $('#Real_ves').prop('readonly', false);
    }

    diam = function () {
        $("#weight_r").val("");
        $('#diam_r').empty();
        $('#core').empty();
        $("#core").select2("val", "");
        $('#lubr_type').empty();
        $("#lubr_type").select2("val", "");
        $('#steep').empty();
        $("#steep").select2("val", "");
        $('#diam_core').empty();
        $("#diam_core").select2("val", "");
        $('#final_weight').empty();
        $("#final_weight").select2("val", "");
        $("#Real_ves").val("");
        $("#constr").html("");
        $("#diam_core").html("");
        $("#step_s").val("");
        $("#step_r").val("");
        $("#diam_core").val("");
        $("#final_weight").html("");
        $('#percentTable').html("");
        $('#diamTable').html("");
        $("#weight_calc").val("");
        $("#weight_calc_final").val("");
        $("#reel").val("");
        $("#final_weight").val("");

        $.ajax({
            type: "POST",
            url: base_url + "/content/gost_r/" + $(gost_r).select2('data').id,
            cache: false,
            dataType: "JSON",
            success: function (data) {
                $('#diam_r').empty();
                $("#diam_r").select2("val", "");
                $.each(data, function (i, value) {
                    $('#diam_r').append($('<option>').text(value).attr('value', i));
                });
                $(document).ready(function () {
                    $("#diam_r").select2({width: '410'});
                });
                WeightEnter();
            }
        })
        $.ajax({
            type: "POST",
            url: base_url + "/content/constr/" + $(gost_r).select2('data').id,
            cache: false,
            dataType: "JSON",
            success: function (data) {
                $.each(data, function (key, value) {
                    construct = value;
                    $('#constr').val(value);
                })
            }
        })
    }

    $(window).load(function () {
        $.ajax({
            type: "POST",
            url: base_url + "/content/getAllGost_r/",
            cache: false,
            dataType: "JSON",
            success: function (data) {
                $.each(data, function (i, value) {
                    $('#gost_r').append($('<option>').text(i).attr('value', value));
                });
                $(document).ready(function () {
                    $("#gost_r").select2({width: '410'});
                });
                $('#help').attr('title', $(gost_r).select2('data').id);
                diam();
                gostChange( $(gost_r).select2('data').id );
            }
        });

        selectedDatum = null;
        $(steep).change(function () {
            get_final_weight();
        })
        $(lubr_type).change(function () {
            get_final_weight();
        })
        $(gost_r).change(function () {
            selectedDatum = null;
            selectedDatum = $(gost_r).select2('data').text;
            $('#help').attr('title', $(gost_r).select2('data').id);
            gostChange( $(gost_r).select2('data').id );
            diam();
            $.ajax({
                type: "POST",
                url: base_url + "/content/constr/" + $(gost_r).select2('data').id,
                cache: false,
                dataType: "JSON",
                success: function (data) {
                    $.each(data, function (key, value) {
                        construct = value;
                        $('#constr').val(value);
                    })
                }
            })

            $.ajax({
                type: "POST",
                url: base_url + "/content/gost_r/" + $(gost_r).select2('data').id,
                cache: false,
                dataType: "JSON",
                success: function (data) {
                    $("#weight_r").val("");
                    $('#diam_r').empty();
                    $('#core').empty();
                    $("#core").select2("val", "");
                    $('#lubr_type').empty();
                    $("#lubr_type").select2("val", "");
                    $('#steep').empty();
                    $("#steep").select2("val", "");
                    $('#diam_core').empty();
                    $("#diam_core").select2("val", "");
                    $('#final_weight').empty();
                    $("#final_weight").select2("val", "");
                    $("#Real_ves").val("");
                    $("#final_weight").val("");
                    $("#constr").html("");
                    $("#final_weight").html("");
                    $("#step_s").val("");
                    $("#step_r").val("");
                    $("#diam_core").val("");
                    $('#percentTable').html("");
                    $('#diamTable').html("");
                    $("#weight_calc").val("");
                    $("#weight_calc_final").val("");
                    $("#reel").val("");

                    $('#diam_r').empty();
                    $("#diam_r").select2("val", "");
                    $.each(data, function (i, value) {
                        $('#diam_r').append($('<option>').text(value).attr('value', i));
                    });

                    $(document).ready(function () {
                        $("#diam_r").select2({width: '410'});
                    });
                }
            })
        });

        // смена диаметра каната
        $(diam_r).change(function () {
            get_core();
            $.ajax({
                type: "POST",
                url: base_url + "/content/get_core/" + $(diam_r).select2('data').id,
                cache: false,
                dataType: "JSON",
                success: function (data) {
                    $('#core').empty();

                    $("#core").select2("val", "");
                    $.each(data, function (i, value) {
                        $('#core').append($('<option>').text(value).attr('value', i));
                    });

                    $(document).ready(function () {
                        $("#core").select2({width: '220'});
                    });

                    for (var p in data) {
                        klop = data[p];

                        break;
                        // к значению каждого свойства прибавить 1
                    }
                    get_diam_core();
                }

            })
            selecteddiam_r = null;
            $("#Real_ves").val("");
            selecteddiam_r = $(diam_r).select2('data');
            $("#weight_r").html("");
            $("#weight_calc").val("");
            $.ajax({
                type: "POST",
                url: base_url + "/content/weight_r/" + $(diam_r).select2('data').id,
                cache: false,
                dataType: "JSON",
                success: function (data) {
                    $.each(data, function (key, value) {
                        $('#weight_r').val(value);
                    })
                }
            })
            $.ajax({
                type: "POST",
                url: base_url + "/content/get_step_lay/" + $(diam_r).select2('data').id + "/" + 0,
                cache: false,
                dataType: "JSON",
                success: function (data) {
                    $.each(data, function (key, value) {
                        $('#step_s').val(value);
                    })
                }
            })
            $.ajax({
                type: "POST",
                url: base_url + "/content/get_step_lay/" + $(diam_r).select2('data').id + "/" + 1,
                cache: false,
                dataType: "JSON",
                success: function (data) {
                    $.each(data, function (key, value) {
                        $('#step_r').val(value);

                    })
                }
            })

            // таб 1
            get_tab1();
            get_tab2();

            $('#b_ves').prop('disabled', false);
            $('#Real_ves').prop('readonly', false);
        });

        // смена сердечника
        $(core).change(function () {
            get_diam_core();
        });

        // расчет веча проволок по заданной длине
        MyFunction = function () {
            get_get_reel();
            var koef = 0;
            koef = ($("#Real_ves").val());
            koef = koef.replace(/\,/g, ".");
            koef = koef / 1000;
            $("#weight_calc").val(Math.round($("#weight_r").val() * koef * 10000) / 10000);
            $("#weight_calc_final").val(Math.round($("#final_weight").val() * koef * 10000) / 10000);

            // расчет данных по заданной длине
            vm.lng_input = [];
            $.each(vm.weight, function(key, val){
                res = (val*koef)*1000/1000;
                vm.lng_input.push(res.toFixed(4));
            });

            // расчет данных по заданной длине (сумарный)
            vm.sum_lng_input = [];
            $.each(vm.sum_weight, function(key, val){
                res = (val*koef)*1000/1000;
                vm.sum_lng_input.push(res.toFixed(4));
            });
        };
    });

    $(function() {
        $("#core").select2({width: '220'});
    });

    </script>

<style>
    .table thead > tr > th, .table tbody > tr > th,
    .table tfoot > tr > th, .table thead > tr > td,
    .table tbody > tr > td, .table tfoot > tr > td {
        padding: 8px 5px;
    }
</style>