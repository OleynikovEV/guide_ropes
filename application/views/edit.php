<style>[v-cloak] { display: none; }</style>

<div class="container" id="cont" v-cloak>

    <div style="text-align: center;">
        <div v-show="msg_error.length != 0" class="alert alert-dismissable alert-danger" style="display: none">
            <span>{{msg_error}}</span>
            <i class="close fa fa-times" aria-hidden="true" v-on:click="close"></i>
        </div>
        <div v-show="msg_success.length != 0" class="alert alert-dismissable alert-success" style="display: none">
            <span>{{msg_success}}</span>
            <i class="close fa fa-times" aria-hidden="true" v-on:click="close"></i>
        </div>
    </div>

    <h3 align="center"> Редактирование справочника канатов </h3>

    <div class="control-group">
        <label class="control-label" id="help" for="gost_r">Стандарт (код)</label>
        <div class="controls">
            <select2 name="gost_r" id="gost_r" style="width: 410px"
                     :options="gost_r_list"
                     v-on:change="gost_r_change"></select2>
            <span>{{construct}}</span>
            <div class="pull-right">
                <span> <b>Поиск Ø </b>
                    <input type="number" v-model.number="diam_search" class="input-small">
                </span>
                <span style="margin-right: 5px"> <b>Поиск эл. кан.</b>
                    <select2 id="strand_r_list" class="input-medium"
                             :options="strand_r_list"
                             :allowClear="true"
                             v-on:change="strand_r_change"></select2>
                </span>
            </div>
        </div>
    </div>

    <ul class="nav">
        <li v-for="(item, index) in nav_list"
            :key="index"
            :class="{active: item.status}"
            @click="clickTab(index)">
            {{ item.name }}
        </li>
        <li>
            <i class="fa fa-cog fa-spin fa-3x fa-fw" v-show="preload"></i>
        </li>
    </ul>

    <div>
        <keep-alive>
            <comment v-bind:is="currentComponent"
                     :id_gost_r="Number(id_gost_r)"
                     :npsv="npsv"
                     :construction="construct"
                     :id_diam="first_id_diam_r"
                     :diam_search="diam_search"
                     :strand_r_selected="strand_r_selected"
                     :operation_list="operation_list"
                     :strand_r_selected="strand_r_selected"
                     v-on:message="messageEvent"
                     v-on:dataloaded="loaded"></comment>
        </keep-alive>
    </div>
</div>

<link href="/css/select2.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/js/select2.js"></script>
<script type="text/javascript" src="/js/vue.js"></script>
<script type="text/javascript" src="/js/vue-select2.js?ver=1.4"></script>
<script type="text/javascript" src="/js/vue-select2_new.js?ver=1.4"></script>

<script type="text/javascript" src="/components/rope_cores.js?1.1.1"></script>
<script type="text/javascript" src="/components/rope_diameters.js?1.1.1"></script>
<script type="text/javascript" src="/components/rope_operations.js?1.1.1"></script>
<script type="text/javascript" src="/components/rope_strands.js?1.1.3"></script>
<script type="text/javascript" src="/components/strand_const.js?1.1.5"></script>
<script type="text/javascript" src="/components/wires_in_strand.js?1.1.1"></script>
<script>
    'use strict';

    Number.prototype.round = function(places) { // математическое округление
        return +(Math.round(this + "e+" + places)  + "e-" + places);
    }

    var vm = new Vue({
        el: '#cont',
        data: {
            construct: '<?= $data["construct"]; ?>', // конструкция каната
            first_id_diam_r: <?= $data['first_id_diam_r'] ?>, // первый диаметр каната для положения проволоки в пряди
            id_gost_r: null,
            gost_change: false,
            gost_r_list: <?= json_encode($data["gost_r_list"]); ?>, // стандарт
            operation_list: <?= json_encode($data["operation_list"]); ?>, // список доступных операций
            strand_r_list: <?= json_encode($data["strand_r_list"]); ?>, // список доступных элементов канатов
            npsv: <?= json_encode($data["npsv_list"]); ?>,
            strand_r_selected: '',
            msg_success: '',
            msg_error: '',
            diam_search: '',
            nav_list: [
                { name: 'Конструкция прядей', status: true, preload: true, component: 'strandConst' },
                { name: 'Проволоки в пряди', status: false, preload: true, component: 'wiresStrand' },
                { name: 'Диаметры', status: false, preload: true, component: 'ropeDiameters' },
                { name: 'Сердечник', status: false, preload: true, component: 'ropeCores' },
                { name: 'Пряди', status: false, preload: true, component: 'ropeStrands' },
                { name: 'Операции', status: false, preload: true, component: 'ropeOperations' },
            ],
            curTabIndex: 0,
            currentComponent: null,
            preload: false,
        },
        mounted: function(){
            this.currentComponent = this.nav_list[this.curTabIndex].component;
            $('#gost_r').trigger('change');

            // отключаем прелоад для первой закладки
            this.preload = this.nav_list[0].preload;
            this.nav_list[0].preload = false;
        },
        methods: {
            strand_r_change: function(val, index, data) {
                this.strand_r_selected = (val > 0) ? data.text : '';
            },
            gost_r_change: function(id_gost, index, arr) {
                this.nav_list.forEach((item) => {
                    item.preload = true;
                });
                this.evePreload(this.curTabIndex);
                this.id_gost_r = id_gost;
                this.first_id_diam_r = arr.id_diam_r;
                this.construct = '';
                this.construct = arr.construct;
            },
            clickTab: function(index) {
                this.evePreload(index);
                this.nav_list[this.curTabIndex].status = false;
                this.nav_list[index].status = true;
                this.currentComponent = this.nav_list[index].component;
                this.curTabIndex = index;
            },
            close: function() {
                this.msg_success = '';
                this.msg_error = '';
            },
            messageEvent: function(status) {
                if (status) {
                    this.msg_success = 'Данные сохранены успешно';
                    console.log('Данные сохранены успешно');
                } else {
                    this.msg_error = 'Данные сохранить не удалось';
                    console.log('Данные сохранить не удалось');
                }
                setTimeout(() => {
                    vm.close();
                }, 2000);
            },
            loaded: function(status) {
                this.preload = false;
            },
            evePreload: function(index) {
                this.preload = this.nav_list[index].preload;
                this.nav_list[index].preload = false;
            }
        },
    });
</script>

<style>
    .nav {
        display: flex;
        border-bottom: 1px solid #ddd;
    }
    .nav li {
        padding: 10px;
        color: #cccccc;
        cursor: pointer;
        border-radius: 4px 4px 0 0;
        margin-bottom: -1px;
    }
    .nav li:hover {
        background-color: #EEEEEE;
        color: #fff;
    }
    .nav .active, .nav .active:hover {
        color: #555555;
        background-color: #ffffff;
        border: 1px solid #dddddd;
        border-bottom-color: transparent;
        cursor: default;
        margin-left: 2px;
        margin-right: 2px;
    }
    .fa-cog {
        color: #5bb75b;
        position: absolute;
        top: 0px;
    }
    @media (min-width: 1200px) {
        .container {
            max-width: 1200px;
            width: 1200px;
        }
    }
</style>