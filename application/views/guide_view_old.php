<div class="container" id="cont">
    <h3 align="center">Полуфабрикат для изготовления канатов. Вес канатов.
        <a style="font-size:24px" style="color: black" onclick="get_reel_all()" onclick="window.location.reload()"
            href="#win2" id="reel_table" value="Барабаны"> Барабаны</a>
    </h3>

    <table width="100%" cellspacing="0" cellpadding="5">
        <tr>
            <td valign="top">
                <div class="control-group">
                    <label class="control-label" id="help" for="gost_r">Стандарт (код)</label>
                    <div class="controls">
                        <select name="gost_r" id="gost_r" value="<? set_value("gost_r") ?>">
                    </div>
                </div>
            </td>

            <td valign="top">
                <div class="control-group">
                    <label class="control-label" for="step_r">Шаг свивки сердечника</label>
                    <div class="controls">
                        <input type="text" readonly name="step_s" id="step_s" value="<? set_value("step_s") ?>">
                    </div>
                </div>
            </td>

            <td valign="top">
                <div class="control-group">
                    <label class="control-label" for="step_r">Шаг свивки каната</label>
                    <div class="controls">
                        <input type="text" readonly name="step_r" id="step_r" value="<? set_value("step_r") ?>">
                    </div>
                </div>
            </td>
        </tr>
    </table>

    <table width="100%" cellspacing="0" cellpadding="5">
        <tr>
            <td valign="top">
                <div class="control-group">
                    <label class="control-label" for="constr">Конструкция</label>
                    <div class="controls">
                        <input style="width:410px" type="text" readonly name="constr" id="constr" value="<? set_value("constr") ?>">
                    </div>
                </div>
            </td>

            <td valign="top">
                <div class="control-group">
                    <label class="control-label" for="core">Выбор сердечника</label>
                    <div class="controls">
                        <select name="core" id="core" value="<? set_value("core") ?>">
                    </div>
                </div>
            </td>

            <td valign="top">
                <div class="control-group">
                    <label class="control-label" for="lubr_type">Способ смазки</label>
                    <div class="controls">
                        <select name="lubr_type" id="lubr_type" value="<? set_value("lubr_type") ?>">
                    </div>
                </div>
            </td>
        </tr>
    </table>

    <table width="100%" cellspacing="0" cellpadding="5">
        <tr>
            <td valign="top" style="width:448px">
                <div class="control-group">
                    <label class="control-label" for="diam_r">Диаметр каната</label>
                    <div class="controls">
                        <select name="diam_r" id="diam_r" value="<? set_value("diam_r") ?>">
                    </div>
                </div>
            </td>

            <td valign="top">
                <div class="control-group">
                    <label class="control-label" for="core">Диаметр сердечника</label>
                    <div class="controls">
                        <input type="text" readonly name="diam_core" id="diam_core" value="<? set_value("diam_core") ?>">
                    </div>
                </div>
            </td>

            <td valign="top">
            </td>
        </tr>
    </table>

    <table width="100%" cellspacing="0" cellpadding="5">
        <tr>
            <td valign="top">
                <div class="control-group">
                    <label class="control-label" for="constr">Вес полуфабриката (1000m)</label>
                    <div class="controls">
                        <input style="width:410px" type="text" readonly name="weight_r" id="weight_r" value="<? set_value("weight_r") ?>">
                    </div>
                </div>
            </td>

            <td valign="top">
                <div class="control-group">
                    <label class="control-label" for="steep">Пропитка</label>
                    <div class="controls">
                        <select name="steep" id="steep" value="<? set_value("steep") ?>">
                    </div>
                </div>
            </td>

            <td valign="top">
                <div class="control-group">
                    <label class="control-label" for="final_weight">Вес каната (1000m)</label>
                    <div class="controls">
                        <input type="text" readonly name="final_weight" id="final_weight" value="<? set_value("final_weight") ?>">
                    </div>
                </div>
            </td>
        </tr>
    </table>

    <table width="100%" cellspacing="0" cellpadding="5">
        <tr>
            <td valign="top" style="width:33%">

            </td>

            <td valign="top">
                <div class="control-group">
                    <label class="control-label" for="Real_ves">Длина заданная</label>
                    <div class="controls">
                        <input style="width:350px" type="text" name="Real_ves" id="Real_ves" value="<? set_value("Real_ves") ?>" readonly="true">
                    </div>
                </div>

                <div class="control-group">
                    <div class="control-group">
                        <input style="width:350px" type="button" onclick="MyFunction()" onclick="window.location.reload()" id="b_ves" disabled="true" value="Рассчитать">
                    </div>
                </div>
            </td>

            <td valign="top" style="width:33%"></td>
        </tr>
    </table>

    <table width="100%" cellspacing="0" cellpadding="5">
        <tr>
            <td valign="top" style="width:50%">
                <div class="control-group">
                    <label class="control-label" for="constr">Вес полуфабриката (заданный)</label>
                    <div class="controls">
                        <input style="width:350px" type="text" readonly="true" name="weight_calc" id="weight_calc" value="<? set_value("weight_calc") ?>">
                    </div>
                </div>
            </td>

            <td>
                <div class="control-group">
                    <label class="control-label" for="constr">№ барабана (вес)</label>
                    <div class="controls">
                        <input style="width:350px" type="text" readonly="true" name="reel" id="reel" value="<? set_value("weight_calc") ?>">
                    </div>

                </div>
            </td>

            <td valign="top" style="width:50%">
                <div class="control-group">
                    <label class="control-label" for="constr">Вес каната (заданный)</label>
                    <div class="controls">
                        <input style="width:350px" type="text" readonly="true" name="weight_calc_final" id="weight_calc_final" value="<? set_value("weight_calc_final") ?>">
                    </div>
                </div>
            </td>
        </tr>
    </table>

    <div class="table" id='percentTable'></div>
    <div class="table" id='diamTable'> </div>
</div>
<? echo form_close() ?>

<a href="#x" class="overlay" id="win2"></a>
<div class="popup">
    <center><h3>Таблица барабанов</h3>
	    <div class="table" id='reeltable'> </div>
    </center>
</div>
<a class="close" title="Закрыть" href="#close"></a></div>


    <link href="/resources/select2/select2.css" rel="stylesheet">
    <script src="<?php echo base_url(select2 . "/select2.js"); ?>"></script>
    <script type="text/javascript">
    var klop;
    var html = "";
    var table;
    var part1;
    var table_d;
    var part_d;

    function string_to_array(str) {
        construct_str = "";
        construct_str = str.replace(/\+/g, "!+!");
        construct_str = construct_str.replace(/\|/g, "!|!");
        construct_str = construct_str.replace(/\;/g, "!;!");
        construct_str = construct_str.replace(/\//g, "!/!");
        construct_str = construct_str.replace(/\(/g, " <b><font color=\"#CC0000\" >(!");
        construct_str = construct_str.replace(/\)/g, "!<b><font color=\"\#CC0000\">)!");
        construct_str = construct_str.replace(/\)!!\+!/g, ")</font></b>+");
        construct_str = construct_str.replace(/\(!0!/g, "(</font></b>0");
        construct_str = construct_str.replace(/\(!о.с.!/g, "(</font></b>о.с.");
        construct_str = construct_str.replace(/\)!!\/!/g, "!<b><font color=\"\#CC0000\">)</font></b>/!");
        construct_str = construct_str.replace(/\(!эл\.пр\.!/g, "(</font></b>эл.пр.");
        construct_str = construct_str.replace(/\(!1заготовка!/g, "(</font></b>1заготовка");
        construct_str = construct_str.replace(/0!\+!/g, "0+!");
        construct_str = construct_str.replace(/\)\<\/font\>\<\/b\>\/!/g, ")</font></b>/");
        construct_str = construct_str.replace(/!\<b\>\<font color\=\"\#CC0000\"\>!/g, "!");
        construct_str = construct_str.replace(/\+1о/g, "+!1о");
        construct_str = construct_str.replace(/\+3о/g, "+!3о");
        construct_str = construct_str.replace(/\/3о/g, "/!3о");
        arr = construct_str.split('!');
        return arr;
    }

    function get_get_reel() {
        $.ajax({
            type: "POST",
            url: base_url + "/content/get_opt_reel/" + $(diam_r).select2('data').text + "/" + ($("#Real_ves").val()),
            cache: false,
            dataType: "JSON",
            success: function (data) {
                $("#reel").val("№ " + data["name"] + " ( " + data["weight"] + " кг )");
            }
        })
    }

    function get_reel_all() {
        $.ajax({
            type: "POST",
            url: base_url + "/content/get_reel_all/",
            cache: false,
            dataType: "JSON",
            success: function (data) {
                html = '';
                html = '<table border=1 white-space=nowrap>';
                html += '<tr> <td>№</td> <td>Диам. щеки</td> <td>Длина шейки</td> <td>Вес без оп.*</td> <td>Вес с оп.*</td> </tr>';
                for (i = 0; i < 20; i++) {
                    html += '<tr> <td>';
                    html += data["name"][i];
                    html += '</td> <td>';
                    html += data["diam_cheek"][i];
                    html += '</td> <td>';
                    html += data["length_pin"][i];
                    html += '</td> <td>'
                    html += data["weight"][i];
                    html += '</td> <td>';
                    html += data["weight_w"][i];
                    html += '</td> </tr>';
                }
                html += '</table>';
                html += '<h4>*"оп." - опалубка</h4>';
                $("#reeltable").html(html);
                location.href = "#win2";
            }
        })
    }

    function get_final_weight() {
        $.ajax({
            type: "POST",
            url: base_url + "/content/get_final_weight/" + $(diam_r).select2('data').id + "/" + $(core).select2('data').id + "/" + $(steep).select2('data').id + "/" + $(lubr_type).select2('data').id,
            cache: false,
            dataType: "JSON",
            success: function (data) {
                $('#final_weight').empty();
                $("#final_weight").select2("val", "");
                $.each(data, function (key, value) {
                    $('#final_weight').val(value).attr('value', key);
                })
                $("#weight_calc_final").val("");
                $("#reel").val("");
            }
        })
    }

    function get_l_type() {
        $.ajax({
            type: "POST",
            url: base_url + "/content/get_l_type/" + $(diam_r).select2('data').id + "/" + $(core).select2('data').id,
            cache: false,
            dataType: "JSON",
            success: function (data) {
                $('#lubr_type').empty();
                $("#lubr_type").select2("val", "");
                $.each(data, function (i, value) {
                    $('#lubr_type').append($('<option>').text(value).attr('value', i));
                });
                $(document).ready(function () {
                    $("#lubr_type").select2({width: '220'});
                });
                get_final_weight();
            }
        })
    }

    function get_steep() {
        $.ajax({
            type: "POST",
            url: base_url + "/content/get_steep/" + $(diam_r).select2('data').id + "/" + $(core).select2('data').id,
            cache: false,
            dataType: "JSON",
            success: function (data) {
                $('#steep').empty();
                $("#steep").select2("val", "");
                $.each(data, function (i, value) {
                    $('#steep').append($('<option>').text(value).attr('value', i));
                });
                $(document).ready(function () {
                    $("#steep").select2({width: '220'});
                });
            }
        })
    }

    function get_diam_core() {
        $.ajax({
            type: "POST",
            url: base_url + "/content/get_diam_c/" + $(diam_r).select2('data').id + "/" + $(core).select2('data').id,
            cache: false,
            dataType: "JSON",
            success: function (data) {
                $('#diam_core').empty();
                $("#diam_core").select2("val", "");
                $.each(data, function (key, value) {
                    $('#diam_core').val(value).attr('value', key);
                })
            }
        })
        get_steep();
        get_l_type();
    }

    WeightEnter = function () {
        $.ajax({
            type: "POST",
            url: base_url + "/content/get_core/" + $(diam_r).select2('data').id,
            cache: false,
            dataType: "JSON",
            success: function (data) {
                $('#core').empty();
                $("#core").select2("val", "");
                $.each(data, function (i, value) {
                    $('#core').append($('<option>').text(value).attr('value', i));
                });
                $(document).ready(function () {
                    $("#core").select2({width: '220'});
                });
                get_diam_core();
            }
        })

        $.ajax({
            type: "POST",
            url: base_url + "/content/weight_r/" + $(diam_r).select2('data').id,
            cache: false,
            dataType: "JSON",
            success: function (data) {
                $.each(data, function (key, value) {
                    $('#weight_r').val(value);
                })
            }
        })

        $.ajax({
            type: "POST",
            url: base_url + "/content/get_step_lay/" + $(diam_r).select2('data').id + "/" + 0,
            cache: false,
            dataType: "JSON",
            success: function (data) {
                $.each(data, function (key, value) {
                    $('#step_s').val(value);
                })
            }
        })

        $.ajax({
            type: "POST",
            url: base_url + "/content/get_step_lay/" + $(diam_r).select2('data').id + "/" + 1,
            cache: false,
            dataType: "JSON",
            success: function (data) {
                $.each(data, function (key, value) {
                    $('#step_r').val(value);
                })
            }
        })

        $.ajax({
            type: "POST",
            url: base_url + "/content/spinup/" + $(diam_r).select2('data').id,
            cache: false,
            dataType: "JSON",
            success: function (result) {
                table = new Array(result['diam_w'], result['ves'], result['percent']);
                part1 = ['Диаметр проволоки', 'Вес(1000m)', 'Процент проволоки'];
                arr = string_to_array(construct);
                {
                    html = '<table white-space=nowrap >';
                    html += '<tr>';
                    html += '<td>Конструкция</td>';
                    for (var k = 0; k < arr.length; k++) {
                        html += '<td nowrap=\"nowrap\">' + arr[k] + '</td>';
                    }
                    html += '</tr>';
                    for (var i = 0; i < 3; i++) {
                        html += '<tr><td>' + part1[i] + '</td>';
                        for (var j = 0; j < table[i].length; j++) {
                            html += '<td></td><td>' + Math.round(table[i][j] * 10000) / 10000 + '</td>';
                        }
                        html += '</tr>';
                    }
                    html += '</table>';
                    html += '<hr color=\"\#CC0000\">';
                    $('#percentTable').html(html);
                }
            }
        });

        /**************************************************************************************************************/
        /*************************** ВЫВОДИМ СУМАРНЫЙ ВЕС *************************************************************/
        $.ajax({
            type: "POST",
            url: base_url + "/content/getSumVes/" + $(diam_r).select2('data').id,
            cache: false,
            dataType: "JSON",
            success: function (result) {
                table_d = new Array(result['diam_w'], result['ves'], result['percent']);
                part_d = ['Сумма по диаметрам проволоки', 'Вес(1000m)', 'Процент проволоки'];
                html_diam = "";
                {
                    html_diam = '<table border=0>';
                    for (var i = 0; i < 3; i++)
                    {
                        html_diam += '<tr><td>' + part_d[i] + '</td>';
                        for (var j = 0; j < table_d[i].length; j++)
                            html_diam += '<td></td><td>' + Math.round(table_d[i][j] * 10000) / 10000 + '</td>';

                        html_diam += '</tr>';
                    }
                    html_diam += '</table>';
                    $('#diamTable').html(html_diam);
                }
            }
        });

        $('#b_ves').prop('disabled', false);
        $('#Real_ves').prop('readonly', false);
    }

    diam = function () {
        $("#weight_r").val("");
        $('#diam_r').empty();
        $('#core').empty();
        $("#core").select2("val", "");
        $('#lubr_type').empty();
        $("#lubr_type").select2("val", "");
        $('#steep').empty();
        $("#steep").select2("val", "");
        $('#diam_core').empty();
        $("#diam_core").select2("val", "");
        $('#final_weight').empty();
        $("#final_weight").select2("val", "");
        $("#Real_ves").val("");
        $("#constr").html("");
        $("#diam_core").html("");
        $("#step_s").val("");
        $("#step_r").val("");
        $("#diam_core").val("");
        $("#final_weight").html("");
        $('#percentTable').html("");
        $('#diamTable').html("");
        $("#weight_calc").val("");
        $("#weight_calc_final").val("");
        $("#reel").val("");
        $("#final_weight").val("");

        $.ajax({
            type: "POST",
            url: base_url + "/content/gost_r/" + $(gost_r).select2('data').id,
            cache: false,
            dataType: "JSON",
            success: function (data) {
                $('#diam_r').empty();
                $("#diam_r").select2("val", "");
                $.each(data, function (i, value) {
                    $('#diam_r').append($('<option>').text(value).attr('value', i));
                });
                $(document).ready(function () {
                    $("#diam_r").select2({width: '350'});
                });
                WeightEnter();
            }
        })
        $.ajax({
            type: "POST",
            url: base_url + "/content/constr/" + $(gost_r).select2('data').id,
            cache: false,
            dataType: "JSON",
            success: function (data) {
                $.each(data, function (key, value) {
                    construct = value;
                    $('#constr').val(value);
                })
            }
        })
    }

    $(window).load(function () {
        $.ajax({
            type: "POST",
            url: base_url + "/content/getAllGost_r/",
            cache: false,
            dataType: "JSON",
            success: function (data) {
                $.each(data, function (i, value) {
                    $('#gost_r').append($('<option>').text(i).attr('value', value));
                });
                $(document).ready(function () {
                    $("#gost_r").select2({width: '410'});
                });
                $('#help').attr('title', $(gost_r).select2('data').id);
                diam();
            }
        });

        selectedDatum = null;
        $(steep).change(function () {
            get_final_weight();
        })
        $(lubr_type).change(function () {
            get_final_weight();
        })
        $(gost_r).change(function () {
            selectedDatum = null;
            selectedDatum = $(gost_r).select2('data').text;
            $('#help').attr('title', $(gost_r).select2('data').id);
            diam();
            $.ajax({
                type: "POST",
                url: base_url + "/content/constr/" + $(gost_r).select2('data').id,
                cache: false,
                dataType: "JSON",
                success: function (data) {
                    $.each(data, function (key, value) {
                        construct = value;
                        $('#constr').val(value);
                    })
                }
            })

            $.ajax({
                type: "POST",
                url: base_url + "/content/gost_r/" + $(gost_r).select2('data').id,
                cache: false,
                dataType: "JSON",
                success: function (data) {
                    $("#weight_r").val("");
                    $('#diam_r').empty();
                    $('#core').empty();
                    $("#core").select2("val", "");
                    $('#lubr_type').empty();
                    $("#lubr_type").select2("val", "");
                    $('#steep').empty();
                    $("#steep").select2("val", "");
                    $('#diam_core').empty();
                    $("#diam_core").select2("val", "");
                    $('#final_weight').empty();
                    $("#final_weight").select2("val", "");
                    $("#Real_ves").val("");
                    $("#final_weight").val("");
                    $("#constr").html("");
                    $("#final_weight").html("");
                    $("#step_s").val("");
                    $("#step_r").val("");
                    $("#diam_core").val("");
                    $('#percentTable').html("");
                    $('#diamTable').html("");
                    $("#weight_calc").val("");
                    $("#weight_calc_final").val("");
                    $("#reel").val("");

                    $('#diam_r').empty();
                    $("#diam_r").select2("val", "");
                    $.each(data, function (i, value) {
                        $('#diam_r').append($('<option>').text(value).attr('value', i));
                    });

                    $(document).ready(function () {
                        $("#diam_r").select2({width: '350'});
                    });
                    for (var p in data) {

                        klop = data[p];
                        break;
                        // к значению каждого свойства прибавить 1
                    }
                }
            })
        });


        $(diam_r).change(function () {
            $.ajax({
                type: "POST",
                url: base_url + "/content/get_core/" + $(diam_r).select2('data').id,
                cache: false,
                dataType: "JSON",
                success: function (data) {
                    $('#core').empty();

                    $("#core").select2("val", "");
                    $.each(data, function (i, value) {
                        $('#core').append($('<option>').text(value).attr('value', i));
                    });

                    $(document).ready(function () {
                        $("#core").select2({width: '220'});
                    });

                    for (var p in data) {
                        klop = data[p];

                        break;
                        // к значению каждого свойства прибавить 1
                    }
                    get_diam_core();
                }

            })
            selecteddiam_r = null;
            $("#Real_ves").val("");
            selecteddiam_r = $(diam_r).select2('data');
            $("#weight_r").html("");
            $("#weight_calc").val("");
            $.ajax({
                type: "POST",
                url: base_url + "/content/weight_r/" + $(diam_r).select2('data').id,
                cache: false,
                dataType: "JSON",
                success: function (data) {
                    $.each(data, function (key, value) {
                        $('#weight_r').val(value);
                    })
                }
            })
            $.ajax({
                type: "POST",
                url: base_url + "/content/get_step_lay/" + $(diam_r).select2('data').id + "/" + 0,
                cache: false,
                dataType: "JSON",
                success: function (data) {
                    $.each(data, function (key, value) {
                        $('#step_s').val(value);
                    })
                }
            })

            $.ajax({
                type: "POST",
                url: base_url + "/content/get_step_lay/" + $(diam_r).select2('data').id + "/" + 1,
                cache: false,
                dataType: "JSON",
                success: function (data) {
                    $.each(data, function (key, value) {
                        $('#step_r').val(value);

                    })
                }
            })

            $.ajax({
                type: "POST",
                url: base_url + "/content/spinup/" + $(diam_r).select2('data').id,
                cache: false,
                dataType: "JSON",
                success: function (result) {
                    table = new Array(result['diam_w'], result['ves'], result['percent']);
                    part1 = ['Диаметр проволоки', 'Вес(1000m)', 'Процент проволоки'];

                    arr = string_to_array(construct);

                    {
                        html = '<table border=0 white-space=nowrap >';
                        html += '<tr>';
                        html += '<td>Конструкция</td>';
                        for (var k = 0; k < arr.length; k++) {
                            html += '<td nowrap=\"nowrap\">' + arr[k] + '</td>';

                        }
                        html += '</tr>';
                        for (var i = 0; i < 3; i++) {
                            html += '<tr><td>' + part1[i] + '</td>';
                            for (var j = 0; j < table[i].length; j++) {
                                html += '<td></td><td>' + Math.round(table[i][j] * 10000) / 10000 + '</td>';
                            }
                            html += '</tr>';
                        }
                        html += '</table>';
                        html += '<hr color=\"\#CC0000\">';
                        $('#percentTable').html(html);
                    }
                }
            });

            /**********************************************************************************************************/
            $.ajax({
                type: "POST",
                url: base_url + "/content/getSumVes/" + $(diam_r).select2('data').id,
                cache: false,
                dataType: "JSON",
                success: function (result) {
                    table_d = new Array(result['diam_w'], result['ves'], result['percent']);
                    part_d = ['Сумма по диаметрам проволоки', 'Вес(1000m)', 'Процент проволоки'];
                    html_diam = "";
                    {
                        html_diam = '<table border=0>';
                        for (var i = 0; i < 3; i++) {
                            html_diam += '<tr><td>' + part_d[i] + '</td>';
                            for (var j = 0; j < table_d[i].length; j++) {
                                html_diam += '<td></td><td>' + Math.round(table_d[i][j] * 10000) / 10000 + '</td>';
                            }
                            html_diam += '</tr>';
                        }
                        html_diam += '</table>';
                        $('#diamTable').html(html_diam);
                    }
                }
            });


            $('#b_ves').prop('disabled', false);
            $('#Real_ves').prop('readonly', false);
        });

        $(core).change(function () {
            get_diam_core();
        });

        MyFunction = function () {
            get_get_reel();
            arr = string_to_array(construct);
            var koef = 0;
            koef = ($("#Real_ves").val());
            koef = koef.replace(/\,/g, ".");
            koef = koef / 1000;
            $("#weight_calc").val(Math.round($("#weight_r").val() * koef * 10000) / 10000);
            $("#weight_calc_final").val(Math.round($("#final_weight").val() * koef * 10000) / 10000)
            {
                html = '<table border=0>';
                html += '<tr>';
                html += '<td>Конструкция</td>';
                for (var k = 0; k < arr.length; k++) {
                    html += '<td nowrap=\"nowrap\">' + arr[k] + '</td>';
                }
                html += '</tr>';
                for (var i = 0; i < 3; i++) {
                    html += '<tr><td>' + part1[i] + '</td>';
                    for (var j = 0; j < table[i].length; j++) {
                        html += '<td></td><td>' + Math.round(table[i][j] * 10000) / 10000 + '</td>';
                    }
                    html += '</tr>';
                }
                html += '<tr>';
                html += '<td>Заданный вес</td>';
                for (var j = 0; j < table[1].length; j++) {
                    html += '<td></td><td>' + Math.round((table[1][j] * koef) * 10000) / 10000 + '</td>';
                }
                html += '</tr>';
                html += '</table>';
                html += '<hr color=\"\#CC0000\">';
                $('#percentTable').html(html);
            }
            html_diam = "";
            {
                html_diam = '<table border=0>';
                for (var i = 0; i < 2; i++) {
                    html_diam += '<tr><td>' + part_d[i] + '</td>';
                    for (var j = 0; j < table_d[i].length; j++) {
                        html_diam += '<td></td><td>' + Math.round(table_d[i][j] * 10000) / 10000 + '</td>';
                    }
                    html_diam += '</tr>';
                }
                html_diam += '<tr>';
                html_diam += '<td>Заданный вес</td>';
                for (var j = 0; j < table_d[1].length; j++) {
                    html_diam += '<td></td><td>' + Math.round((table_d[1][j] * koef) * 10000) / 10000 + '</td>';
                }
                html_diam += '</table>';
                $('#diamTable').html(html_diam);
            }
        };
    });

    </script>