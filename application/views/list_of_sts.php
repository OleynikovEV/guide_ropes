<?php
    if( !is_array($data) or is_null($data) )
    {
        echo "Список пустой";
        return;
    }
?>

<style>
    body { font-size: 12px; }
    table
    {
        border: 1px solid #eaf2fb;
        width: 1096px !important;
        margin-left: auto;
        margin-right: auto;
    }

    th, td
    {
        border: 1px solid #eaf2fb;
        padding: 5px;
    }

    .align_center { text-align: center; }

    thead > tr > th
    {
        padding: 0 !important;
        margin: 0 !important;
        text-align: center !important;
        vertical-align: middle !important;
        font-size: 14px;
    }

    tbody > tr > th
    {
        text-align: center !important;
        vertical-align: middle !important;
        font-weight: bolder;
        font-size: 14px;
        background-color: #F9F9F9;
    }

    @media print
    {
        body
        {
            font-size: 12px;
            font-family: Avenir, Arial, sans-serif;
        }

        table
        {
            /*margin-top: -20px !important;*/
            width: 900px !important;
            page-break-inside: auto;
            font-weight: bolder;
        }

        table, th, td { border-color: black !important; }

        th, td { padding: 0 !important; }

        .print, .footer, tfoot { display: none; }
     }
</style>

<p class="print"><input class="btn" id="print" value="Распечатать"> <i> Материал ОС сердечника - полипропилен пропитан (А2 смз.) </i> </p>

<?php
    $thead = '
        <thead>
            <tr>
                <th>№<br />п/п</th>
                <th>Ф каната<br />мм</th>
                <th>Раскладка процентов по проволокам (диаметр-процент)</th>
                <th>Расч.вес.<br />1000м, кг</th>
            </tr>
        </thead>';
?>

<table>
    <?php echo $thead;  ?>
    <tbody>
    <?php
        $page = 1; // номер страницы
        $column = 1; // номер строки
        // перебираем весь массив
        foreach( $data as $key => $value )
        {
            if( $column > 51 ) // если номер строки 51, создаем новую таблицу на новой странице
            {
                // создаем новую страницу и на ней новую таблицу
                echo
                    '
                                </tbody>
                            </table>
                            <div align="center" style="margin: 10px;">&mdash;'. $page .'&mdash;</div>
                            <table style="margin: 10px;">
                                '. $thead .'
                                <tbody>
                        ';
				
				if( $column != 53) // если название стандарта оказывается последней строкой на листе, переносим его на новую страницу и смещаем новую таблицу на несколько строк
					echo '<br /><br />';		
                $page++; // увеличивем номер страницы на 1
                $column = 1; // увеличивем номер строки на 1
            }

            // выводим одной строкой
            echo
            '
                <tr role="row">
                    <th colspan="4">
                        <b>('. $value['id_gost_r'] . ')&nbsp;&nbsp;' . $value['gost_r'] . '&nbsp;&nbsp;' . $value['construct'] . '&nbsp;&nbsp;(' . $value['sts'] . ')</b>
                    </th>
                </tr>
            ';
            $column++;
            $i = 1; // счетчик
            // перебираем все диаметры по стандартам
            foreach( $value['diam_r'] as $key_row => $row )
            {
                $all_diam = null;
                $bg_color = "background-color: white; color: black;";
                // перебираем все диаметры проволоки в канате
                if( is_array($row) )
                {
                    if( ($column > 52) or ($page == 1 and $column > 51) ) // если номер строки 51 или для первой страницы 50, создаем новую таблицу
                    {
                        // создаем новую страницу и на ней новую таблицу + номер страницы
                        echo
                        '
                                </tbody>
                            </table>
                            <div align="center" style="margin: 10px;">&mdash; '. $page .' &mdash;</div>
                            <table style="margin: 10px;">
                                '. $thead .'
                                <tbody>
                        ';
                        $page++; // увеличиваем номер страницы на 1
                        $column = 1; // увеличивем номер строки на 1
                    }

                    // и формируем строку состоящую из диаметра проволоки в канате и его сумарного веса (по диаметру каната)
                    foreach ($row as $k => $v)
                    {
                        if( $k != 'final_weight' ) // исключаем ключ final_weight
                            $all_diam = $all_diam . '[' . $v["diam_w"] . '&mdash;' . $v["percent"] . ']&nbsp;&nbsp;';
                    }

                    if( is_null($all_diam) ) // если в справочнике нет проволок, подсвечиваем строку красным
                        $bg_color = "background-color: red; color: white;";

                    echo
                    '
                        <tr style="'. $bg_color .'" >
                            <td class="align_center">' . $i . '</td>
                            <td class="align_center"><b>' . $key_row . '</b></td>
                            <td>' . $all_diam . '</td>
                            <td class="align_center">'. $row['final_weight'] .'</td>
                        </tr>
                    ';
                }
                else
                {
                    echo
                    '
                        <tr style="background-color: #fff819; color: black;" >
                            <td class="align_center">' . $i . '</td>
                            <td class="align_center"><b> Ошибка данные по диаметру не удалось получить </b></td>
                            <td> - </td>
                            <td class="align_center"> - </td>
                        </tr>
                    ';
                }
                $i++;
                $column++;
            }
        }
    ?>
    </tbody>
</table>
<!-- выводим номер страницы на последнем листе -->
<?php echo '<div align="center" style="margin: 10px;">&mdash; '. $page .' &mdash;</div>'; ?>

<script language="javascript">
    $('#print').on('click',function(){ window.print(); })
</script>

