<!doctype html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?php echo $title ?></title>
    <meta name="description" content="<?php echo $description ?>"/>
    <meta name="viewport" content="width=device-width">
    <meta name="keywords" content="<?php echo $keywords ?>"/>
    <meta name="author" content="<?php echo $author ?>"/>

    <link rel="stylesheet" href="<?php echo base_url(CSS . "style-modal.css "); ?>">
    <link rel="stylesheet" href="<?php echo base_url(CSS . "style.css"); ?>">
    <link rel="stylesheet" href="<?php echo base_url(CSS . "global.css"); ?>">
    <link rel="stylesheet" href="<?php echo base_url("/css/font-awesome.css"); ?>">
    <?php foreach ($css as $c): ?>
        <link rel="stylesheet" href="<?php echo base_url() . CSS . $c ?>">
    <?php endforeach; ?>
    <link rel="stylesheet" href="<?php echo base_url(CSS . "bootstrap.min.css"); ?>">

    <script src="<?php echo base_url(JS . "libs/jquery-ui/jquery-1.9.1.js"); ?>"></script>
    <script src="<?php echo base_url(JS . "bootstrap.min.js"); ?>"></script>
    <script src="<?php echo base_url(JS . "script.js"); ?>"></script>
</head>
<body>
    <?php echo $body ?>
</body>
</html>
