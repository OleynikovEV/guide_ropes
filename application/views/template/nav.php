<?php $access = $this->session->userdata; ?>
<ul class="nav nav-pills nav-list nav-stacked navbar-nav">
    <li  class="dropdown <?php echo isActive($pageName, "start"); ?>">
        <a class="dropdown-toggle" href="#" data-toggle="dropdown">Заявки <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
            <li><a class="btn1" href="/query/filter">Список заявок</a></li>
            <li><a class="btn1" href="/query/new_query">Заявки на рассмотрении</a></li>
            <li><a class="btn1" href="/query/closed">Выданные в производство</a></li>
            <li><a class="btn1" href="/query/deny">Не приняты в производство</a></li>
            <li><a class="btn1" href="/query/no_info">Заявки с недостающими данными</a></li>
            <li><a class="btn1" href="/query/new_prod">Новый вид продукции</a><br/></li>
        </ul>
    </li>
    <?php
    if (isset($access['logged_in']['username'])) {
        ?>
        <li class="<?php echo isActive($pageName, "arm"); ?>">
            <a href="<?php echo base_url() ?>arm/index">АРМ</a>
        </li>
    <?
    }
    if (isset($access['logged_in']['username'])) {
        ?>
        <li class="<?php echo isActive($pageName, "users"); ?>">
            <a href="<?php echo site_url('verifylogin/out'); ?>">
                <?php echo($access['logged_in']['username'] . '(Выход)'); ?>
            </a>
        </li>
    <?php
    } else {
        ?>
        <li class="<?php echo isActive($pageName, "users"); ?>">
            <a href="<?php echo site_url('login'); ?>">Вход</a>
        </li>
    <? } ?>
    <li class="<?php echo isActive($pageName, "doc"); ?>">
        <a href="<?php echo base_url(); ?>docs">Инструкции</a>
    </li>
</ul>
<script>
    $(window).load(function () {
        $('.row div[class^="span"]:last-child').addClass('last-child');
    })
</script>