<header id="header" class="navbar navbar-inverse navbar-collapse navbar-static-top" role="navigation">
    <div class="navbar-inner ">
        <div class="container">
            <a class="navbar-brand" href="/">Старт</a>
            <a class="navbar-brand" href="liststs">Список стандартов</a>
            <?php if(is_logged_in() === true) {?>
                <a class="navbar-brand" href="edit">Редактировать</a>
                <a class="navbar-brand" href="login/logout">Выход</a>
            <?php } else {?>
                <a class="navbar-brand" href="login">Редактировать</a>
            <?php } ?>
            <a class="navbar-brand" href="/?ver=old">Старая версия</a>
        </div>
    </div>
</header>
