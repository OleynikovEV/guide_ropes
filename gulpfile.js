var gulp        = require('gulp');
var browserSync = require('browser-sync').create();

// Обновление страниц сайта на локальном сервере
gulp.task('browser-sync', function() {
    browserSync.init({
        proxy: "192.168.81.15:8899"
    });
});

// Наблюдение за файлами
gulp.task('watch', ['browser-sync'], function() {
  gulp.watch('application/**/*.php', browserSync.reload);
  gulp.watch('public_html/components/*.js', browserSync.reload);
  //gulp.watch('public_html/**/*.js', browserSync.reload);
  //gulp.watch('public_html/css/*.css', browserSync.reload);
});

gulp.task('default', ['browser-sync', 'watch']);