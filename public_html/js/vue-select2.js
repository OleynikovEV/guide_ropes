Vue.component('select2', {
    props: {
        options: {
            default: []
        },
        value: {
            default: null
        },
        placeholder: {
            type: String,
            default: 'Выберите элемент'
        },
        addempty: {
            type: Boolean,
            default: false
        },
        allowclear: {
            type: Boolean,
            default: false
        },
    },
    template: '<select></select>',
    mounted: function () {
        var vm = this;
        if (this.addempty == true && this.options != null) {
            $(this.$el).prepend('<option></option>');
        }

        $(this.$el) // init select2
            .select2({
                placeholder: this.placeholder,
                allowClear: this.allowclear,
                data: this.options
            })
            .val(vm.value)
            .trigger('change')
            .on('change', function() { // emit event on change.
                index = $('#'+vm.$el.id+' option').index( $('#'+vm.$el.id+' option:selected') );
                vm.$emit('change', this.value, index, vm.options[index]);
            })
    },
    watch: {
        value: function (value) { // update value
            $(this.$el).val(value).trigger('change');
        },
        options: function (options) { // update options
            $(this.$el).empty(); // clear options
            $(this.$el).select2({ data: options });
        }
    },
    destroyed: function () {
        $(this.$el).off().select2('destroy');
    }
})