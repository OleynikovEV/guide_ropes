Vue.component('select2_new', {
    props: {
        options: {
            default: []
        },
        value: {
            default: null
        },
        placeholder: {
            type: String,
            default: 'Выберите элемент'
        },
        allowclear: {
            type: Boolean,
            default: false
        },
        addempty: {
            type: Boolean,
            default: false
        },
        send_data: {
            default: null
        }
    },
    template: '<select></select>',
    data: function() {
        return {
            event_change: false,
        }
    },
    mounted: function () {
        var that = this;
        if (this.addempty == true && this.options != null) {
            $(this.$el).prepend('<option></option>');
        }
        $(this.$el) // init select2
            .select2({
                closeOnSelect: true,
                placeholder: this.placeholder,
                allowClear: this.allowclear,
                data: this.options,
            })
            .val(this.value)
            .trigger('change');

        $(this.$el).on('change', function () { // emit event on change.
            that.event_change = true;
            index = $('#'+that.$el.id+' option').index( $('#'+that.$el.id+' option:selected') ) - 1;
            that.$emit('input', this.value, index, that.options[index], that.send_data);
            that.$emit('change', this.value, index, that.options[index], that.send_data);
        });
    },
    watch: {
        value: function (value) { // update value
            if (this.event_change == false) {
                $(this.$el).val(value).trigger('change.select2');
                index = $('#' + this.$el.id + ' option').index($('#' + this.$el.id + ' option:selected'));
                this.$emit('input', value, index, this.options[index], that.send_data);
            } else
                this.event_change = false;
        },
        options: function (options) { // update options
            $(this.$el).empty(); // clear options
            $(this.$el).select2({ data: options });
        }
    },
    destroyed: function () {
        $(this.$el).off().select2('destroy');
    }
})