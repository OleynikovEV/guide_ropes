/**
 * Created by Eugene on 21.07.2017.
 */
var language = {
    sSearch: '',
    sEmptyTable: '<b>Записи не найдены</b>',
    sInfo: 'Найдено _TOTAL_ записей',
    sInfoFiltered: '(всего записей _MAX_)',
    sInfoEmpty: '',
    sLengthMenu: 'Показать _MENU_ записей',
    paginate: {
        first: 'В начало',
            previous: '< назад',
            next: 'дальше >',
            last: 'В конец'
    },
}