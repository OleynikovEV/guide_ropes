function addzero(n) {
    return n < 9 ? "0" + n : n;
}


var base_url = (function () {
    var a = document.createElement('a');
    url = window.location.href;
    a.href = url;
    return a.protocol + '//' + a.hostname + (a.port ? (':' + a.port) : "");
})();
